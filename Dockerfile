FROM node:15

RUN mkdir /client
WORKDIR /client
COPY package.json /client/package.json
COPY yarn.lock /client/yarn.lock

RUN yarn install

COPY . /client
