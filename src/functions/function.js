import moment from 'moment';
import firebase from '../firebase/config';

export const previewFile = (file, basicLength, callback) => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  const reader = new FileReader();

  reader.addEventListener('load', () => {
    const image = new Image();
    image.src = reader.result;
    image.onload = () => {
      let sx;
      let sy;
      let sw;
      let sh;

      const orgW = image.naturalWidth;
      const orgH = image.naturalHeight;
      const ratioW = basicLength / orgW;
      const ratioH = basicLength / orgH;
      if (orgW < orgH) {
        sx = 0;
        sy = (orgH * ratioW - basicLength) / 2;
        sw = orgW;
        sh = orgW;
      } else {
        sx = (orgW * ratioH - basicLength) / 2;
        sy = 0;
        sw = orgH;
        sh = orgH;
      }
      canvas.width = basicLength;
      canvas.height = basicLength;
      context.drawImage(image, sx, sy, sw, sh, 0, 0, basicLength, basicLength);
      const resized = canvas.toDataURL('image/jpeg');
      callback(resized);
    };
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
};

export const fileSizeCheck = (target) => {
  const sizeLimit = 1024 * 1024;
  if (target.files[0].size > sizeLimit) {
    target.value = '';
    throw new Error('ファイルサイズは1MB以下にしてください');
  } else {
    return target.files[0];
  }
};

export const sendEmail = () => {
  const actionCodeSettings = {
    url: 'http://localhost:8000',
  };
  firebase.auth().languageCode = 'ja';
  const user = firebase.auth().currentUser;
  user.sendEmailVerification(actionCodeSettings)
    .then(() => {
      window.flash('認証メールを送信しました');
    });
};

export const sliceByNumber = (array, number) => {
  const length = Math.ceil(array.length / number);
  return new Array(length).fill().map((_, i) => array.slice(i * number, (i + 1) * number));
};

export const howLongAgo = (dateString) => {
  if (!moment(dateString).isValid) {
    return 'dateStringが有効ではありません';
  }

  const now = moment();
  const fromDay = moment(dateString);

  if (now.diff(fromDay, 'y')) {
    return `${now.diff(fromDay, 'y')}年前`;
  } if (now.diff(fromDay, 'M')) {
    return `${now.diff(fromDay, 'M')}ヶ月前`;
  } if (now.diff(fromDay, 'w')) {
    return `${now.diff(fromDay, 'w')}週間前`;
  } if (now.diff(fromDay, 'd')) {
    return `${now.diff(fromDay, 'd')}日前`;
  } if (now.diff(fromDay, 'h')) {
    return `${now.diff(fromDay, 'h')}時間前`;
  }
  return 'たった今';
};

export const mixArray = (a1, a2) => {
  const result = [];
  a1.forEach((a, i) => {
    a2.forEach((b, j) => {
      if (i === j) {
        result.push({
          name: a,
          value: b,
        });
      }
    });
  });
  return result;
};
