import { configureStore } from '@reduxjs/toolkit';
import userReducer from './features/user/userSlice';
import errorsReducer from './features/errors/errorsSlice';
import confirmReducer from './features/confirm/confirmSlice';
import sessionModalReducer from './features/sessionModal/sessionModalSlice';
import authTokenReducer from './features/authToken/authTokenSlice';
import reauthModalReducer from './features/reauthModal/reauthModalSlice';
import genreReducer from './features/genre/genreSlice';
import questionFormReducer from './features/questionForm/questionFormSlice';
import mobileUserDropdownReducer from './features/mobileUserDropdown/mobileUserDropdownSlice';

const setLocalStorage = (store) => (next) => (action) => {
  next(action);
  if (action.type === 'user/setUser') {
    localStorage.setItem('user', JSON.stringify(store.getState().user.value));
  }
};

const middlewares = [setLocalStorage];

const store = configureStore({
  reducer: {
    user: userReducer,
    errors: errorsReducer,
    confirm: confirmReducer,
    sessionModal: sessionModalReducer,
    authToken: authTokenReducer,
    reauthModal: reauthModalReducer,
    genre: genreReducer,
    questionForm: questionFormReducer,
    mobileUserDropdown: mobileUserDropdownReducer,
  },
  preloadedState: { user: { value: JSON.parse(localStorage.getItem('user')) } },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  }).concat(...middlewares),
});

export default store;
