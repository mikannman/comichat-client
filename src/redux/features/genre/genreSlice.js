import { createSlice } from '@reduxjs/toolkit';

export const genreSlice = createSlice({
  name: 'genre',
  initialState: {
    data: [
      'ギャグ・コメディー',
      '学園',
      '恋愛・ラブコメ',
      'SF・ファンタジー',
      'セクシー',
      'スポーツ',
      'ヒューマンドラマ',
      '4コマ',
      '料理・グルメ',
      'バトル・アクション',
      'ミステリー・サスペンス',
      '歴史・時代劇',
      'BL',
      'TL',
      '動物・ペット',
      '車・バイク',
      '百合・GL',
      '政治・ビジネス',
      '刑事・ハードボイルド',
      'ホラー',
    ],
  },
  reducers: {},
});

export default genreSlice.reducer;
