import { createSlice } from '@reduxjs/toolkit';

export const mobileUserDropdownSlice = createSlice({
  name: 'mobileUserDropdown',
  initialState: {
    visible: false,
    menus: [],
    handleClickMenu: () => null,
    activeItem: '',
  },
  reducers: {
    setMobileUserDropdownConfig: (state, action) => ({ ...state, ...action.payload }),
    setMobileUserDropdownActiveItem: (state, action) => ({ ...state, activeItem: action.payload }),
    setMobileUserDropdownVisible: (state, action) => ({ ...state, visible: action.payload }),
  },
});

export const {
  setMobileUserDropdownConfig,
  setMobileUserDropdownActiveItem,
  setMobileUserDropdownVisible,
} = mobileUserDropdownSlice.actions;

export default mobileUserDropdownSlice.reducer;
