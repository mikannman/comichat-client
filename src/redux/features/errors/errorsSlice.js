import { createSlice } from '@reduxjs/toolkit';

export const errorsSlice = createSlice({
  name: 'errors',
  initialState: {
    validationErrors: {
      name: [],
      email: [],
    },
    communicationErrors: [],
  },
  reducers: {
    setValidationErrors: (state, action) => (
      {
        ...state,
        validationErrors:
        { ...state.validationErrors, ...action.payload },
      }
    ),
    resetValidationErrors: (state) => ({
      ...state,
      validationErrors: {
        name: [],
        email: [],
      },
    }),
    setCommunicationErrors: (state, action) => ({ ...state, communicationErrors: action.payload }),
    resetCommunicationErrors: (state) => ({ ...state, communicaitonErrors: [] }),
    resetAllErrors: (state) => ({
      ...state,
      validationErrors: {
        name: [],
        email: [],
      },
      communicationErrors: [],
    }),
  },
});

export const {
  setValidationErrors,
  resetValidationErrors,
  setCommunicationErrors,
  resetCommunicationErrors,
  resetAllErrors,
} = errorsSlice.actions;

export default errorsSlice.reducer;
