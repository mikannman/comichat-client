import { createSlice } from '@reduxjs/toolkit';

export const sessionModalSlice = createSlice({
  name: 'sessionModal',
  initialState: {
    input: {
      imageURL: '',
      userID: '',
      name: '',
      email: '',
      password: '',
      passwordConfirmation: '',
    },
    error: {
      imageURL: [],
      userID: [],
      name: [],
      email: [],
      password: [],
      passwordConfirmation: [],
      communication: [],
    },
    verifyUserId: 'green',
    state: {
      open: false,
      mode: '',
      nextPageOpen: false,
      forgotPasswordOpen: false,
    },
    warningText: '',
  },
  reducers: {
    setInput: (state, action) => ({ ...state, input: { ...state.input, ...action.payload } }),
    resetInput: (state) => ({
      ...state,
      input: {
        ...state.input,
        userID: '',
        name: '',
        email: '',
        password: '',
        passwordConfirmation: '',
      },
    }),
    setError: (state, action) => ({
      ...state,
      error: {
        ...state.error,
        ...action.payload,
      },
    }),
    setCommunicationError: (state, action) => ({
      ...state,
      error: {
        ...state.error,
        imageURL: [],
        userID: [],
        name: [],
        email: [],
        password: [],
        passwordConfirmation: [],
        communication: action.payload,
      },
    }),
    resetError: (state) => ({
      ...state,
      error: {
        ...state.error,
        imageURL: [],
        userID: [],
        name: [],
        email: [],
        password: [],
        passwordConfirmation: [],
        communication: [],
      },
    }),
    setVerifyUserId: (state, action) => ({ ...state, verifyUserId: action.payload }),
    setState: (state, action) => ({ ...state, state: { ...state.state, ...action.payload } }),
    goToSignUp: (state) => ({
      ...state,
      error: {
        imageURL: [],
        userID: [],
        name: [],
        email: [],
        password: [],
        passwordConfirmation: [],
        communication: [],
      },
      state: {
        open: true,
        mode: 'アカウントを作成',
        nextPageOpen: false,
        forgotPasswordOpen: false,
      },
    }),
    goToSignIn: (state) => ({
      ...state,
      error: {
        imageURL: [],
        userID: [],
        name: [],
        email: [],
        password: [],
        passwordConfirmation: [],
        communication: [],
      },
      state: {
        open: true,
        mode: 'ログイン',
        nextPageOpen: false,
        forgotPasswordOpen: false,
      },
    }),
    setWarningText: (state, action) => ({ ...state, warningText: action.payload }),
    allReset: (state) => ({
      ...state,
      input: {
        imageURL: '',
        userID: '',
        name: '',
        email: '',
        password: '',
        passwordConfirmation: '',
      },
      error: {
        imageURL: [],
        userID: [],
        name: [],
        email: [],
        password: [],
        passwordConfirmation: [],
        communication: [],
      },
      verifyUserId: 'green',
      state: {
        open: false,
        mode: '',
        nextPageOpen: false,
        forgotPasswordOpen: false,
      },
      warningText: '',
    }),
  },
});

export const {
  setInput, resetInput, setError, resetError, goToSignIn, goToSignUp,
  setCommunicationError, setVerifyUserId, setState, setWarningText, allReset,
} = sessionModalSlice.actions;

export default sessionModalSlice.reducer;
