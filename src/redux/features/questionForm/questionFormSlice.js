import { createSlice } from '@reduxjs/toolkit';

export const questionFormSlice = createSlice({
  name: 'questionForm',
  initialState: {
    inputData: {
      readingMangaList: [],
      readingMangaValue: '',
      readedMangaList: [],
      readedMangaValue: '',
      textareaValue: '',
      checkedAll: false,
      askedUser: {
        id: 0,
        name: '',
        imageURL: '',
        userId: '',
      },
      askedUserValue: '',
      isChanged: false,
    },
  },
  reducers: {
    setInputData: (state, action) => ({
      state, inputData: { ...state.inputData, ...action.payload, isChanged: true },
    }),
    setIsChanged: (state, action) => ({
      state, inputData: { ...state.inputData, isChanged: action.payload },
    }),
    addDataMangaList: (state, action) => ({
      state,
      inputData: {
        ...state.inputData,
        [`${action.payload.type}List`]: [
          ...state.inputData[`${action.payload.type}List`],
          action.payload.addData,
        ],
      },
    }),
    removeDataMangaList: (state, action) => ({
      state,
      inputData: {
        ...state.inputData,
        [`${action.payload.type}List`]: state.inputData[`${action.payload.type}List`].filter((listData) => (
          listData !== action.payload.removeData
        )),
      },
    }),
    allReset: (state) => ({
      ...state,
      inputData: {
        readingMangaList: [],
        readingMangaValue: '',
        readedMangaList: [],
        readedMangaValue: '',
        textareaValue: '',
        checkedAll: false,
        askedUser: {
          id: 0,
          name: '',
          imageURL: '',
          userId: '',
        },
        askedUserValue: '',
        isChanged: false,
      },
    }),
  },
});

export const {
  setInputData, allReset, addDataMangaList, removeDataMangaList, setIsChanged,
} = questionFormSlice.actions;

export default questionFormSlice.reducer;
