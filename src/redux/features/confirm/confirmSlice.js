import { createSlice } from '@reduxjs/toolkit';

export const confirmSlice = createSlice({
  name: 'confirm',
  initialState: {
    open: false,
    content: '',
    onConfirm: null,
    onCancel: () => null,
  },
  reducers: {
    setConfirmConfig: (state, action) => ({ ...state, ...action.payload }),
    setConfirmOpen: (state, action) => ({ ...state, open: action.payload }),
  },
});

export const { setConfirmConfig, setConfirmOpen } = confirmSlice.actions;

export default confirmSlice.reducer;
