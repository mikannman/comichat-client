import { createSlice } from '@reduxjs/toolkit';

export const authTokenSlice = createSlice({
  name: 'authToken',
  initialState: {
    token: '',
  },
  reducers: {
    setToken: (state, action) => ({ ...state, token: action.payload }),
  },
});

export const { setToken } = authTokenSlice.actions;

export default authTokenSlice.reducer;
