import { createSlice } from '@reduxjs/toolkit';

export const reauthModalSlice = createSlice({
  name: 'reauthModal',
  initialState: {
    open: false,
    callback: null,
  },
  reducers: {
    setOpen: (state, action) => ({ ...state, open: action.payload }),
    setCallback: (state, action) => ({ ...state, callback: action.payload }),
  },
});

export const { setOpen, setCallback } = reauthModalSlice.actions;

export default reauthModalSlice.reducer;
