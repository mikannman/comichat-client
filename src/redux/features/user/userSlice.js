import { createSlice } from '@reduxjs/toolkit';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    value: null,
    isLogin: false,
    onMyPage: false,
  },
  reducers: {
    setUser: (state, action) => ({ ...state, value: action.payload }),
    setIsLogin: (state, action) => ({ ...state, isLogin: action.payload }),
    setOnMyPage: (state, action) => ({ ...state, onMyPage: action.payload }),
    resetUser: (state) => ({ ...state, value: null, onMyPage: false }),
  },
});

export const {
  setUser, resetUser, setIsLogin, setOnMyPage,
} = userSlice.actions;

export default userSlice.reducer;
