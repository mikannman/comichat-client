import { InMemoryCache } from '@apollo/client';

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        getAllQuestionsByCondition: {
          keyArgs: ['condition'],

          merge(existing, incoming, {
            args: {
              CREATE = false,
            },
          }) {
            if (CREATE) {
              const merged = incoming.edges.slice(0).slice(0, 5);
              return {
                ...existing,
                ...incoming,
                edges: merged,
              };
            }

            return {
              ...existing,
              ...incoming,
            };
          },
        },
        getReviews: {
          keyArgs: ['mid', 'after'],

          merge(existing, incoming, {
            args: {
              DELETE = false, CREATE = false,
            },
          }) {
            if (DELETE || CREATE) {
              return {
                ...existing,
                ...incoming,
                edges: incoming.edges,
              };
            }

            return {
              ...existing,
              ...incoming,
            };
          },
        },
        getShowQuestionAnswers: {
          keyArgs: ['id'],

          merge(existing, incoming, {
            args: {
              DELETE = false, CREATE = false,
            },
          }) {
            if (DELETE || CREATE) {
              return {
                ...existing,
                ...incoming,
                edges: incoming.edges,
              };
            }

            return {
              ...existing,
              ...incoming,
            };
          },
        },
        getNotifications: {
          keyArgs: false,

          merge(existing, incoming, {
            args: {
              after = btoa(String(0)), DELETE = false,
            },
          }) {
            if (DELETE) {
              return {
                ...existing,
                ...incoming,
                edges: incoming.edges,
              };
            }

            const merged = existing ? existing.edges.slice(0) : [];
            for (let i = 0; i < incoming.edges.length; i += 1) {
              merged[Number(atob(String(after))) + i] = incoming.edges[i];
            }

            return {
              ...existing,
              ...incoming,
              edges: merged,
            };
          },
        },
        getQuestionAnswers: {
          keyArgs: ['id'],

          read(existing) {
            return existing;
          },

          merge(existing, incoming, {
            args: {
              after = btoa(String(0)), DELETE = false, CREATE = false,
            },
          }) {
            if (DELETE || CREATE) {
              return {
                ...existing,
                ...incoming,
                edges: incoming.edges,
              };
            }
            const merged = existing ? existing.edges.slice(0) : [];
            for (let i = 0; i < incoming.edges.length; i += 1) {
              merged[Number(atob(String(after))) + i] = incoming.edges[i];
            }

            return {
              ...existing,
              ...incoming,
              edges: merged,
            };
          },
        },
        getAnswerReplies: {
          keyArgs: ['id'],

          read(existing) {
            return existing;
          },

          merge(
            existing,
            incoming,
            { args: { after = btoa(String(0)), DELETE = false, CREATE = false } },
          ) {
            if (DELETE || CREATE) {
              return {
                ...existing,
                ...incoming,
                edges: incoming.edges,
              };
            }
            const merged = existing ? existing.edges.slice(0) : [];
            for (let i = 0; i < incoming.edges.length; i += 1) {
              merged[Number(atob(String(after))) + i] = incoming.edges[i];
            }

            return {
              ...existing,
              ...incoming,
              edges: merged,
            };
          },
        },
      },
    },
    User: {
      fields: {
        following: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
        followers: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
        reviews: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
        answers: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
      },
    },
    Answer: {
      fields: {
        replies: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
        likeAnswers: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
        currentUserLikes: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
      },
    },
    Question: {
      fields: {
        answers: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
      },
    },
    Review: {
      fields: {
        likeReviews: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
      },
    },
    Manga: {
      keyFields: ['mid'],
      fields: {
        currentUserWannaRead: {
          merge(existing, incoming) {
            return { ...existing, ...incoming };
          },
        },
      },
    },
  },
});

export default cache;
