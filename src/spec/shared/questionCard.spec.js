import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, screen } from '../test-utils';
import {
  questionMock, questionAnswerMock, createAnswerMock,
} from '../mocks/shared/questionCardMock';
import QuestionCard from '../../components/shared/QuestionCard';

const refetch = () => null;

describe('<QuestionCard />', () => {
  it('Can answer', async () => {
    render(
      <QuestionCard
        question={questionMock}
        refetch={refetch}
      />, {
        wrapperProps: {
          mocks: [
            questionAnswerMock,
            createAnswerMock,
          ],
          isLogin: true,
        },
      },
    );

    const testContent = await screen.findByText('testContent');
    await screen.findByText('answeredUserName');

    const showAnswer = screen.getByText('回答を見る');
    userEvent.click(showAnswer);
    const testAnswer = await screen.findByText('testAnswer');

    const textarea = screen.getByRole('textbox');
    userEvent.type(textarea, 'testCreateAnswer');

    const submit = screen.getByText('送信');
    userEvent.click(submit);
    await screen.findAllByText('createAnswerUser');

    expect(testContent).toBeInTheDocument();
    expect(testAnswer).toBeInTheDocument();
  });
});
