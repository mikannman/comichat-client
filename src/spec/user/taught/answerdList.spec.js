import React from 'react';
import userEvent from '@testing-library/user-event';
import { gql } from '@apollo/client';
import { render, screen } from '../../test-utils';
import {
  answeredListMock, answerAreaMock, likesMock, dislikesMock,
  secondAnsweredListMock,
} from '../../mocks/taught/answeredListMock';
import cache from '../../../apollo/cache';
import AnsweredList from '../../../components/user/taught/AnsweredList';

const answerLikeTestFragment = gql`
  fragment answerLikeTestRef on Answer {
    id
    likeAnswers {
      count
    }
    currentUserLikes {
      isExist
    }
  }
`;

describe('<AnsweredList />', () => {
  it('Can like', async () => {
    render(
      <AnsweredList id="1" />, {
        wrapperProps: {
          mocks: [
            answeredListMock,
            answerAreaMock,
            likesMock,
            dislikesMock,
            secondAnsweredListMock,
          ],
          isLogin: true,
        },
      },
    );

    const testContent = await screen.findByText('testContent');
    await screen.findByText('userName');

    const showAnswerButton = screen.getByText('回答を見る');
    userEvent.click(showAnswerButton);

    const like = screen.getByTestId('like-1');

    expect(
      cache.readFragment({
        id: 'Answer:1',
        fragment: answerLikeTestFragment,
      }).currentUserLikes.isExist,
    ).toBeFalsy();
    expect(
      cache.readFragment({
        id: 'Answer:1',
        fragment: answerLikeTestFragment,
      }).likeAnswers.count,
    ).toEqual(1);

    userEvent.click(like);

    await screen.findByText('2');

    expect(
      cache.readFragment({
        id: 'Answer:1',
        fragment: answerLikeTestFragment,
      }).currentUserLikes.isExist,
    ).toBeTruthy();
    expect(
      cache.readFragment({
        id: 'Answer:1',
        fragment: answerLikeTestFragment,
      }).likeAnswers.count,
    ).toEqual(2);

    expect(testContent).toBeInTheDocument();
  });
});
