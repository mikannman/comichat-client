import { GET_POPULAR_USERS } from '../../../graphql/topQueries';

export const popularUsersMock = {
  request: {
    query: GET_POPULAR_USERS,
  },
  result: {
    data: {
      getPopularUsers: [
        {
          __typename: 'User',
          id: '3',
          userId: 'testId',
          name: 'testName',
          imageURL: '',
        },
      ],
    },
  },
};
