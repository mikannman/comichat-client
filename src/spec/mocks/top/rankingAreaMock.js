import { REGISTER_WANNA_READ_MANGA } from '../../../graphql/mangaQueries';
import { GET_SMALL_MANGA_RANKING } from '../../../graphql/rankingQueries';

export const rankingAreaMock = {
  request: {
    query: GET_SMALL_MANGA_RANKING,
    variables: {
      type: 'weeklyRating',
      first: 5,
      genre: '',
    },
  },
  result: {
    data: {
      getMangaRanking: {
        __typename: 'MangaConnection',
        edges: [
          {
            __typename: 'MangaEdge',
            node: {
              __typename: 'Manga',
              mid: 'testMid',
              title: 'testTitle',
              currentUserWannaRead: {
                __typename: 'ExtensionIsExist',
                isExist: false,
              },
              currentUserReaded: {
                __typename: 'ExtensionIsExist',
                isExist: false,
              },
              reviews: {
                __typename: 'ExtensionAverage',
                average: 2.5,
              },
            },
          },
        ],
      },
    },
  },
};

export const registerWannaReadMock = {
  request: {
    query: REGISTER_WANNA_READ_MANGA,
    variables: { mid: 'testMid' },
  },
  result: {
    data: {
      registerWannaReadManga: {
        success: true,
      },
    },
  },
};
