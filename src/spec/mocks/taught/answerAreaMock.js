import { GET_QUESTION_ANSWERS } from '../../../graphql/answerQueries';
import { LIKES, DISLIKES } from '../../../graphql/likeQueries';

export const likesMock = {
  request: {
    query: LIKES,
    variables: {
      id: '1',
      type: 'Answer',
    },
  },
  result: {
    data: {
      likes: {
        succcess: true,
      },
    },
  },
};

export const dislikesMock = {
  request: {
    query: DISLIKES,
    variables: {
      id: '1',
      type: 'Answer',
    },
  },
  result: {
    data: {
      dislikes: {
        succcess: true,
      },
    },
  },
};

export const answerAreaMock = {
  request: {
    query: GET_QUESTION_ANSWERS,
    variables: {
      id: '1',
      first: 5,
      after: 'MA==',
    },
  },
  result: {
    data: {
      getQuestionAnswers: {
        __typename: 'AnswerConnection',
        edges: [
          {
            __typename: 'AnswerEdge',
            node: {
              __typename: 'Answer',
              id: '1',
              content: 'testAnswer',
              updatedAt: '2020-01-01',
              replies: {
                __typename: 'ExtensionCount',
                count: 0,
              },
              likeAnswers: {
                __typename: 'ExtensionCount',
                count: 1,
              },
              user: {
                __typename: 'User',
                id: '1',
                name: 'userName',
                imageURL: '',
              },
              currentUserLikes: {
                __typename: 'ExtensionIsExist',
                isExist: false,
              },
            },
          },
        ],
        pageInfo: {
          hasNextPage: false,
          hasPreviousPage: false,
        },
        totalCount: 1,
      },
    },
  },
};
