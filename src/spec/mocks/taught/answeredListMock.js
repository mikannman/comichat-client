import { GET_ANSWERED_LIST } from '../../../graphql/taughtQueries';
import { GET_QUESTION_ANSWERS } from '../../../graphql/answerQueries';
import { LIKES, DISLIKES } from '../../../graphql/likeQueries';

export const answeredListMock = {
  request: {
    query: GET_ANSWERED_LIST,
    variables: {
      id: '1',
      first: 5,
    },
  },
  result: {
    data: {
      getAnsweredList: {
        __typename: 'QuestionConnection',
        edges: [
          {
            __typename: 'QuestionEdge',
            node: {
              __typename: 'Question',
              id: '1',
              genres: [
                { genre: 'testGenre' },
              ],
              readingMangaMid: 'test',
              readedMangaMid: 'test',
              content: 'testContent',
              all: true,
              updatedAt: '2020-01-07',
              createdAt: '2020-01-07',
              askUser: {
                __typename: 'User',
                id: '1',
                name: 'askUser',
                imageURL: '',
              },
              askedUser: {
                __typename: 'User',
                id: '2',
                name: 'askedUser',
                imageURL: '',
              },
            },
          },
        ],
        totalCount: 1,
      },
    },
  },
};

export const secondAnsweredListMock = answeredListMock;

export const answerAreaMock = {
  request: {
    query: GET_QUESTION_ANSWERS,
    variables: {
      id: '1',
      first: 5,
      after: 'MA==',
    },
  },
  result: {
    data: {
      getQuestionAnswers: {
        __typename: 'AnswerConnection',
        edges: [
          {
            __typename: 'AnswerEdge',
            node: {
              __typename: 'Answer',
              id: '1',
              content: 'testAnswer',
              updatedAt: '2020-01-01',
              replies: {
                __typename: 'ExtensionCount',
                count: 0,
              },
              likeAnswers: {
                __typename: 'ExtensionCount',
                count: 1,
              },
              user: {
                __typename: 'User',
                id: '1',
                name: 'userName',
                imageURL: '',
              },
              currentUserLikes: {
                __typename: 'ExtensionIsExist',
                isExist: false,
              },
            },
          },
        ],
        pageInfo: {
          hasNextPage: false,
          hasPreviousPage: false,
        },
        totalCount: 1,
      },
    },
  },
};

export const likesMock = {
  request: {
    query: LIKES,
    variables: {
      id: '1',
      type: 'Answer',
    },
  },
  result: {
    data: {
      likes: {
        succcess: true,
      },
    },
  },
};

export const dislikesMock = {
  request: {
    query: DISLIKES,
    variables: {
      id: '1',
      type: 'Answer',
    },
  },
  result: {
    data: {
      dislikes: {
        succcess: true,
      },
    },
  },
};
