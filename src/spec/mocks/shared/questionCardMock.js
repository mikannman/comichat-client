import { GET_QUESTION_ANSWERS, CREATE_ANSWER } from '../../../graphql/answerQueries';

export const questionMock = {
  id: '1',
  genres: [
    { genre: 'ホラー' },
  ],
  readingMangaMid: 'テスト,サンプル',
  readedMangaMid: 'テス,サンプ',
  content: 'testContent',
  all: true,
  updatedAt: '2020-01-01',
  askUser: {
    id: '2',
    name: 'askUser',
    imageURL: '',
  },
  askedUser: null,
};

export const questionAnswerMock = {
  request: {
    query: GET_QUESTION_ANSWERS,
    variables: {
      id: '1',
      first: 5,
      after: 'MA==',
    },
  },
  result: {
    data: {
      getQuestionAnswers: {
        __typename: 'AnswerConnection',
        edges: [
          {
            __typename: 'AnswerEdge',
            node: {
              __typename: 'Answer',
              id: '1',
              content: 'testAnswer',
              updatedAt: '2020-01-01',
              replies: {
                __typename: 'ExtensionCount',
                count: 0,
              },
              likeAnswers: {
                __typename: 'ExtensionCount',
                count: 1,
              },
              user: {
                __typename: 'User',
                id: '5',
                name: 'answeredUserName',
                imageURL: '',
              },
              currentUserLikes: {
                __typename: 'ExtensionIsExist',
                isExist: false,
              },
            },
          },
        ],
        pageInfo: {
          hasNextPage: false,
          hasPreviousPage: false,
        },
        totalCount: 1,
      },
    },
  },
};

export const createAnswerMock = {
  request: {
    query: CREATE_ANSWER,
    variables: {
      questionId: '1',
      questionUserId: '2',
      content: 'testCreateAnswer',
    },
  },
  result: {
    data: {
      createAnswer: {
        __typename: 'CreateAnswerPayload',
        answer: {
          __typename: 'Answer',
          id: '3',
          content: 'testCreateAnswer',
          updatedAt: '2020-01-01',
          replies: {
            count: 0,
          },
          likeAnswers: {
            __typename: 'ExtensionCount',
            count: 0,
          },
          user: {
            __typename: 'User',
            id: '4',
            name: 'createAnswerUser',
            imageURL: '',
          },
          currentUserLikes: {
            __typename: 'ExtensionIsExist',
            isExist: false,
          },
        },
      },
    },
  },
};
