import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { useQuery } from '@apollo/client';
import { MockedProvider } from '@apollo/client/testing';
import { MemoryRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import PropTypes from 'prop-types';
import cache from '../apollo/cache';
import { GET_USER } from '../graphql/userQueries';

const createStore = configureStore();

const initialState = {
  user: {
    value: null,
    isLogin: false,
  },
  authToken: {
    token: '',
  },
  confirm: {
    open: false,
    content: '',
    onConfirm: null,
  },
  errors: {
    validationErrors: {
      name: [],
      email: [],
    },
    communicationErrors: [],
  },
  reauthModal: {
    open: false,
    callback: null,
  },
  sessionModal: {
    input: {
      imageURL: '',
      userID: '',
      name: '',
      email: '',
      password: '',
      passwordConfirmation: '',
    },
    error: {
      imageURL: [],
      userID: [],
      name: [],
      email: [],
      password: [],
      passwordConfirmation: [],
      communication: [],
    },
    verifyUserId: 'green',
    state: {
      open: false,
      mode: '',
      nextPageOpen: false,
      forgotPasswordOpen: false,
    },
    warningText: '',
  },
};

const loggedInStore = createStore({
  ...initialState,
  user: {
    value: {
      id: '1',
      name: 'testUser',
      userId: 'testUserId',
      imageURL: '',
    },
    isLogin: true,
  },
});

const notLoggedInStore = createStore(initialState);

const loggedInUserMock = {
  request: {
    query: GET_USER,
    variables: {
      id: '1',
    },
  },
  result: {
    data: {
      getUser: {
        __typename: 'User',
        id: '1',
        name: 'testUser',
        userId: 'testUserId',
        imageURL: '',
        email: 'example@sample.com',
        message: 'sample message',
        following: {
          __typename: 'ExtensionCount',
          count: 0,
        },
        followers: {
          __typename: 'ExtensionCount',
          count: 0,
        },
        reviews: {
          __typename: 'ExtensionCount',
          count: 0,
        },
        answers: {
          __typename: 'ExtensionCount',
          count: 0,
        },
        currentUserFollowed: {
          __typename: 'ExtensionIsExist',
          isExist: false,
        },
      },
    },
  },
};

const LoggedInComponent = ({ children }) => {
  useQuery(GET_USER, {
    variables: { id: '1' },
  });

  return children;
};

const AllTheProviders = ({ children }, options = {}) => {
  const store = options.isLogin ? loggedInStore : notLoggedInStore;
  window.flash = (messageArg, typeArg = 'success') => {
    console.log(messageArg);
    console.log(typeArg);
  };

  return (
    <Provider store={store}>
      <MemoryRouter initialEntries={['/']}>
        <MockedProvider
          mocks={
            options.isLogin
              ? [...options.mocks, loggedInUserMock]
              : options.mocks
          }
          cache={cache}
        >
          {
            options.isLogin
              ? <LoggedInComponent children={children} />
              : children
          }
        </MockedProvider>
      </MemoryRouter>
    </Provider>
  );
};

AllTheProviders.propTypes = {
  children: PropTypes.element.isRequired,
};

const customRender = (ui, options) => {
  render(ui, { wrapper: (props) => AllTheProviders(props, options.wrapperProps), ...options });
};

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render, loggedInStore, notLoggedInStore };
