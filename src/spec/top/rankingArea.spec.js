import React from 'react';
import userEvent from '@testing-library/user-event';
import { gql } from '@apollo/client';
import { render, screen } from '../test-utils';
import cache from '../../apollo/cache';
import RankingArea from '../../components/top/RankingArea';
import { rankingAreaMock, registerWannaReadMock } from '../mocks/top/rankingAreaMock';

const mangaRegisterTestFragment = gql`
  fragment mangaTestRef on Manga {
    mid
    currentUserReaded {
      isExist
    }
    currentUserWannaRead {
      isExist
    }
  }
`;

describe('<AankingArea />', () => {
  it('changes to registered when you press the register button', async () => {
    render(
      <RankingArea
        type="weeklyRating"
        title="今週はこのまんががおもしろい！！"
        first={5}
      />, {
        wrapperProps: {
          mocks: [rankingAreaMock, registerWannaReadMock],
          isLogin: true,
        },
      },
    );

    const testTitle = await screen.findByText('testTitle');

    const wannaRead = screen.getByText('読みたい');

    expect(
      cache.readFragment({
        id: 'Manga:{"mid":"testMid"}',
        fragment: mangaRegisterTestFragment,
      }).currentUserWannaRead.isExist,
    ).toBeFalsy();

    userEvent.click(wannaRead);

    await screen.findByText('読みたいを解除');

    expect(
      cache.readFragment({
        id: 'Manga:{"mid":"testMid"}',
        fragment: mangaRegisterTestFragment,
      }).currentUserWannaRead.isExist,
    ).toBeTruthy();
    expect(testTitle).toBeInTheDocument();
  });

  it('does not change to registered when you press the register button', async () => {
    await cache.reset();

    render(
      <RankingArea
        type="weeklyRating"
        title="今週はこのまんががおもしろい！！"
        first={5}
      />, {
        wrapperProps: {
          mocks: [rankingAreaMock, registerWannaReadMock],
          isLogin: false,
        },
      },
    );

    const testTitle = await screen.findByText('testTitle');

    const wannaRead = screen.getByText('読みたい');

    expect(
      cache.readFragment({
        id: 'Manga:{"mid":"testMid"}',
        fragment: mangaRegisterTestFragment,
      }).currentUserWannaRead.isExist,
    ).toBeFalsy();

    userEvent.click(wannaRead);

    expect(
      cache.readFragment({
        id: 'Manga:{"mid":"testMid"}',
        fragment: mangaRegisterTestFragment,
      }).currentUserWannaRead.isExist,
    ).toBeFalsy();
    expect(testTitle).toBeInTheDocument();
  });
});
