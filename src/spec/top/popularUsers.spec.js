import React from 'react';
import { render, screen } from '../test-utils';
import PopularUsers from '../../components/top/PopularUsers';
import { popularUsersMock } from '../mocks/top/popularUsersMock';

describe('<PopularUsers />', () => {
  it('Can render normally', async () => {
    render(
      <PopularUsers />, {
        wrapperProps: {
          mocks: [popularUsersMock],
        },
      },
    );

    const test = await screen.findByText('testName');

    expect(test).toBeInTheDocument();
  });
});
