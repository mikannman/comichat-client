import { gql } from '@apollo/client';
import { RequiredManga } from './fragments';

export const RATING_MANGA = gql`
  mutation RatingManga($mid: String!, $rate: String!) {
    ratingManga(input: {
      mid: $mid
      rate: $rate
    })
    {
      rating {
        rate
      }
    }
  }
`;

export const GET_MANGA = gql`
  query GetManga($mid: String!) {
    getManga(mid: $mid) {
      ...RequiredMangaParams
    }
  }
  ${RequiredManga.fragments.params}
`;

export const REGISTER_WANNA_READ_MANGA = gql`
  mutation registerWannaReadManga($mid: String!) {
    registerWannaReadManga(input: {
      mid: $mid
    })
    {
      success
    }
  }
`;

export const UNREGISTER_WANNA_READ_MANGA = gql`
  mutation unregisterWannaReadManga($mid:String!) {
    unregisterWannaReadManga(input: {
      mid: $mid
    })
    {
      success
    }
  }
`;

export const REGISTER_READED_MANGA = gql`
  mutation registerReadedManga($mid: String!) {
    registerReadedManga(input: {
      mid: $mid
    })
    {
      success
    }
  }
`;

export const UNREGISTER_READED_MANGA = gql`
  mutation unregisterReadedManga($mid: String!) {
    unregisterReadedManga(input: {
      mid: $mid
    })
    {
      success
    }
  }
`;

export const ADD_IMAGE_URL_AND_AFFILIATE_URL = gql`
  mutation addImageUrlAndAffiliateUrl($mid: String!, $imageUrl: String, $affiliateUrl: String){
    addImageUrlAndAffiliateUrl(input: {
      mid: $mid
      imageUrl: $imageUrl
      affiliateUrl: $affiliateUrl
    })
    {
      success
    }
  }
`;
