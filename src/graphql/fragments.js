import { gql } from '@apollo/client';

export const RequiredUser = {
  fragments: {
    params: gql`
      fragment RequiredUserParams on User {
        id
        userId
        name
        imageURL
        guest
      }
    `,
  },
};

export const UserSubInfo = {
  fragments: {
    count: gql`
      fragment UserSubInfoCount on User {
        following {
          count
        }
        followers {
          count
        }
        reviews {
          count
        }
        answers {
          count
        }
      }
    `,
  },
};

export const RequiredManga = {
  fragments: {
    params: gql`
      fragment RequiredMangaParams on Manga {
        mid
        title
        synopsis
        datePublished
        publisher
        imageUrl
        affiliateUrl
        creators {
          creator
        }
        genres {
          genre
        }
        labels {
          label
        }
        currentUserWannaRead {
          isExist
        }
        currentUserReaded {
          isExist
        }
        currentUserRated {
          rate
        }
        reviews {
          average
        }
      }
    `,
  },
};

export const RequiredSmallManga = {
  fragments: {
    params: gql`
      fragment RequiredSmallMangaParams on Manga {
        mid
        title
        imageUrl
        affiliateUrl
        currentUserWannaRead {
          isExist
        }
        currentUserReaded {
          isExist
        }
        reviews {
          average
        }
      }
    `,
  },
};

export const RequiredQuestion = {
  fragments: {
    params: gql`
      fragment RequiredQuestionParams on Question {
        id
        genres {
          genre
        }
        readingMangaMid
        readedMangaMid
        content
        all
        updatedAt
        createdAt
        askUser {
          id
          name
          userId
          imageURL
          guest
        }
        askedUser {
          id
          name
          userId
          imageURL
          guest
        }
      }
    `,
  },
};

export const RequiredAnswer = {
  fragments: {
    params: gql`
      fragment RequiredAnswerParams on Answer {
        id
        content
        updatedAt
        replies {
          count
        }
        likeAnswers {
          count
        }
        user {
          id
          name
          imageURL
          guest
        }
        currentUserLikes {
          isExist
        }
      }
    `,
  },
};

export const RequiredReply = {
  fragments: {
    params: gql`
      fragment RequiredReplyParams on Reply {
        id
        content
        updatedAt
        likeReplies {
          count
        }
        user {
          id
          name
          imageURL
          guest
        }
        currentUserLikes {
          isExist
        }
      }
    `,
  },
};

export const RequiredReview = {
  fragments: {
    params: gql`
      fragment RequiredReviewParams on Review {
        id
        comment
        star
        netabare
        updatedAt
        likeReviews {
          count
        }
        currentUserLikes {
          isExist
        }
        user {
          id
          userId
          name
          imageURL
          guest
        }
        manga {
          mid
          title
          imageUrl
          affiliateUrl
        }
      }
    `,
  },
};
