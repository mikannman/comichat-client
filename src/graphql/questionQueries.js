import { gql } from '@apollo/client';
import { RequiredQuestion } from './fragments';

export const CREATE_QUESTION = gql`
  mutation CreateQuestion(
    $askedUserId: Int,
    $genres: [String!],
    $readingMangaMid: String,
    $readedMangaMid: String,
    $content: String,
    $all: Boolean,
  ) {
    createQuestion(input: {
      askedUserId: $askedUserId,
      genres: $genres,
      readingMangaMid: $readingMangaMid,
      readedMangaMid: $readedMangaMid,
      content: $content,
      all: $all,
    })
    {
      question {
        id
        askUserId
        askedUserId
        genres {
          genre
        }
        readingMangaMid
        readedMangaMid
        content
        all
      }
    }
  }
`;

export const UPDATE_QUESTION = gql`
  mutation UpdateQuestion(
    $id: ID!,
    $askedUserId: Int,
    $genres: [String!],
    $readingMangaMid: String,
    $readedMangaMid: String,
    $content: String,
    $all: Boolean
  ) {
    updateQuestion(input: {
      id: $id,
      askedUserId: $askedUserId,
      genres: $genres,
      readingMangaMid: $readingMangaMid,
      readedMangaMid: $readedMangaMid,
      content: $content,
      all: $all,
    })
    {
      question {
        id
        askUserId
        askedUserId
        genres {
          genre
        }
        readingMangaMid
        readedMangaMid
        content
        all
      }
    }
  }
`;

export const DELETE_QUESTION = gql`
  mutation DeleteQuestion($id: ID!) {
    deleteQuestion(input: {
      id: $id
    })
    {
      success
    }
  }
`;

export const GET_SMALL_ALL_QUESTIONS_BY_CONDITION = gql`
  query GetSmallAllQuestionsByCondition($first: Int, $after: String, $condition: String!) {
    getAllQuestionsByCondition(first: $first, after: $after, condition: $condition) {
      edges {
        node {
          id
          content
          askUser {
            id
            name
            userId
            imageURL
            guest
          }
        }
      }
    }
  }
`;

export const GET_ALL_QUESTIONS_BY_CONDITION = gql`
  query GetAllQuestionsByCondition($first: Int, $after: String, $condition: String!, $genre: String) {
    getAllQuestionsByCondition(first: $first, after: $after, condition: $condition, genre: $genre) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;

export const GET_QUESTION = gql`
  query GetQuestion($id: ID!) {
    getQuestion(id: $id) {
      ...RequiredQuestionParams
    }
  }
  ${RequiredQuestion.fragments.params}
`;
