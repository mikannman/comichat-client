import { gql } from '@apollo/client';
import {
  RequiredUser, UserSubInfo, RequiredQuestion,
  RequiredReview,
} from './fragments';

export const GET_USER = gql`
  query GetUser($id: ID!) {
    getUser(id: $id) {
      ...RequiredUserParams
      email
      message
      ...UserSubInfoCount
      currentUserFollowed {
        isExist
      }
    }
  }
  ${RequiredUser.fragments.params}
  ${UserSubInfo.fragments.count}
`;

export const GET_RECOMMENDED_USERS = gql`
  query GetRecommendedUsers {
    getRecommendedUsers {
      ...RequiredUserParams
    }
  }
  ${RequiredUser.fragments.params}
`;

export const GET_TOLD_QUESTIONS = gql`
  query GetToldQuestions($id: ID!, $first: Int, $after: String) {
    getToldQuestions(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;

export const GET_FOLLOWING = gql`
  query GetFollwing($id: ID!, $first: Int, $after: String) {
    getFollowing(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredUserParams
          message
          ...UserSubInfoCount
          currentUserFollowed {
            isExist
          }
        }
      }
      totalCount
    }
  }
  ${RequiredUser.fragments.params}
  ${UserSubInfo.fragments.count}
`;

export const GET_FOLLOWERS = gql`
  query GetFollwers($id: ID!, $first: Int, $after: String) {
    getFollowers(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredUserParams
          message
          ...UserSubInfoCount
          currentUserFollowed {
            isExist
          }
        }
      }
      totalCount
    }
  }
  ${RequiredUser.fragments.params}
  ${UserSubInfo.fragments.count}
`;

export const GET_USER_REVIEWS = gql`
  query GetUserReviews($id: ID!, $first: Int, $after: String) {
    getUserReviews(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredReviewParams
        }
      }
      totalCount
    }
  }
  ${RequiredReview.fragments.params}
`;

export const UPDATE_USER = gql`
  mutation UpdateUser(
    $userId: String!,
    $name: String!,
    $imageURL: String,
    $message: String
  ) {
    updateUser(input: {
      userId: $userId
      name: $name
      imageURL: $imageURL
      message: $message
    })
    {
      user {
        ...RequiredUserParams
        email
        message
        provider
      }
    }
  }
  ${RequiredUser.fragments.params}
`;

export const UPDATE_EMAIL = gql`
  mutation UpdateEmail(
    $email: String!
  ) {
    updateEmail(input: {
      email: $email
    })
    {
      user {
        ...RequiredUserParams
        email
        message
        provider
      }
    }
  }
  ${RequiredUser.fragments.params}
`;

export const DELETE_USER = gql`
  mutation deleteUser {
    deleteUser(input: {}) {
      success
    }
  }
`;

export const FOLLOW = gql`
  mutation follow($otherId: ID!) {
    follow(input: {
      otherId: $otherId
    })
    {
      success
    }
  }
`;

export const UNFOLLOW = gql`
  mutation unfollow($otherId: ID!) {
    unfollow(input: {
      otherId: $otherId
    })
    {
      success
    }
  }
`;

export const ALL_WANNA_READ_MANGAS = gql`
  query allWannaReadMangas($id:ID!, $first: Int, $after: String) {
    allWannaReadMangas(id: $id, first: $first, after: $after) {
      edges {
        node {
          mid
          title
          datePublished
          publisher
        }
      }
      totalCount
    }
  }
`;

export const ALL_READED_MANGAS = gql`
  query allReadedMangas($id:ID!, $first: Int, $after: String) {
    allReadedMangas(id: $id, first: $first, after: $after) {
      edges {
        node {
          mid
          title
          datePublished
          publisher
        }
      }
      totalCount
    }
  }
`;
