import { gql } from '@apollo/client';
import { RequiredReply } from './fragments';

export const GET_ANSWER_REPLIES = gql`
  query GetAnswerReplies($id: ID!, $first: Int, $after: String) {
    getAnswerReplies(id: $id, first: $first, after: $after) {
      edges {
        node{
          ...RequiredReplyParams
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
  ${RequiredReply.fragments.params}
`;

export const CREATE_REPLY = gql`
  mutation CreateReply(
    $answerId: ID!,
    $answerUserId: ID!,
    $content: String!
  ) {
    createReply(input: {
      answerId: $answerId,
      answerUserId: $answerUserId,
      content: $content
    })
    {
      reply {
        id
        content
        updatedAt
        user {
          id
          name
          imageURL
        }
      }
    }
  }
`;

export const UPDATE_REPLY = gql`
  mutation UpdateReply(
    $id: ID!,
    $content: String
  ) {
    updateReply(input: {
      id: $id,
      content: $content
    })
    {
      reply {
        id
        content
      }
    }
  }
`;

export const DELETE_REPLY = gql`
  mutation DeleteReply($id: ID!) {
    deleteReply(input: {
      id: $id
    })
    {
      success
    }
  }
`;
