import { gql } from '@apollo/client';
import { RequiredUser } from './fragments';

export const SIGN_UP_OR_IN = gql`
  mutation SignUpOrIn(
    $name: String,
    $email: String!,
    $userId: String,
    $imageURL: String,
    $provider: String!
  ) {
    signUpOrIn(input: {
      name: $name
      email: $email
      userId: $userId
      imageURL: $imageURL
      provider: $provider
    })
    {
      user {
        ...RequiredUserParams
        email
        message
        provider
        guest
      }
    }
  }
  ${RequiredUser.fragments.params}
`;

export const VERIFY_EMAIL = gql`
  mutation VerifyEmail(
    $email: String!
  ) {
    verifyEmail(input: {
      email: $email,
    })
    {
      registered
      userId
    }
  }
`;

export const VERIFY_USER_ID = gql`
  query VerifyUserId($userId: String!) {
    verifyUserId(userId: $userId) {
      type
      text
    }
  }
`;
