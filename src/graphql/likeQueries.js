import { gql } from '@apollo/client';

export const LIKES = gql`
  mutation Likes($id: ID!, $type: String!) {
    likes(input: {
      id: $id,
      type: $type
    })
    {
      success
    }
  }
`;

export const DISLIKES = gql`
  mutation Dislikes($id: ID!, $type: String!) {
    dislikes(input: {
      id: $id,
      type: $type
    })
    {
      success
    }
  }
`;
