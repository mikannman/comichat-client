import { gql } from '@apollo/client';

export const GET_NOTIFICATIONS = gql`
  query GetNotifications($first: Int, $after: String) {
    getNotifications(first: $first, after: $after) {
      edges {
        node {
          id
          visitor {
            id
            name
            imageURL
          }
          question {
            id
            content
          }
          answer {
            content
            questionId
          }
          reply {
            content
            answer {
              questionId
            }
          }
          review {
            comment
            mid
          }
          action
        }
      }
      totalCount
    }
  }
`;

export const CHECKED_NOTIFICATIONS = gql`
  mutation CheckedNotifications($ids: [ID!]) {
    checkedNotifications(input: {
      ids: $ids
    })
    {
      checkedIds
    }
  }
`;
