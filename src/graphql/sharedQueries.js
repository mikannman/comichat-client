import { gql } from '@apollo/client';
import { RequiredUser, RequiredManga } from './fragments';

export const GET_POPULAR_KEYWORDS = gql`
  query GetPopularKeywords{
    getPopularKeywords {
      word
    }
  }
`;

export const SEARCH_USER = gql`
  query SearchUser($word: String!, $selectedUserName: String) {
    searchUser(word: $word, selectedUserName: $selectedUserName) {
      ...RequiredUserParams
    }
  }
  ${RequiredUser.fragments.params}
`;

export const SEARCH_MANGA = gql`
  query SearchManga(
    $word: String!,
    $first: Int,
    $after: String,
    $selectedList: [String!],
    $condition: String
    ) {
    searchManga(
      word: $word,
      first: $first,
      after: $after,
      selectedList: $selectedList
      condition: $condition
      ) {
      edges {
        node {
          ...RequiredMangaParams
        }
      }
      totalCount
    }
  }
  ${RequiredManga.fragments.params}
`;
