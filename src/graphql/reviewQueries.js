import { gql } from '@apollo/client';
import { RequiredReview } from './fragments';

export const GET_REVIEWS = gql`
  query GetReviews($mid: String!, $first: Int, $after: String) {
    getReviews(mid: $mid, first: $first, after: $after) {
      edges {
        node {
          ...RequiredReviewParams
        }
      }
      totalCount
    }
  }
  ${RequiredReview.fragments.params}
`;

export const CREATE_REVIEW = gql`
  mutation createReview($mid: String!, $star: Int!, $comment: String, $netabare: Boolean) {
    createReview(input: {
      mid: $mid,
      star: $star,
      comment: $comment,
      netabare: $netabare
    })
    {
      review {
        id
        star
        comment
        netabare
        user {
          id
          name
          imageURL
        }
      }
    }
  }
`;

export const UPDATE_REVIEW = gql`
  mutation updateReview($id: ID!, $star: Int, $comment: String, $netabare: Boolean) {
    updateReview(input: {
      id: $id,
      star: $star,
      comment: $comment,
      netabare: $netabare
    })
    {
      review {
        id
        star
        comment
        netabare
        user {
          id
          name
          imageURL
        }
      }
    }
  }
`;

export const DELETE_REVIEW = gql`
  mutation deleteReview($id: ID!) {
    deleteReview(input: {
      id: $id
    })
    {
      success
    }
  }
`;
