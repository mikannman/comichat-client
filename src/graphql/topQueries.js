import { gql } from '@apollo/client';
import {
  RequiredUser, RequiredReview,
} from './fragments';

export const GET_RECOMMENDED_USERS = gql`
  query GetRecommendedUsers {
    getRecommendedUsers {
      ...RequiredUserParams
    }
  }
  ${RequiredUser.fragments.params}
`;

export const GET_POPULAR_USERS = gql`
  query GetPopularUsers {
    getPopularUsers {
      ...RequiredUserParams
    }
  }
  ${RequiredUser.fragments.params}
`;

export const GET_RECENT_REVIEWS = gql`
  query GetRecentReviews {
    getRecentReviews {
      ...RequiredReviewParams
    }
  }
  ${RequiredReview.fragments.params}
`;

export const GET_RECENT_QUESTIONS_TO_ALL = gql`
  query GetRecentQuestionsToAll($first: Int, $after: String) {
    getRecentQuestionsToAll(first: $first, after: $after) {
      edges {
        node {
          id
          content
          askUser {
            id
            name
            userId
            imageURL
          }
        }
      }
    }
  }
`;
