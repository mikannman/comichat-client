import { gql } from '@apollo/client';
import { RequiredQuestion } from './fragments';

export const GET_ASKED_LIST = gql`
  query GetAskedList($id: ID!, $first: Int, $after: String) {
    getAskedList(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;

export const GET_ANSWERED_LIST = gql`
  query GetAnsweredList($id: ID!, $first: Int, $after: String) {
    getAnsweredList(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;

export const GET_UNANSWERED_LIST = gql`
  query GetUnansweredList($id: ID!, $first: Int, $after: String) {
    getUnansweredList(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;
