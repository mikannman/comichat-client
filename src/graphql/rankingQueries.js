import { gql } from '@apollo/client';
import { RequiredManga, RequiredSmallManga } from './fragments';

export const GET_MANGA_RANKING = gql`
  query GetMangaRanking($type: String!, $genre: String, $first: Int, $after: String) {
    getMangaRanking(type: $type, genre: $genre, first: $first, after: $after) {
      edges {
        node {
          ...RequiredMangaParams
        }
      }
      totalCount
      rankingTotalCount
    }
  }
  ${RequiredManga.fragments.params}
`;

export const GET_SMALL_MANGA_RANKING = gql`
  query GetSmallMangaRanking($type: String!, $genre: String $first: Int) {
    getMangaRanking(type: $type, genre: $genre first: $first) {
      edges {
        node {
          ...RequiredSmallMangaParams
        }
      }
    }
  }
  ${RequiredSmallManga.fragments.params}
`;
