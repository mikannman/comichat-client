import { gql } from '@apollo/client';
import { RequiredAnswer } from './fragments';

export const GET_QUESTION_ANSWERS = gql`
  query GetQuestionAnswers($id: ID!, $first: Int, $after: String) {
    getQuestionAnswers(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredAnswerParams
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
  ${RequiredAnswer.fragments.params}
`;

export const GET_SHOW_QUESTION_ANSWERS = gql`
  query GetShowQuestionAnswers($id: ID!, $first: Int, $after: String) {
    getShowQuestionAnswers(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredAnswerParams
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
  ${RequiredAnswer.fragments.params}
`;

export const CREATE_ANSWER = gql`
  mutation CreateAnswer(
    $questionId: ID!,
    $questionUserId: ID!,
    $content: String!
  ) {
    createAnswer(input: {
      questionId: $questionId,
      questionUserId: $questionUserId,
      content: $content
    })
    {
      answer {
        ...RequiredAnswerParams
      }
    }
  }
  ${RequiredAnswer.fragments.params}
`;

export const UPDATE_ANSWER = gql`
  mutation UpdateAnswer(
    $id: ID!,
    $content: String
  ) {
    updateAnswer(input: {
      id: $id,
      content: $content
    })
    {
      answer {
        id
        content
      }
    }
  }
`;

export const DELETE_ANSWER = gql`
  mutation DeleteAnswer($id: ID!) {
    deleteAnswer(input: {
      id: $id
    })
    {
      success
    }
  }
`;
