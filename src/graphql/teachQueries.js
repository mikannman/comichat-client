import { gql } from '@apollo/client';
import { RequiredQuestion } from './fragments';

export const GET_ASKED_ME_QUESTIONS = gql`
  query GetAskedMeQuestions($id: ID!, $first: Int, $after: String) {
    getAskedMeQuestions(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;

export const GET_ASKED_ALL_QUESTIONS = gql`
  query GetAskedAllQuestions($id: ID!, $first: Int, $after: String) {
    getAskedAllQuestions(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;

export const GET_TOLD_QUESTIONS = gql`
  query GetToldQuestions($id: ID!, $first: Int, $after: String) {
    getToldQuestions(id: $id, first: $first, after: $after) {
      edges {
        node {
          ...RequiredQuestionParams
        }
      }
      totalCount
    }
  }
  ${RequiredQuestion.fragments.params}
`;
