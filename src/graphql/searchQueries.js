import { gql } from '@apollo/client';

export const ADD_POPULAR_KEYWORD = gql`
  mutation AddPopularKeyword($word: String!) {
    addPopularKeyword(input: {
      word: $word
    })
    {
      success
    }
  }
`;

export const SEARCH_CREATORS = gql`
  query SearchCreators($word: String!) {
    searchCreators(word: $word) {
      creator
    }
  }
`;

export const SEARCH_MANGA_TITLES = gql`
  query SearchMangaTitles($word: String!) {
    searchMangaTitles(word: $word) {
      title
    }
  }
`;

export const SEARCH_MANGA_INPUT = gql`
  query SearchMangaInput(
    $word: String!,
    $first: Int,
    $selectedList: [String!],
    ) {
    searchManga(
      word: $word,
      first: $first,
      selectedList: $selectedList
      ) {
      edges {
        node {
          mid
          title
        }
      }
    }
  }
`;

export const SEARCH_USER_INPUT = gql`
  query SearchUser($word: String!, $selectedUserId: String) {
    searchUser(word: $word, selectedUserId: $selectedUserId) {
      id
      name
      imageURL
      userId
    }
  }
`;
