import React, { useState } from 'react';
import { Container, Grid, Icon } from 'semantic-ui-react';
import { useParams, useLocation } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import queryString from 'query-string';
import { GET_MANGA_RANKING } from '../../graphql/rankingQueries';
import MangaListArea from '../shared/MangaListArea';
import PopularKeywords from '../shared/PopularKeywords';

const perPage = 10;

const rankingTypes = {
  weeklyRating: '週間おもしろい！！ランキング',
  weeklyReviews: '週間レビュー数ランキング',
  weeklyByGenre: 'の週間ランキング',
  allTime: 'オールタイムランキング',
};

export default function Ranking() {
  const [totalPage, setTotalPage] = useState(0);
  const location = useLocation();
  const { type } = useParams();
  const genre = decodeURIComponent(queryString.parse(location.search).genre || '');

  const {
    loading, data, refetch,
  } = useQuery(GET_MANGA_RANKING, {
    variables: { type, genre, first: perPage },
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getMangaRanking.rankingTotalCount / perPage));
    },
  });

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={11}>
            <h1>
              {genre}
              {rankingTypes[type]}
            </h1>
            <MangaListArea
              mangas={data.getMangaRanking.edges}
              totalPage={totalPage}
              perPage={perPage}
              paginationVariables={{ type }}
              paginationRefetch={refetch}
            />
          </Grid.Column>
          <Grid.Column width={5}>
            <PopularKeywords />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
