import React, { useState } from 'react';
import { Container, Grid, Icon } from 'semantic-ui-react';
import { useParams, useLocation } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import queryString from 'query-string';
import { useMediaQuery } from 'react-responsive';
import { GET_MANGA_RANKING } from '../../../graphql/rankingQueries';
import TabletMangaListArea from '../../shared/responsive/TabletMangaListArea';
import MobileMangaListArea from '../../shared/responsive/MobileMangaListArea';

const perPage = 10;

const rankingTypes = {
  weeklyRating: '週間おもしろい！！ランキング',
  weeklyReviews: '週間レビュー数ランキング',
  weeklyByGenre: 'の週間ランキング',
  allTime: 'オールタイムランキング',
};

export default function Ranking() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [totalPage, setTotalPage] = useState(0);
  const location = useLocation();
  const { type } = useParams();
  const genre = decodeURIComponent(queryString.parse(location.search).genre || '');

  const {
    loading, data, refetch,
  } = useQuery(GET_MANGA_RANKING, {
    variables: { type, genre, first: perPage },
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getMangaRanking.rankingTotalCount / perPage));
    },
  });

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <h1
              style={{
                fontSize: isMobile ? '20px' : '',
                margin: '0 0 40px 0',
              }}
            >
              {genre}
              {rankingTypes[type]}
            </h1>
            {
              isMobile
                ? (
                  <MobileMangaListArea
                    mangas={data.getMangaRanking.edges}
                    totalPage={totalPage}
                    perPage={perPage}
                    paginationVariables={{ type }}
                    paginationRefetch={refetch}
                  />
                )
                : (
                  <TabletMangaListArea
                    mangas={data.getMangaRanking.edges}
                    totalPage={totalPage}
                    perPage={perPage}
                    paginationVariables={{ type }}
                    paginationRefetch={refetch}
                  />
                )
            }
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
