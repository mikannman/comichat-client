import React from 'react';
import styled from 'styled-components';
import { Modal, Icon } from 'semantic-ui-react';
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import SignUpModal from './SignUpModal';
import SignInModal from './SignInModal';
import { allReset, resetError, setState } from '../../redux/features/sessionModal/sessionModalSlice';
import { Warning } from '../../style/globalStyle';

const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  background-color: #ebebeb;
  height: 60px;
  position: relative;
`;

const CloseButton = styled.div`
  position: absolute;
  right: ${(props) => props.right || '10%'};
  top: ${(props) => props.top || '37%'};
  color: #b0b0b0;
  cursor: pointer;
`;

const ReturnButton = styled.div`
  position: absolute;
  left: 10%;
  top: 37%;
  color: blue;
  cursor: pointer;
`;

const HeaderText = styled.h2`
  padding-top: 16px;
`;

const DisplayModal = () => {
  const modalState = useSelector((state) => state.sessionModal.state);
  switch (modalState.mode) {
    case 'アカウントを作成':
      return (
        <SignUpModal />
      );
    case 'ログイン':
      return (
        <SignInModal />
      );
    default:
      return (<></>);
  }
};

export default function SessionModal() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const dispatch = useDispatch();
  const modalState = useSelector((state) => state.sessionModal.state);
  const modalCommunicationError = useSelector((state) => state.sessionModal.error.communication);
  const modalWarningText = useSelector((state) => state.sessionModal.warningText);

  const onClose = () => {
    dispatch(allReset());
  };

  const handleClickReturn = () => {
    dispatch(resetError());
    dispatch(setState({ nextPageOpen: false }));
  };

  return (
    <Modal
      open={modalState.open}
      onClose={onClose}
      size="small"
    >
      <StyledHeader>
        <HeaderText>
          {modalState.mode}
        </HeaderText>
        {
          modalState.nextPageOpen
            && (
              <ReturnButton
                onClick={handleClickReturn}
              >
                {'< 戻る'}
              </ReturnButton>
            )
        }
        <CloseButton
          onClick={onClose}
          right={isMobile && '3%'}
          top={isMobile && '20%'}
        >
          {
            isMobile
              ? <Icon name="delete" />
              : '閉じる'
          }
        </CloseButton>
      </StyledHeader>
      {
        modalWarningText
          && (
            <h4 style={{ textAlign: 'center' }}>
              {modalWarningText}
            </h4>
          )
      }
      {modalCommunicationError.map((errorMessage) => (
        <Warning
          key={errorMessage}
          margin="10px 0 0 20px"
        >
          {errorMessage}
        </Warning>
      ))}
      <DisplayModal />
    </Modal>
  );
}
