import React from 'react';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';
import { useDispatch, useSelector } from 'react-redux';
import {
  Center, DivSpacer, StyledInput, ClickableText, Warning,
} from '../../style/globalStyle';
import { useAuth } from '../../hooks/useAuth';
import {
  setState, setInput, goToSignUp, resetError,
} from '../../redux/features/sessionModal/sessionModalSlice';

const InputWrapper = styled.div`
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function SignInWithEmail() {
  const dispatch = useDispatch();
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const modalError = useSelector((state) => state.sessionModal.error);
  const modalInput = useSelector((state) => state.sessionModal.input);
  const { validationCheckAndAuth, loading } = useAuth({
    type: 'signIn',
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    validationCheckAndAuth();
  };

  const handleChangeEmail = (e) => {
    dispatch(setInput({ email: e.target.value }));
  };

  const handleChangePassword = (e) => {
    dispatch(setInput({ password: e.target.value }));
  };

  const handleClickText = () => {
    dispatch(goToSignUp());
  };

  const handleClickForgotPassword = () => {
    dispatch(resetError());
    dispatch(setState({ forgotPasswordOpen: true }));
  };

  return (
    <>
      <DivSpacer marginTop="40px">
        <h4>メールアドレスでログイン</h4>
        <form
          noValidate
          onSubmit={handleSubmit}
        >
          {modalError.email.map((errorMessage) => (
            <Warning key={errorMessage}>{errorMessage}</Warning>
          ))}
          <InputWrapper style={{ marginBottom: '10px' }}>
            <StyledInput
              height="40px"
              placeholder="メールアドレス"
              type="email"
              value={modalInput.email}
              onChange={handleChangeEmail}
            />
          </InputWrapper>
          {modalError.password.map((errorMessage) => (
            <Warning key={errorMessage}>{errorMessage}</Warning>
          ))}
          <InputWrapper>
            <StyledInput
              height="40px"
              placeholder="パスワード"
              type="password"
              value={modalInput.password}
              onChange={handleChangePassword}
            />
          </InputWrapper>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              margin: '15px 30px 0 0',
            }}
          >
            <ClickableText onClick={handleClickForgotPassword}>パスワードを忘れた場合</ClickableText>
          </div>
          <Center marginTop="10px">
            <Button
              color="teal"
              type="submit"
              disabled={!modalInput.email || !modalInput.password}
              loading={loading}
            >
              ログイン
            </Button>
          </Center>
        </form>
      </DivSpacer>
      <Center
        marginTop="40px"
        marginBottom="20px"
        color="#b0b0b0"
        flexDirection={isMobile ? 'column' : ''}
        textAlign={isMobile ? 'center' : ''}
      >
        アカウントをお持ちでない方は
        <ClickableText
          onClick={handleClickText}
        >
          アカウント登録
        </ClickableText>
      </Center>
    </>
  );
}
