import React from 'react';
import { Button, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { Center, DivSpacer } from '../../style/globalStyle';
import firebase, {
  providerFacebook, providerTwitter,
} from '../../firebase/config';
import { useAuth } from '../../hooks/useAuth';
import { setCommunicationError } from '../../redux/features/sessionModal/sessionModalSlice';
import { setToken } from '../../redux/features/authToken/authTokenSlice';

export default function SNSRegistration({ text }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const dispatch = useDispatch();
  const { signUpOrIn } = useAuth({});

  const socialLogin = (provider, providerName) => {
    firebase.auth().signInWithPopup(provider)
      .then(async (result) => {
        await result.user.getIdToken(/* forceRefresh */ true)
          .then((idToken) => {
            dispatch(setToken(idToken));
          });
        signUpOrIn({
          variables: {
            name: result.user.displayName,
            email: result.user.email,
            imageURL: result.user.photoURL,
            provider: result.user.providerData[0].providerId,
          },
        });
      })
      .catch((error) => {
        if (error.code === 'auth/account-exists-with-different-credential') {
          firebase.auth().fetchSignInMethodsForEmail(error.email)
            .then((result) => {
              dispatch(setCommunicationError(
                [`${result[0]}ですでに登録されています。`],
              ));
            });
        } else if (error.code === 'auth/operation-not-supported-in-this-environment') {
          dispatch(setCommunicationError(
            [`この環境では${providerName}は利用できません。`],
          ));
        } else if (error.code === 'auth/cancelled-popup-request') {
          dispatch(setCommunicationError(
            ['連続したログイン要求はできません'],
          ));
        } else if (error.code === 'auth/popup-blocked') {
          dispatch(setCommunicationError(
            ['ポップアップがブラウザにブロックされました。メールアドレスで登録してください。'],
          ));
        } else if (error.code === 'auth/unauthorized-domain') {
          dispatch(setCommunicationError(
            ['アプリのドメインが許可されていません。'],
          ));
        } else {
          dispatch(setCommunicationError(
            ['予期せぬエラーが発生しました。'],
          ));
        }
      });
  };

  const facebookLogin = () => socialLogin(providerFacebook, 'Facebook');

  const twitterLogin = () => socialLogin(providerTwitter, 'Twitter');

  return (
    <div>
      <h4>{text}</h4>
      <Center>
        <Button
          color="facebook"
          onClick={facebookLogin}
          size={isMobile && 'mini'}
        >
          <Icon name="facebook" />
          Facebook
        </Button>
        <DivSpacer marginLeft="20px">
          <Button
            color="twitter"
            onClick={twitterLogin}
            size={isMobile && 'mini'}
          >
            <Icon name="twitter" />
            Twitter
          </Button>
        </DivSpacer>
      </Center>
    </div>
  );
}

SNSRegistration.propTypes = {
  text: PropTypes.string.isRequired,
};
