import React from 'react';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import {
  Center, DivSpacer, StyledInput, Warning,
} from '../../style/globalStyle';
import {
  setInput, resetError, setState,
} from '../../redux/features/sessionModal/sessionModalSlice';
import { useAuth } from '../../hooks/useAuth';

const InputWrapper = styled.div`
  margin-top: -15px;
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function EmailRegistration() {
  const dispatch = useDispatch();
  const modalInput = useSelector((state) => state.sessionModal.input);
  const emailErrors = useSelector((state) => state.sessionModal.error.email);
  const { validationCheckAndAuth, loading } = useAuth({
    onComfirmedEmail: (result) => {
      dispatch(setState({ nextPageOpen: true }));
      dispatch(setInput({ userID: result.verifyEmail.userId }));
      dispatch(resetError());
    },
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    validationCheckAndAuth();
  };

  const handleChange = (e) => {
    dispatch(setInput({ email: e.target.value }));
  };

  return (
    <DivSpacer marginTop="40px">
      <form
        noValidate
        onSubmit={handleSubmit}
      >
        <h4
          style={{
            fontSize: '13px',
          }}
        >
          メールアドレスからアカウントを作成
        </h4>
        <DivSpacer marginTop="30px">
          {emailErrors.map((errorMessage) => (
            <Warning
              key={errorMessage}
              style={{
                margin: '20px 0',
              }}
            >
              {errorMessage}
            </Warning>
          ))}
          <InputWrapper>
            <StyledInput
              height="30px"
              type="email"
              placeholder="メールアドレス"
              value={modalInput.email}
              onChange={handleChange}
            />
          </InputWrapper>
        </DivSpacer>
        <Center marginTop="30px">
          <Button
            type="submit"
            color="teal"
            loading={loading}
            disabled={!modalInput.email}
          >
            アカウントを作成する
          </Button>
        </Center>
      </form>
    </DivSpacer>
  );
}
