import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import EmailRegistration from './EmailRegistration';
import SNSRegistration from './SNSRegistration';
import { Center, Wrapper, ClickableText } from '../../style/globalStyle';
import InputAccountInfoModal from './InputAccountInfoModal';
import { setState, resetError } from '../../redux/features/sessionModal/sessionModalSlice';

export default function SignUpModal() {
  const dispatch = useDispatch();
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const modalState = useSelector((state) => state.sessionModal.state);

  const handleClickText = () => {
    dispatch(setState({ mode: 'ログイン' }));
    dispatch(resetError());
  };

  return (
    <Wrapper
      padding="30px"
    >
      {
        modalState.nextPageOpen
          ? (
            <InputAccountInfoModal />
          )
          : (
            <>
              <SNSRegistration
                text="SNSアカウントから作成"
              />
              <EmailRegistration />
              <Center
                marginTop="40px"
                marginBottom="20px"
                color="#b0b0b0"
                flexDirection={isMobile ? 'column' : ''}
                textAlign={isMobile ? 'center' : ''}
              >
                すでにアカウントをお持ちの場合
                <ClickableText
                  onClick={handleClickText}
                >
                  ログイン
                </ClickableText>
              </Center>
            </>
          )
      }
    </Wrapper>
  );
}
