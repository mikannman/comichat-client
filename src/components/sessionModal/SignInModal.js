import React from 'react';
import { useSelector } from 'react-redux';
import { Wrapper } from '../../style/globalStyle';
import SNSRegistration from './SNSRegistration';
import SignInWithEmail from './SignInWithEmail';
import ForgotPasswordModal from './ForgotPasswordModal';

export default function SignInModal() {
  const forgotPasswordOpen = useSelector((state) => state.sessionModal.state.forgotPasswordOpen);

  return (
    <Wrapper
      padding="30px"
    >
      <SNSRegistration
        text="SNSアカウントでログイン"
      />
      {
        forgotPasswordOpen
          ? <ForgotPasswordModal />
          : <SignInWithEmail />
      }
    </Wrapper>
  );
}
