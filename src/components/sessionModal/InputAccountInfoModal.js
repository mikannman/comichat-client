import React from 'react';
import {
  Icon, Grid, Button,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import {
  CircleImage, ParentWrapper, Warning,
} from '../../style/globalStyle';
import { fileSizeCheck, previewFile } from '../../functions/function';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import { useAuth } from '../../hooks/useAuth';
import InputArea from '../shared/InputArea';
import UserIdInputArea from '../shared/UserIdInputArea';
import { setInput, setError, setVerifyUserId } from '../../redux/features/sessionModal/sessionModalSlice';

const ImageLabel = styled.div`
  font-size: 15px;
  color: blue;
  margin-top: 10px;
`;

const CloseIcon = styled.div`
  position: absolute;
  top: 4%;
  right: 40%;
  cursor: pointer;
`;

const infos = [
  {
    label: '名前',
    objectKey: 'name',
    objectType: 'text',
  },
  {
    label: 'メールアドレス',
    objectKey: 'email',
    objectType: 'email',
  },
  {
    label: 'パスワード',
    objectKey: 'password',
    objectType: 'password',
  },
  {
    label: 'パスワードの確認',
    objectKey: 'passwordConfirmation',
    objectType: 'password',
  },
];

export default function InputAccountInfoModal() {
  const modalError = useSelector((state) => state.sessionModal.error);
  const modalInput = useSelector((state) => state.sessionModal.input);
  const isEmptyInput = useSelector((state) => {
    const v = state.sessionModal.input;
    return (
      Boolean([v.name, v.email, v.password, v.passwordConfirmation].filter((c) => (
        c === ''
      )).length || state.sessionModal.verifyUserId === 'red')
    );
  });
  const dispatch = useDispatch();
  const imageURL = useSelector((state) => state.sessionModal.input.imageURL);
  const { validationCheckAndAuth, loading } = useAuth({
    type: 'signUp',
  });

  const handleChangeImageURL = async (e) => {
    try {
      const file = await fileSizeCheck(e.target);
      previewFile(file, 150, (result) => {
        dispatch(setInput({ imageURL: result }));
      });
    } catch (error) {
      dispatch(setError({ imageURL: [error.message] }));
    }
  };

  const handleClickImageDelete = () => dispatch(setInput({ imageURL: '' }));

  const handleSubmit = (e) => {
    e.preventDefault();
    validationCheckAndAuth();
  };

  const handleChangeInput = (e) => dispatch(setInput({ [e.target.dataset.key]: e.target.value }));

  const handleChangeUserID = (e) => dispatch(setInput({ userID: e.target.value }));

  const onVerifiedUserId = (type) => dispatch(setVerifyUserId(type));

  return (
    <ParentWrapper justifyContent="center">
      <form
        noValidate
        onSubmit={handleSubmit}
      >
        <Grid centered>
          <Grid.Row
            style={{
              padding: '0',
            }}
          >
            {
              Boolean(imageURL)
                && (
                  <CloseIcon
                    onClick={handleClickImageDelete}
                  >
                    <Icon name="close" />
                  </CloseIcon>
                )
            }
            <label style={{ cursor: 'pointer' }}>
              <input
                type="file"
                accept="image/*"
                style={{ display: 'none' }}
                onChange={handleChangeImageURL}
              />
              <CircleImage
                src={imageURL || DefaultAccontImage}
                alt="プロフィール画像"
                height="120"
                width="120"
              />
              <ImageLabel>プロフィール画像を変更</ImageLabel>
            </label>
            {modalError.imageURL.map((errorMessage) => (
              <Warning key={errorMessage}>{errorMessage}</Warning>
            ))}
          </Grid.Row>
          <UserIdInputArea
            value={modalInput.userID}
            handleChange={handleChangeUserID}
            onVerifiedUserId={onVerifiedUserId}
          />
          {
            infos.map(({ label, objectKey, objectType }) => (
              <InputArea
                label={label}
                objectKey={objectKey}
                objectType={objectType}
                value={modalInput[objectKey]}
                errors={modalError[objectKey]}
                handleChange={handleChangeInput}
                key={objectKey}
              />
            ))
          }
          <Grid.Row style={{ marginTop: '10px' }}>
            <Button
              color="teal"
              type="submit"
              disabled={isEmptyInput}
              loading={loading}
            >
              登録する
            </Button>
          </Grid.Row>
        </Grid>
      </form>
    </ParentWrapper>
  );
}
