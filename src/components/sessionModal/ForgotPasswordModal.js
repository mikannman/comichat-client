import React, { useState } from 'react';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';
import { useDispatch, useSelector } from 'react-redux';
import {
  Center, DivSpacer, StyledInput, ClickableText, Warning,
} from '../../style/globalStyle';
import firebase from '../../firebase/config';
import { useAuth } from '../../hooks/useAuth';
import {
  setInput, allReset, goToSignUp, goToSignIn,
  setCommunicationError,
} from '../../redux/features/sessionModal/sessionModalSlice';

const InputWrapper = styled.div`
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function ForgotPassword() {
  const dispatch = useDispatch();
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const modalError = useSelector((state) => state.sessionModal.error);
  const modalInput = useSelector((state) => state.sessionModal.input);
  const [loading, setLoading] = useState(false);
  const { validationCheck } = useAuth({
    onCompletedCheck: () => {
      const actionCodeSettings = {
        url: `${process.env.REACT_APP_CLIENT_URL}/settingsPassword`,
      };
      firebase.auth().fetchSignInMethodsForEmail(modalInput.email)
        .then((result) => {
          if (result[0] === 'password') {
            firebase.auth().sendPasswordResetEmail(modalInput.email, actionCodeSettings)
              .then(() => {
                dispatch(allReset());
                window.flash('メールを送信しました');
                setLoading(false);
              })
              .catch((error) => {
                if (error.code === 'auth/user-not-found') {
                  dispatch(setCommunicationError(['メールを送信できませんでした']));
                }
                setLoading(false);
              });
          } else if (result[0] === undefined) {
            setLoading(false);
            dispatch(setCommunicationError(
              ['このメールアドレスはまだ登録されていません'],
            ));
          } else {
            setLoading(false);
            dispatch(setCommunicationError(
              [`このメールアドレスは${result[0]}ですでに登録されています`],
            ));
          }
        });
    },
    onFailedCheck: () => setLoading(false),
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    validationCheck();
  };

  const handleChangeEmail = (e) => {
    dispatch(setInput({ email: e.target.value }));
  };

  const handleClickText = () => {
    dispatch(goToSignUp());
  };

  const handleClickReturn = () => {
    dispatch(goToSignIn());
  };

  return (
    <>
      <DivSpacer marginTop="40px">
        <h4>パスワード再設定メールを送信する</h4>
        <form
          noValidate
          onSubmit={handleSubmit}
        >
          {modalError.email.map((errorMessage) => (
            <Warning key={errorMessage}>{errorMessage}</Warning>
          ))}
          <InputWrapper style={{ marginBottom: '10px' }}>
            <StyledInput
              height="40px"
              placeholder="メールアドレス"
              type="email"
              value={modalInput.email}
              onChange={handleChangeEmail}
            />
          </InputWrapper>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              margin: '15px 30px 0 0',
            }}
          >
            <ClickableText onClick={handleClickReturn}>ログインに戻る</ClickableText>
          </div>
          <Center marginTop="10px">
            <Button
              color="teal"
              type="submit"
              disabled={!modalInput.email}
              loading={loading}
            >
              送信
            </Button>
          </Center>
        </form>
      </DivSpacer>
      <Center
        marginTop="40px"
        marginBottom="20px"
        color="#b0b0b0"
        flexDirection={isMobile ? 'column' : ''}
        textAlign={isMobile ? 'center' : ''}
      >
        アカウントをお持ちでない方は
        <ClickableText onClick={handleClickText}>アカウント登録</ClickableText>
      </Center>
    </>
  );
}
