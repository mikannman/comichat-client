import React, { useState, useEffect } from 'react';
import {
  ApolloClient, ApolloProvider,
} from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import App from './App';
import firebase from '../firebase/config';
import { resetUser, setIsLogin } from '../redux/features/user/userSlice';
import { setToken } from '../redux/features/authToken/authTokenSlice';
import cache from '../apollo/cache';

export default function Auth() {
  const dispatch = useDispatch();
  const [connected, setConnected] = useState(false);
  const token = useSelector((state) => state.authToken.token);
  const currentUser = useSelector((state) => state.user.value);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        if (currentUser) {
          if (user.email === currentUser.email) {
            await user.getIdToken(/* forceRefresh */ true).then((idToken) => {
              dispatch(setToken(idToken));
              dispatch(setIsLogin(true));
              setConnected(true);
            });
          } else {
            localStorage.removeItem('user');
            dispatch(setToken(''));
            dispatch(resetUser());
            dispatch(setIsLogin(false));
          }
        }

        await user.getIdToken(/* forceRefresh */ true).then((idToken) => {
          dispatch(setToken(idToken));
          dispatch(setIsLogin(true));
          setConnected(true);
        });
      } else {
        setConnected(true);
        localStorage.removeItem('user');
        dispatch(setToken(''));
        dispatch(resetUser());
        dispatch(setIsLogin(false));
      }
    });
  }, []);

  const client = new ApolloClient({
    uri: `${process.env.REACT_APP_SERVER_URL}/graphql`,
    headers: {
      Authorization: token ? `Bearer ${token}` : '',
    },
    cache,
  });

  return (
    <ApolloProvider client={client}>
      {
        connected && <App />
      }
    </ApolloProvider>
  );
}
