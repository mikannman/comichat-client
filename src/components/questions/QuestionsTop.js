import React from 'react';
import { Grid } from 'semantic-ui-react';
import QuestionArea from './QuestionArea';
import AllQuestionsByCondition from '../shared/AllQuestionsByCondition';

const questionProps = [
  { condition: 'unanswered', headerText: '新着の未回答質問', path: '/questions/list/unanswered' },
  { condition: 'answered', headerText: '新着の回答済み質問', path: '/questions/list/answered' },
];

export default function QuestionsTop() {
  return (
    <Grid>
      <Grid.Row>
        <QuestionArea />
      </Grid.Row>
      <Grid.Row columns={2} centered>
        {
          questionProps.map((props) => (
            <Grid.Column
              width={7}
              key={props.condition}
            >
              <AllQuestionsByCondition
                {...props}
              />
            </Grid.Column>
          ))
        }
      </Grid.Row>
    </Grid>
  );
}
