import React from 'react';
import { Grid } from 'semantic-ui-react';
import QuestionArea from '../QuestionArea';
import AllQuestionsByCondition from '../../shared/AllQuestionsByCondition';
import RightGenreArea from '../RightGenreArea';

const questionProps = [
  { condition: 'unanswered', headerText: '新着の未回答質問', path: '/questions/list/unanswered' },
  { condition: 'answered', headerText: '新着の回答済み質問', path: '/questions/list/answered' },
];

export default function QuestionsTop() {
  return (
    <Grid>
      <Grid.Row>
        <QuestionArea />
      </Grid.Row>
      {
        questionProps.map((props) => (
          <Grid.Row
            key={props.condition}
            centered
          >
            <AllQuestionsByCondition
              {...props}
            />
          </Grid.Row>
        ))
      }
      <Grid.Row
        centered
      >
        <RightGenreArea />
      </Grid.Row>
    </Grid>
  );
}
