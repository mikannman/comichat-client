import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import {
  Route, Switch, useRouteMatch,
} from 'react-router-dom';
import MobileQuestionsTop from './MobileQuestionsTop';
import MobileShowQuestion from './MobileShowQuestion';
import QuestionList from '../QuestionList';

export default function MobileQuestions() {
  const match = useRouteMatch();

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <Switch>
              <Route
                key="questionsTop"
                exact
                path={`${match.path}/`}
              >
                <MobileQuestionsTop />
              </Route>
              <Route
                key="showQuestion"
                path={`${match.path}/show/:id`}
              >
                <MobileShowQuestion />
              </Route>
              <Route
                key="questionList"
                path={`${match.path}/list/:condition`}
              >
                <QuestionList />
              </Route>
            </Switch>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
