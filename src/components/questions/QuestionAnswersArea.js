import React, { useEffect, useCallback, useState } from 'react';
import { Grid, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import DisplayPagination from '../shared/DisplayPagination';
import QuestionAnswer from './QuestionAnswer';

export default function QuestionAnswersArea({
  questionId, getShowQuestionAnswers, loading, refetch,
  answerAreaPerPage,
}) {
  const [totalPage, setTotalPage] = useState(null);

  useEffect(() => {
    setTotalPage(Math.ceil(getShowQuestionAnswers.totalCount / answerAreaPerPage));
  }, [getShowQuestionAnswers]);

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Grid.Row columns={1}>
      <Grid.Column>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              {getShowQuestionAnswers.edges.map((edge) => (
                <QuestionAnswer
                  answer={edge.node}
                  questionId={questionId}
                  key={edge.node.id}
                />
              ))}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row centered columns={3}>
            <DisplayPagination
              perPage={answerAreaPerPage}
              totalPage={totalPage}
              refetch={refetch}
              handleChangePage={handleChangePage}
            />
          </Grid.Row>
        </Grid>
      </Grid.Column>
    </Grid.Row>
  );
}

QuestionAnswersArea.propTypes = {
  questionId: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  refetch: PropTypes.func.isRequired,
  answerAreaPerPage: PropTypes.number.isRequired,
  getShowQuestionAnswers: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          id: PropTypes.string.isRequired,
          content: PropTypes.string.isRequired,
          updatedAt: PropTypes.string.isRequired,
          replies: PropTypes.shape({
            count: PropTypes.number.isRequired,
          }).isRequired,
          likeAnswers: PropTypes.shape({
            count: PropTypes.number.isRequired,
          }).isRequired,
          user: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            imageURL: PropTypes.string.isRequired,
          }).isRequired,
          currentUserLikes: PropTypes.shape({
            isExist: PropTypes.bool.isRequired,
          }).isRequired,
        }).isRequired,
      }).isRequired,
    ).isRequired,
    totalCount: PropTypes.number.isRequired,
  }).isRequired,
};
