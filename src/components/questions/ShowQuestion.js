import React, { useState } from 'react';
import {
  Grid, Button, Icon, Label, Segment, Divider,
} from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import { useParams, Link } from 'react-router-dom';
import { useQuery, useMutation, gql } from '@apollo/client';
import { StyledTextarea } from '../../style/globalStyle';
import { GET_QUESTION } from '../../graphql/questionQueries';
import { CREATE_ANSWER, GET_SHOW_QUESTION_ANSWERS } from '../../graphql/answerQueries';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import { howLongAgo } from '../../functions/function';
import QuestionAnswersArea from './QuestionAnswersArea';

const query = gql`
  query GetShowQuestionAnswers($id: String, $after: String, $CREATE: Boolean) {
    getShowQuestionAnswers(id: $id, after: $after, CREATE: $CREATE) {
      edges {
        node {
          id
          content
        }
      }
      totalCount
    }
  }
`;

const answerAreaPerPage = 5;

export default function ShowQuestion() {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const { id } = useParams();
  const [textareaValue, setTextareaValue] = useState('');

  const { loading, data } = useQuery(GET_QUESTION, {
    variables: {
      id,
    },
  });

  const {
    loading: answerLoading, data: answerData,
    refetch: answerRefetch,
  } = useQuery(GET_SHOW_QUESTION_ANSWERS, {
    variables: { id, first: answerAreaPerPage },
  });

  const [createAnswer, { loading: createAnswerLoading }] = useMutation(CREATE_ANSWER, {
    onCompleted: () => {
      setTextareaValue('');
    },
    update: (cache, { data: { createAnswer: ca } }) => {
      const answersData = cache.readQuery({
        query,
        variables: {
          id: data.getQuestion.id,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          id: data.getQuestion.id,
          CREATE: true,
        },
        data: {
          getShowQuestionAnswers: {
            edges: [
              {
                node: ca.answer,
                __typename: 'AnsewrEdge',
              },
              ...answersData.getShowQuestionAnswers.edges,
            ],
            totalCount: answersData.getShowQuestionAnswers.totalCount + 1,
          },
        },
      });
    },
    onError: (errors) => {
      errors.forEach((e) => {
        if (e.extensions.code === 'AUTHENTICATION_ERROR') {
          window.flash(e.message, 'error');
        }
      });
    },
  });

  const handleChangeTextarea = (e) => {
    setTextareaValue(e.target.value);
  };

  const handleClickSubmitButton = (e) => {
    createAnswer({
      variables: {
        questionId: e.target.dataset.questionId,
        questionUserId: e.target.dataset.questionUserId,
        content: textareaValue,
      },
    });
  };

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Segment>
      <Grid style={{ padding: '15px' }}>
        <Grid.Row>
          <Link to={`/user/${data.getQuestion.askUser.id}`}>
            <Label image>
              <img src={data.getQuestion.askUser.imageURL || DefaultAccontImage} alt="ユーザー画像" />
              {data.getQuestion.askUser.name}
            </Label>
          </Link>
          to:
          {
            data.getQuestion.askedUser
              && (
                <Label image>
                  <img src={data.getQuestion.askedUser.imageURL || DefaultAccontImage} alt="ユーザー画像" />
                  {data.getQuestion.askedUser.name}
                </Label>
              )
          }
          {
            data.getQuestion.all
              && (
                <Label>
                  みんな
                </Label>
              )
          }
          {howLongAgo(data.getQuestion.updatedAt)}
          {
            answerData && answerData.getShowQuestionAnswers.totalCount
              ? (
                <Label color="red">
                  回答済み
                </Label>
              )
              : (
                <Label color="grey">
                  未回答
                </Label>
              )
          }
        </Grid.Row>
        <Grid.Row style={{ padding: '3px' }}>
          探しているジャンル:
          {
            data.getQuestion.genres.length
              ? (
                data.getQuestion.genres.map((g) => (
                  <Label key={g.genre}>{g.genre}</Label>
                ))
              )
              : <Label>選択されていません</Label>
          }
        </Grid.Row>
        <Grid.Row style={{ padding: '3px' }}>
          今読んでいる漫画:
          {
            data.getQuestion.readingMangaMid
              ? (
                data.getQuestion.readingMangaMid.split(',').map((mid) => (
                  <Label key={`reading-${mid}`}>{mid}</Label>
                ))
              )
              : <Label>選択されていません</Label>
          }
        </Grid.Row>
        <Grid.Row style={{ padding: '3px' }}>
          今まで読んできた漫画:
          {
            data.getQuestion.readedMangaMid
              ? (
                data.getQuestion.readedMangaMid.split(',').map((mid) => (
                  <Label key={`readed-${mid}`}>{mid}</Label>
                ))
              )
              : <Label>選択されていません</Label>
          }
        </Grid.Row>
        <Divider section />
        <Grid.Row
          style={{
            margin: '15px',
          }}
        >
          {data.getQuestion.content}
        </Grid.Row>
        {
          answerData && Boolean(answerData.getShowQuestionAnswers.totalCount)
            && (
              <Grid.Row
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                <spen>
                  回答者:
                  {
                    answerData.getShowQuestionAnswers.edges.slice(0, 2).map((edge) => (
                      <Label image key={edge.node.id}>
                        <img src={edge.node.user.imageURL || DefaultAccontImage} alt="ユーザー画像" />
                        {edge.node.user.name}
                      </Label>
                    ))
                  }
                  {
                    answerData.getShowQuestionAnswers.totalCount > 2
                      ? '他...'
                      : ''
                  }
                </spen>
                <span>
                  回答数:
                  {answerData.getShowQuestionAnswers.totalCount}
                </span>
              </Grid.Row>
            )
          }
        <Divider section />
        {
          answerData && data.getQuestion.askUser.id !== currentUserId
            && (
              <>
                <Grid.Row>
                  <StyledTextarea
                    placeholder="〇〇な漫画がおすすめですよ！！"
                    value={textareaValue}
                    onChange={handleChangeTextarea}
                    border="solid 1px black"
                    height="70px"
                    padding="10px"
                  />
                </Grid.Row>
                <Grid.Row
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                  }}
                >
                  <Button
                    onClick={handleClickSubmitButton}
                    color="teal"
                    loading={createAnswerLoading}
                    data-question-id={data.getQuestion.id}
                    data-question-user-id={data.getQuestion.askUser.id}
                  >
                    送信
                  </Button>
                </Grid.Row>
              </>
            )
        }
        {answerLoading && <Icon name="spinner" loading />}
        {
          answerData && answerData.getShowQuestionAnswers.totalCount
            ? (
              <Grid.Row>
                <QuestionAnswersArea
                  questionId={data.getQuestion.id}
                  getShowQuestionAnswers={answerData.getShowQuestionAnswers}
                  loading={answerLoading}
                  refetch={answerRefetch}
                  answerAreaPerPage={answerAreaPerPage}
                />
              </Grid.Row>
            )
            : (
              <Grid.Row>まだ回答がありません</Grid.Row>
            )
        }
      </Grid>
    </Segment>
  );
}
