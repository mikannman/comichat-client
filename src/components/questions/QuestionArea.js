import React, { useState, useEffect, useRef } from 'react';
import { Button } from 'semantic-ui-react';
import { Transition } from 'react-transition-group';
import CreateQuestionForm from '../shared/CreateQuestionForm';
import usePromptSignIn from '../../hooks/usePromptSignIn';

const duration = 200;

const defaultSyle = {
  transition: `all ${duration}ms ease`,
  height: '0px',
  overflow: 'hidden',
  margin: '10px',
};

let writeAreaHeight;

export default function QuestionArea() {
  const [visibleQuestionArea, setVisibleQuestionArea] = useState(false);
  const contentElem = useRef();
  const promptSignIn = usePromptSignIn();

  useEffect(() => {
    writeAreaHeight = `${contentElem.current.getBoundingClientRect().height}px`;
  }, []);

  const transitionStyles = {
    entering: {
      height: writeAreaHeight,
    },
    entered: {
      height: 'auto',
    },
    exiting: {
      height: writeAreaHeight,
    },
    exited: {
      height: '0px',
    },
  };

  const handleClickQuestionButton = () => {
    promptSignIn({
      callback: () => setVisibleQuestionArea((prev) => !prev),
    });
  };

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
        <Button
          color="teal"
          size="large"
          onClick={handleClickQuestionButton}
        >
          {visibleQuestionArea ? '閉じる' : '質問する'}
        </Button>
      </div>
      <Transition in={visibleQuestionArea} timeout={duration}>
        {(transitionState) => (
          <div
            style={{
              ...defaultSyle,
              ...transitionStyles[transitionState],
            }}
          >
            <div ref={contentElem}>
              <CreateQuestionForm
                labelWidth={7}
              />
            </div>
          </div>
        )}
      </Transition>
    </>
  );
}
