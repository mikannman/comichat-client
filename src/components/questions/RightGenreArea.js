import React from 'react';
import { useSelector } from 'react-redux';
import { Label } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';

export default function RightGenreArea() {
  const history = useHistory();
  const genres = useSelector((state) => state.genre.data);

  const handleClickLabel = (e) => {
    history.push(`/questions/list/byGenre?genre=${encodeURIComponent(e.target.dataset.keyword)}`);
  };

  return (
    <div>
      <h3>ジャンル別に質問を見る</h3>
      {
        genres.map((genre) => (
          <Label
            key={genre}
            data-keyword={genre}
            onClick={handleClickLabel}
            as="a"
            style={{
              marginTop: '5px',
            }}
          >
            {genre}
          </Label>
        ))
      }
    </div>
  );
}
