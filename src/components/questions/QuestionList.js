import React, { useState, useCallback } from 'react';
import { Grid, Icon, Card } from 'semantic-ui-react';
import { useParams, useLocation } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import queryString from 'query-string';
import { useMediaQuery } from 'react-responsive';
import { GET_ALL_QUESTIONS_BY_CONDITION } from '../../graphql/questionQueries';
import DisplayPagination from '../shared/DisplayPagination';
import QuestionCard from '../shared/QuestionCard';
import MobileQuestionCard from '../shared/responsive/MobileQuestionCard';

const perPage = 5;

const questionTypes = {
  recent: '最新の質問一覧',
  answered: '回答済み質問一覧',
  unanswered: '未回答質問一覧',
  byGenre: 'の質問一覧',
};

export default function QuestionList() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [totalPage, setTotalPage] = useState(0);
  const { condition } = useParams();
  const location = useLocation();
  const genre = decodeURIComponent(queryString.parse(location.search).genre || '');

  const {
    loading, data, refetch,
  } = useQuery(GET_ALL_QUESTIONS_BY_CONDITION, {
    variables: { condition, genre, first: perPage },
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getAllQuestionsByCondition.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Grid centered>
      <Grid.Row>
        <h1>
          {genre}
          {questionTypes[condition]}
        </h1>
      </Grid.Row>
      {
        data.getAllQuestionsByCondition.edges.length
          ? (
            <>
              <Card.Group centered>
                {
                  data.getAllQuestionsByCondition.edges.map((edge) => (
                    isMobile
                      ? (
                        <MobileQuestionCard
                          question={edge.node}
                          refetch={refetch}
                          key={edge.node.id}
                        />
                      )
                      : (
                        <QuestionCard
                          question={edge.node}
                          refetch={refetch}
                          key={edge.node.id}
                        />
                      )
                  ))
                }
              </Card.Group>
              <Grid.Row>
                <DisplayPagination
                  perPage={perPage}
                  totalPage={totalPage}
                  refetch={refetch}
                  handleChangePage={handleChangePage}
                />
              </Grid.Row>
            </>
          )
          : (
            <Grid.Row>
              まだ質問がありません
            </Grid.Row>
          )
      }
    </Grid>
  );
}
