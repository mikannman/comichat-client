import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import {
  Route, Switch, useRouteMatch,
} from 'react-router-dom';
import QuestionsTop from './QuestionsTop';
import ShowQuestion from './ShowQuestion';
import QuestionList from './QuestionList';
import RightGenreArea from './RightGenreArea';
import RecentQuestionsToAll from '../top/RecentQuestionsToAll';

export default function Questions() {
  const match = useRouteMatch();

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={11}>
            <Switch>
              <Route
                key="questionsTop"
                exact
                path={`${match.path}/`}
              >
                <QuestionsTop />
              </Route>
              <Route
                key="showQuestion"
                path={`${match.path}/show/:id`}
              >
                <ShowQuestion />
              </Route>
              <Route
                key="questionList"
                path={`${match.path}/list/:condition`}
              >
                <QuestionList />
              </Route>
            </Switch>
          </Grid.Column>
          <Grid.Column width={5}>
            <RightGenreArea />
            <RecentQuestionsToAll />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
