import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Label, Feed, Dropdown,
} from 'semantic-ui-react';
import { useMutation, gql } from '@apollo/client';
import { useSelector } from 'react-redux';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import { howLongAgo } from '../../functions/function';
import { DELETE_ANSWER, UPDATE_ANSWER } from '../../graphql/answerQueries';
import Like from '../shared/Like';
import FeedEdit from '../user/taught/FeedEdit';
import FeedContent from '../user/taught/FeedContent';
import ReplyArea from '../user/taught/ReplyArea';
import ReplyTextarea from '../user/taught/ReplyTextarea';
import useConfirm from '../../hooks/useConfirm';

const query = gql`
  query GetShowQuestionAnswers($id: String, $after: String, $DELETE: Boolean) {
    getShowQuestionAnswers(id: $id, after: $after, DELETE: $DELETE) {
      edges {
        node {
          id
          content
        }
      }
      totalCount
    }
  }
`;

export default function QuestionAnswer({ answer, questionId }) {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const confirm = useConfirm();
  const [editMode, setEditMode] = useState(false);
  const [openReply, setOpenReply] = useState(false);
  const [openReplyTextarea, setOpenReplyTextarea] = useState(false);

  const [updateAnswer, { loading: updateLoading }] = useMutation(UPDATE_ANSWER, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const [deleteAnswer] = useMutation(DELETE_ANSWER, {
    update: (cache) => {
      const data = cache.readQuery({
        query,
        variables: {
          id: questionId,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          id: questionId,
          DELETE: true,
        },
        data: {
          getShowQuestionAnswers: {
            edges: data.getShowQuestionAnswers.edges.filter((edge) => (
              edge.node.id !== answer.id
            )),
            totalCount: data.getShowQuestionAnswers.totalCount - 1,
          },
        },
      });
    },
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const handleClickEdit = () => setEditMode(true);

  const handleClickDelete = () => {
    confirm({
      onConfirm: () => deleteAnswer({ variables: { id: answer.id } }),
    });
  };

  const handleClickOpenReplyTextarea = () => {
    setOpenReplyTextarea(true);
    setOpenReply(true);
  };

  const handleClickOpenReply = () => setOpenReply(true);

  const handleClickCloseReply = () => setOpenReply(false);

  return (
    <Grid>
      <Grid.Row centered>
        <Grid.Column width={14}>
          <Feed>
            <Feed.Event>
              <Feed.Label image={answer.user.imageURL || DefaultAccontImage} />
              <Feed.Content>
                <Feed.Summary>
                  {answer.user.name}
                  <Feed.Date>{howLongAgo(answer.updatedAt)}</Feed.Date>
                  {
                    currentUserId === answer.user.id
                      && (
                        <Dropdown
                          icon="ellipsis vertical"
                          style={{ alignSelf: 'center', color: 'grey', margin: '0 0 0 10px' }}
                        >
                          <Dropdown.Menu>
                            <Dropdown.Item
                              onClick={handleClickEdit}
                            >
                              編集
                            </Dropdown.Item>
                            <Dropdown.Item
                              onClick={handleClickDelete}
                            >
                              削除
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      )
                  }
                </Feed.Summary>
                {
                  updateLoading
                    ? <Icon loading name="spinner" />
                    : editMode
                      ? (
                        <FeedEdit
                          id={answer.id}
                          content={answer.content}
                          updateFunc={updateAnswer}
                          setEditMode={setEditMode}
                        />
                      )
                      : (
                        <FeedContent
                          content={answer.content}
                        />
                      )
                }
                <Feed.Meta>
                  <Feed.Like>
                    <Like
                      id={answer.id}
                      type="Answer"
                      isLike={answer.currentUserLikes.isExist}
                    />
                    {answer.likeAnswers.count}
                  </Feed.Like>
                  <Icon
                    name="reply"
                    onClick={handleClickOpenReplyTextarea}
                  />
                  {
                    openReplyTextarea
                      && (
                        <ReplyTextarea
                          id={answer.id}
                          setOpenReplyTextarea={setOpenReplyTextarea}
                        />
                      )
                  }
                  {
                    Boolean(answer.replies.count)
                      && (
                        openReply
                          ? (
                            <Label
                              as="a"
                              onClick={handleClickCloseReply}
                            >
                              返信を非表示
                            </Label>
                          )
                          : (
                            <Label
                              as="a"
                              onClick={handleClickOpenReply}
                            >
                              {`${answer.replies.count}件の返信を表示`}
                            </Label>
                          )
                      )
                  }
                </Feed.Meta>
              </Feed.Content>
            </Feed.Event>
          </Feed>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row centered columns={1}>
        <Grid.Column width={16}>
          {
            openReply
              && (
                <ReplyArea
                  id={answer.id}
                  setOpenReply={setOpenReply}
                />
              )
          }
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

QuestionAnswer.propTypes = {
  questionId: PropTypes.string.isRequired,
  answer: PropTypes.shape({
    id: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
    replies: PropTypes.shape({
      count: PropTypes.number.isRequired,
    }).isRequired,
    likeAnswers: PropTypes.shape({
      count: PropTypes.number.isRequired,
    }).isRequired,
    user: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
    }).isRequired,
    currentUserLikes: PropTypes.shape({
      isExist: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
};
