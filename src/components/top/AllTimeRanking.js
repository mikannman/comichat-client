import React from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import {
  Grid, Card, Icon,
} from 'semantic-ui-react';
import MangaImage from '../shared/MangaImage';
import { GET_SMALL_MANGA_RANKING } from '../../graphql/rankingQueries';
import DisplayMangaStars from '../shared/DisplayMangaStars';
import RegisterButtons from '../shared/RegisterButtons';

export default function AllTimeRanking() {
  const { loading, data } = useQuery(GET_SMALL_MANGA_RANKING, {
    variables: {
      type: 'allTime',
      first: 5,
    },
  });

  return (
    <Card>
      <Card.Content>
        <Card.Header>
          オールタイムランキング
          <Link to="/ranking/allTime">
            <small
              style={{
                marginLeft: '3px',
                fontSize: '8px',
              }}
            >
              全部見る
            </small>
          </Link>
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <Grid>
          {
            loading
              ? <Icon name="spinner" loading />
              : (
                data && data.getMangaRanking.edges.map((edge, idx) => (
                  <Grid.Row columns={3} key={edge.node.mid}>
                    <Grid.Column
                      width={4}
                      verticalAlign="middle"
                      style={{
                        padding: '0 3px',
                      }}
                    >
                      <h4
                        style={{
                          textAlign: 'center',
                          margin: '0 3px 3px 1px',
                        }}
                      >
                        {idx + 1}
                        位
                      </h4>
                      <Link
                        to={`/manga/${encodeURIComponent(edge.node.mid)}`}
                        style={{
                          textAlign: 'center',
                        }}
                      >
                        <MangaImage
                          title={edge.node.title}
                          length={30}
                          mid={edge.node.mid}
                          imageUrl={edge.node.imageUrl}
                          affiliateUrl={edge.node.affiliateUrl}
                        />
                      </Link>
                    </Grid.Column>
                    <Grid.Column
                      width={6}
                      verticalAlign="middle"
                      style={{
                        padding: '0',
                      }}
                    >
                      <Link to={`/manga/${encodeURIComponent(edge.node.mid)}`}>
                        <h4 style={{ margin: '0' }}>{edge.node.title}</h4>
                      </Link>
                      <DisplayMangaStars
                        reviewsAverage={edge.node.reviews.average}
                        size="small"
                      />
                    </Grid.Column>
                    <Grid.Column
                      width={6}
                      style={{
                        padding: '0 3px',
                      }}
                      verticalAlign="middle"
                    >
                      <RegisterButtons
                        mid={edge.node.mid}
                        isWannaReadManga={edge.node.currentUserWannaRead.isExist}
                        isReadedManga={edge.node.currentUserReaded.isExist}
                        width="100px"
                        size="mini"
                        margin="3px 0"
                      />
                    </Grid.Column>
                  </Grid.Row>
                ))
              )
          }
        </Grid>
      </Card.Content>
    </Card>
  );
}
