import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_POPULAR_USERS } from '../../graphql/topQueries';
import UsersCard from '../shared/UsersCard';

export default function PopularUsers() {
  const { loading, data } = useQuery(GET_POPULAR_USERS);

  return (
    <UsersCard
      headerText="人気のユーザー"
      users={data && data.getPopularUsers}
      loading={loading}
    />
  );
}
