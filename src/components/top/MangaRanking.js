import React from 'react';
import { Grid, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import RankingItem from './RankingItem';

export default function MangaRanking({
  mangas, loading, first, width,
}) {
  return (
    <Grid.Row columns={first}>
      {
        loading
          ? (
            <Grid centered>
              <Icon loading name="spinner" />
            </Grid>
          )
          : (
            mangas.map((manga, idx) => (
              <RankingItem
                manga={manga}
                idx={idx}
                key={manga.node.mid}
                width={width}
              />
            ))
          )
      }
    </Grid.Row>
  );
}

MangaRanking.propTypes = {
  mangas: PropTypes.arrayOf(
    PropTypes.shape({
      node: PropTypes.shape({
        mid: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        currentUserWannaRead: PropTypes.shape({
          isExist: PropTypes.bool.isRequired,
        }),
        currentUserReaded: PropTypes.shape({
          isExist: PropTypes.bool.isRequired,
        }),
        reviews: PropTypes.shape({
          average: PropTypes.number.isRequired,
        }),
      }).isRequired,
    }).isRequired,
  ),
  loading: PropTypes.bool.isRequired,
  first: PropTypes.number.isRequired,
  width: PropTypes.string.isRequired,
};

MangaRanking.defaultProps = {
  mangas: [],
};
