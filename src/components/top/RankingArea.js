import React from 'react';
import { useQuery } from '@apollo/client';
import { Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import { GET_SMALL_MANGA_RANKING } from '../../graphql/rankingQueries';
import MangaRanking from './MangaRanking';

export default function RankingArea({
  type, title, genre, first, titleFontSize, margin, border,
  width, gridWidth,
}) {
  const isMobile = useMediaQuery({ maxWidth: 767 });

  const { loading, data } = useQuery(GET_SMALL_MANGA_RANKING, {
    variables: {
      type,
      first,
      genre,
    },
  });

  return (
    <Grid style={{ margin, width: gridWidth }}>
      <Grid.Row columns={2}>
        <Grid.Column width={11}>
          <h1 style={{ fontSize: titleFontSize }}>{title}</h1>
        </Grid.Column>
        <Grid.Column
          width={5}
          verticalAlign="middle"
        >
          <Link
            to={genre ? `/ranking/${type}?genre=${encodeURIComponent(genre)}` : `/ranking/${type}`}
          >
            <div
              style={{
                cursor: 'pointer',
                textAlign: 'right',
              }}
            >
              全部見る
              &gt;
            </div>
          </Link>
        </Grid.Column>
      </Grid.Row>
      <hr
        style={{
          width: '100%',
          margin: isMobile ? '0 0 10px 0' : '0 0 25px 0',
          border,
        }}
      />
      <MangaRanking
        mangas={data && data.getMangaRanking.edges}
        loading={loading}
        first={first}
        width={width}
      />
    </Grid>
  );
}

RankingArea.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  genre: PropTypes.string,
  first: PropTypes.number.isRequired,
  titleFontSize: PropTypes.string,
  margin: PropTypes.string,
  border: PropTypes.string,
  width: PropTypes.string,
  gridWidth: PropTypes.string,
};

RankingArea.defaultProps = {
  genre: '',
  titleFontSize: '',
  margin: '',
  border: 'solid 3px grey',
  width: '145px',
  gridWidth: '',
};
