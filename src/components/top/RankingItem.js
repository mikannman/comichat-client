import React, {
  useState, useEffect, useRef,
} from 'react';
import { useMediaQuery } from 'react-responsive';
import { Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MangaImage from '../shared/MangaImage';
import DisplayMangaStars from '../shared/DisplayMangaStars';
import RegisterButtons from '../shared/RegisterButtons';

export default function RankingItem({ manga, idx, width }) {
  const [isOver, setIsOver] = useState(false);
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const parentElem = useRef();
  const childElem = useRef();

  useEffect(() => {
    const pw = parentElem.current.getBoundingClientRect().width;
    const cw = childElem.current.getBoundingClientRect().width;

    if (pw === cw) {
      setIsOver(true);
    }
  }, []);

  return (
    <Grid.Column
      key={manga.node.mid}
    >
      <Grid centered>
        <Grid.Row
          style={{
            padding: '0',
          }}
        >
          <h3>
            {idx + 1}
            位
          </h3>
        </Grid.Row>
        <Grid.Row
          style={{
            padding: '0 3px',
          }}
        >
          <div
            ref={parentElem}
            style={{
              width: '100%',
            }}
          >
            <Link
              to={`/manga/${encodeURIComponent(manga.node.mid)}`}
              style={{
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <h5
                ref={childElem}
                style={{
                  margin: '5px 0',
                  height: '20px',
                  overflow: 'hidden',
                }}
              >
                {manga.node.title}
              </h5>
              {
                isOver
                  && (
                    <h5
                      style={{
                        margin: '5px 0 5px -1px',
                      }}
                    >
                      …
                    </h5>
                  )
              }
            </Link>
          </div>
        </Grid.Row>
        <Grid.Row
          style={{
            padding: '0',
            margin: isMobile ? '3px 0' : '10px 0',
          }}
        >
          <Link
            to={`/manga/${encodeURIComponent(manga.node.mid)}`}
          >
            <MangaImage
              title={manga.node.title}
              length={isMobile ? 40 : 60}
              mid={manga.node.mid}
              imageUrl={manga.node.imageUrl}
              affiliateUrl={manga.node.affiliateUrl}
            />
          </Link>
        </Grid.Row>
        <Grid.Row
          style={{
            padding: '0',
            margin: '10px 0',
          }}
        >
          <DisplayMangaStars
            reviewsAverage={manga.node.reviews.average}
            size={isMobile ? 'mini' : 'small'}
            height={isMobile ? '20px' : ''}
          />
          <div
            style={{
              margin: '-4px 0 0 2px',
            }}
          >
            {manga.node.reviews.average}
          </div>
        </Grid.Row>
        <Grid.Row
          style={{
            padding: '0',
          }}
        >
          <RegisterButtons
            mid={manga.node.mid}
            isWannaReadManga={manga.node.currentUserWannaRead.isExist}
            isReadedManga={manga.node.currentUserReaded.isExist}
            size="mini"
            width={width}
            padding="10px 0"
            margin="4px 3px"
          />
        </Grid.Row>
      </Grid>
    </Grid.Column>
  );
}

RankingItem.propTypes = {
  idx: PropTypes.number.isRequired,
  width: PropTypes.string.isRequired,
  manga: PropTypes.shape({
    node: PropTypes.shape({
      mid: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
      affiliateUrl: PropTypes.string.isRequired,
      currentUserWannaRead: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }),
      currentUserReaded: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }),
      reviews: PropTypes.shape({
        average: PropTypes.number.isRequired,
      }),
    }).isRequired,
  }).isRequired,
};
