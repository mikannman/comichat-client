import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import MobileRecentReviews from './MobileRecentReviews';
import MobileRankingArea from './MobileRankingArea';

export default function TabletTop() {
  const genres = useSelector((state) => state.genre.data);

  const rankingProps = [
    {
      type: 'weeklyRating',
      title: '今週はこのまんが！',
      first: 3,
      width: '90px',
      margin: '0',
      titleFontSize: '18px',
    },
    {
      type: 'weeklyReviews',
      title: '今週のレビュー数ランキング',
      first: 3,
      width: '90px',
      margin: '3rem 0 0 0',
      titleFontSize: '14px',
    },
    {
      type: 'allTime',
      title: 'オールタイムランキング',
      first: 3,
      width: '90px',
      margin: '3rem 0 0 0',
      titleFontSize: '15px',
    },
  ];

  const genreProps = genres.map((genre, idx) => (
    {
      type: 'weeklyByGenre',
      title: genre,
      genre,
      first: 3,
      titleFontSize: '15px',
      border: 'solid 2px grey',
      width: '88px',
      margin: idx === 0 ? '0' : '3rem 0 0 0',
      gridWidth: '100%',
    }
  ));

  return (
    <div>
      <MobileRecentReviews />
      <Container style={{ marginTop: '20px' }}>
        {
          rankingProps.map((props) => (
            <MobileRankingArea
              {...props}
              key={props.type}
            />
          ))
        }
        <Grid
          style={{
            margin: '3rem 0 0 0',
          }}
        >
          <Grid.Row>
            <h1
              style={{
                fontSize: '20px',
              }}
            >
              ジャンル別週間ランキング
            </h1>
          </Grid.Row>
          <hr
            style={{
              width: '100%',
              border: 'solid 3px grey',
            }}
          />
          {
            genreProps.map((props) => (
              <MobileRankingArea
                {...props}
                key={`${props.type}-${props.genre}`}
              />
            ))
          }
        </Grid>
      </Container>
    </div>
  );
}
