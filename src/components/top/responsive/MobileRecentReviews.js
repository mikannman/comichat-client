import React from 'react';
import { useQuery } from '@apollo/client';
import {
  Container, Grid, Icon, Card, Image,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';
import { Paragraph } from '../../../style/globalStyle';
import { GET_RECENT_REVIEWS } from '../../../graphql/topQueries';
import MangaImage from '../../shared/MangaImage';
import DefaultAccountImage from '../../image/defaultAccountImage.png';
import DisplayStars from '../../shared/DisplayStars';
import MangaTitle from '../../shared/MangaTitle';

const Back = styled.div`
  width: 100vw;
  height: 250px;
  background-color: #92cc66;
`;

const CarouselItem = styled.div`
  height: 180px;
  background-color: white;
  display: flex;
  justify-content: center;
`;

export default function RecentReviews() {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const { loading, data } = useQuery(GET_RECENT_REVIEWS);

  return (
    <Back>
      <Container>
        <div style={{ height: '5px' }} />
        <h1
          style={{
            color: 'white',
            fontSize: '15px',
            margin: '10px',
          }}
        >
          新着レビュー
        </h1>
        {
          loading
            ? (
              <Grid
                centered
                style={{
                  backgroundColor: 'white',
                  height: '180px',
                }}
              >
                <Icon loading name="spinner" />
              </Grid>
            )
            : (
              <Carousel
                showThumbs={false}
                autoPlay
                infiniteLoop
              >
                {
                  data && data.getRecentReviews.map((review) => (
                    <CarouselItem key={review.id}>
                      <div style={{ height: '50px' }} />
                      <Card
                        style={{
                          height: '140px',
                          width: '90%',
                          padding: '10px',
                        }}
                      >
                        <Grid
                          style={{
                            margin: '0',
                          }}
                        >
                          <Grid.Row
                            columns={2}
                            style={{
                              padding: '0',
                            }}
                          >
                            <Grid.Column
                              width={11}
                              style={{
                                padding: '0',
                              }}
                            >
                              <Grid
                                style={{
                                  margin: '0',
                                }}
                              >
                                <Grid.Row
                                  style={{
                                    padding: '0',
                                    margin: '0',
                                  }}
                                >
                                  <Grid.Column
                                    width={3}
                                    verticalAlign="middle"
                                    style={{
                                      padding: '0',
                                    }}
                                  >
                                    <Link to={currentUserId === review.user.id ? '/user/mypage' : `/user/${review.user.id}`}>
                                      <Image
                                        circular
                                        src={review.user.imageURL || DefaultAccountImage}
                                        size="massive"
                                        fluid
                                      />
                                    </Link>
                                  </Grid.Column>
                                  <Grid.Column
                                    width={6}
                                    verticalAlign="middle"
                                  >
                                    <Link to={currentUserId === review.user.id ? '/user/mypage' : `/user/${review.user.id}`}>
                                      <h4
                                        style={{
                                          marginBottom: '3px',
                                          fontSize: '10px',
                                        }}
                                      >
                                        {review.user.name}
                                      </h4>
                                      {
                                        !currentUser.guest
                                          && (
                                            <small
                                              style={{
                                                fontSize: '8px',
                                                wordBreak: 'break-all',
                                              }}
                                            >
                                              {review.user.userId}
                                            </small>
                                          )
                                      }
                                    </Link>
                                  </Grid.Column>
                                  <Grid.Column
                                    width={7}
                                    verticalAlign="middle"
                                    style={{
                                      padding: '0',
                                    }}
                                  >
                                    <DisplayStars
                                      star={review.star}
                                      size="mini"
                                      height="20px"
                                    />
                                  </Grid.Column>
                                </Grid.Row>
                                <Grid.Row
                                  style={{
                                    padding: '0',
                                    margin: '8px 0',
                                  }}
                                >
                                  <div
                                    style={{
                                      height: '60px',
                                      width: '180px',
                                      overflow: 'hidden',
                                    }}
                                  >
                                    <Paragraph
                                      overflow="scroll"
                                      height="70px"
                                      width="190px"
                                      margin="0"
                                    >
                                      <div style={{ fontSize: '10px' }}>
                                        {review.comment}
                                      </div>
                                    </Paragraph>
                                  </div>
                                </Grid.Row>
                              </Grid>
                            </Grid.Column>
                            <Grid.Column
                              width={5}
                              style={{
                                padding: '0',
                              }}
                            >
                              <Link
                                to={`/manga/${encodeURIComponent(review.manga.mid)}`}
                                style={{ display: 'block' }}
                              >
                                <MangaTitle
                                  title={review.manga.title}
                                  fontSize="10px"
                                  height="11px"
                                />
                                <MangaImage
                                  title={review.manga.title}
                                  length={35}
                                  mid={review.manga.mid}
                                  imageUrl={review.manga.imageUrl}
                                  affiliateUrl={review.manga.affiliateUrl}
                                />
                              </Link>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      </Card>
                    </CarouselItem>
                  ))
                }
              </Carousel>
            )
        }
      </Container>
    </Back>
  );
}
