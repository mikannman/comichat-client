import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import RecentReviews from '../RecentReviews';
import RankingArea from '../RankingArea';

export default function TabletTop() {
  const genres = useSelector((state) => state.genre.data);

  const rankingProps = [
    {
      type: 'weeklyRating',
      title: '今週はこのまんががおもしろい！！',
      first: 3,
      width: '100px',
      margin: '3rem 0 0 0',
    },
    {
      type: 'weeklyReviews',
      title: '今週のレビュー数ランキング',
      first: 3,
      width: '100px',
      margin: '3rem 0 0 0',
    },
    {
      type: 'allTime',
      title: 'オールタイムランキング',
      first: 3,
      width: '100px',
      margin: '3rem 0 0 0',
    },
  ];

  const genreProps = genres.map((genre, idx) => (
    {
      type: 'weeklyByGenre',
      title: genre,
      genre,
      first: 3,
      titleFontSize: '20px',
      border: 'solid 2px grey',
      width: '100px',
      margin: idx === 0 ? '0' : '3rem 0 0 0',
      gridWidth: '100%',
    }
  ));

  return (
    <div>
      <RecentReviews />
      <Container style={{ marginTop: '50px' }}>
        {
          rankingProps.map((props) => (
            <RankingArea
              {...props}
              key={props.type}
            />
          ))
        }
        <Grid
          style={{
            margin: '3rem 0 0 0',
          }}
        >
          <Grid.Row>
            <h1>ジャンル別週間ランキング</h1>
          </Grid.Row>
          <hr
            style={{
              width: '100%',
              border: 'solid 3px grey',
            }}
          />
          {
            genreProps.map((props) => (
              <RankingArea
                {...props}
                key={`${props.type}-${props.genre}`}
              />
            ))
          }
        </Grid>
      </Container>
    </div>
  );
}
