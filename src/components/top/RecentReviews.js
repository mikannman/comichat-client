import React from 'react';
import { useQuery } from '@apollo/client';
import {
  Container, Grid, Icon, Card, Image,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';
import { Paragraph, DivSpacer } from '../../style/globalStyle';
import { GET_RECENT_REVIEWS } from '../../graphql/topQueries';
import MangaImage from '../shared/MangaImage';
import DefaultAccountImage from '../image/defaultAccountImage.png';
import DisplayStars from '../shared/DisplayStars';
import MangaTitle from '../shared/MangaTitle';

const Back = styled.div`
  width: 100vw;
  height: 400px;
  background-color: #92cc66;
`;

const CarouselItem = styled.div`
  height: 300px;
  background-color: white;
  display: flex;
  justify-content: center;
`;

export default function RecentReviews() {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const { loading, data } = useQuery(GET_RECENT_REVIEWS);

  return (
    <Back>
      <Container>
        <div style={{ height: '5px' }} />
        <h1
          style={{
            color: 'white',
          }}
        >
          新着レビュー
        </h1>
        {
          loading
            ? (
              <Grid
                centered
                style={{
                  backgroundColor: 'white',
                  height: '300px',
                }}
              >
                <Icon loading name="spinner" />
              </Grid>
            )
            : (
              <Carousel
                showThumbs={false}
                autoPlay
                infiniteLoop
              >
                {
                  data && data.getRecentReviews.map((review) => (
                    <CarouselItem key={review.id}>
                      <div style={{ height: '50px' }} />
                      <Card style={{ height: '250px', width: '600px' }}>
                        <Grid>
                          <Grid.Row columns={2}>
                            <Grid.Column width={11}>
                              <Grid>
                                <Grid.Row
                                  style={{
                                    margin: '15px',
                                  }}
                                >
                                  <Grid.Column
                                    width={4}
                                    verticalAlign="middle"
                                  >
                                    <Link to={currentUserId === review.user.id ? '/user/mypage' : `/user/${review.user.id}`}>
                                      <Image
                                        circular
                                        src={review.user.imageURL || DefaultAccountImage}
                                      />
                                    </Link>
                                  </Grid.Column>
                                  <Grid.Column
                                    width={5}
                                    verticalAlign="middle"
                                    textAlign="left"
                                  >
                                    <Link to={currentUserId === review.user.id ? '/user/mypage' : `/user/${review.user.id}`}>
                                      <h4 style={{ marginBottom: '3px' }}>{review.user.name}</h4>
                                      {
                                        !review.user.guest
                                          && (
                                            <small
                                              style={{
                                                wordBreak: 'break-all',
                                              }}
                                            >
                                              {review.user.userId}
                                            </small>
                                          )
                                      }
                                    </Link>
                                  </Grid.Column>
                                  <Grid.Column
                                    width={7}
                                    verticalAlign="middle"
                                    style={{
                                      padding: '0',
                                    }}
                                  >
                                    <DivSpacer
                                      marginRight="30px"
                                    >
                                      <DisplayStars
                                        star={review.star}
                                        size="large"
                                      />
                                    </DivSpacer>
                                  </Grid.Column>
                                </Grid.Row>
                                <Grid.Row
                                  style={{
                                    padding: '0',
                                  }}
                                >
                                  <div
                                    style={{
                                      height: '110px',
                                      width: '390px',
                                      overflow: 'hidden',
                                    }}
                                  >
                                    <Paragraph
                                      overflow="scroll"
                                      height="120px"
                                      width="400px"
                                    >
                                      <div style={{ fontSize: '17px' }}>
                                        {review.comment}
                                      </div>
                                    </Paragraph>
                                  </div>
                                </Grid.Row>
                              </Grid>
                            </Grid.Column>
                            <Grid.Column width={5}>
                              <Link
                                to={`/manga/${encodeURIComponent(review.manga.mid)}`}
                                style={{ display: 'block' }}
                              >
                                <MangaTitle title={review.manga.title} />
                                <MangaImage
                                  title={review.manga.title}
                                  length={70}
                                  mid={review.manga.mid}
                                  imageUrl={review.manga.imageUrl}
                                  affiliateUrl={review.manga.affiliateUrl}
                                />
                              </Link>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      </Card>
                    </CarouselItem>
                  ))
                }
              </Carousel>
            )
        }
      </Container>
    </Back>
  );
}
