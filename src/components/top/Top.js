import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import RecentReviews from './RecentReviews';
import PopularUsers from './PopularUsers';
import RankingArea from './RankingArea';
import AllTimeRanking from './AllTimeRanking';
import { sliceByNumber } from '../../functions/function';
import AllQuestionsByCondition from '../shared/AllQuestionsByCondition';

export default function Top() {
  const genres = useSelector((state) => state.genre.data);

  const rankingProps = [
    {
      type: 'weeklyRating',
      title: '今週はこのまんががおもしろい！！',
      first: 5,
      margin: '1rem 0 0 0',
    },
    {
      type: 'weeklyReviews',
      title: '今週のレビュー数ランキング',
      first: 5,
      margin: '5rem 0 0 0',
    },
  ];

  const genreProps = genres.map((genre, idx) => (
    {
      type: 'weeklyByGenre',
      title: genre,
      genre,
      first: 3,
      titleFontSize: '20px',
      margin: idx % 2 !== 0 ? '1rem 0 0 15px' : '1rem 15px 0 0',
      border: 'solid 2px grey',
      width: '100px',
    }
  ));
  const genreRankingProps = sliceByNumber(genreProps, 2);

  return (
    <div>
      <RecentReviews />
      <Container style={{ marginTop: '50px' }}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={11}>
              {
                rankingProps.map((props) => (
                  <RankingArea
                    {...props}
                    key={props.type}
                  />
                ))
              }
              <Grid
                style={{
                  margin: '5rem 0 0 0',
                }}
              >
                <Grid.Row>
                  <h1>ジャンル別週間ランキング</h1>
                </Grid.Row>
                <hr
                  style={{
                    width: '100%',
                    border: 'solid 3px grey',
                  }}
                />
                {
                  genreRankingProps.map((row) => (
                    <Grid.Row columns={2} key={`row${row[0].genre}`}>
                      {
                        row.map((props) => (
                          <Grid.Column
                            width={8}
                            style={{ padding: '0' }}
                            key={`w${props.genre}`}
                          >
                            <RankingArea {...props} />
                          </Grid.Column>
                        ))
                      }
                    </Grid.Row>
                  ))
                }
              </Grid>
            </Grid.Column>
            <Grid.Column width={5}>
              <AllQuestionsByCondition
                condition="recent"
                headerText="新しいみんなへの質問"
                path="/questions/list/recent"
              />
              <PopularUsers />
              <AllTimeRanking />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </div>
  );
}
