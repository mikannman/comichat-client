import React from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import {
  Grid, Card, Icon, Image,
} from 'semantic-ui-react';
import { GET_SMALL_ALL_QUESTIONS_BY_CONDITION } from '../../graphql/questionQueries';
import DefaultAccountImage from '../image/defaultAccountImage.png';

export default function RecentQuestionsToAll() {
  const { loading, data } = useQuery(GET_SMALL_ALL_QUESTIONS_BY_CONDITION, {
    variables: {
      condition: 'recent',
      first: 5,
    },
  });

  return (
    <Card>
      <Card.Content>
        <Card.Header>
          新しいみんなへの質問
          <Link to="/questions/list/recent">
            <small
              style={{
                marginLeft: '3px',
                fontSize: '8px',
              }}
            >
              全部見る
            </small>
          </Link>
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <Grid>
          {
            loading
              ? <Icon name="spinner" loading />
              : (
                data && data.getAllQuestionsByCondition.edges.map((edge) => (
                  <Grid.Row columns={3} key={edge.node.id}>
                    <Grid.Column
                      width={3}
                      verticalAlign="middle"
                      style={{
                        padding: '0 3px',
                      }}
                    >
                      <Link
                        to={`/questions/show/${edge.node.id}`}
                      >
                        <Image
                          circular
                          size="tiny"
                          src={edge.node.askUser.imageURL || DefaultAccountImage}
                        />
                      </Link>
                    </Grid.Column>
                    <Grid.Column
                      width={5}
                      verticalAlign="middle"
                      style={{
                        padding: '0',
                      }}
                    >
                      <Link to={`/questions/show/${edge.node.id}`}>
                        <h4 style={{ margin: '0' }}>{edge.node.askUser.name}</h4>
                        <small>{edge.node.askUser.userId}</small>
                      </Link>
                    </Grid.Column>
                    <Grid.Column
                      width={8}
                      style={{
                        padding: '0 3px',
                        height: '56px',
                        overflow: 'hidden',
                      }}
                    >
                      <Link to={`/questions/show/${edge.node.id}`}>
                        {edge.node.content}
                      </Link>
                    </Grid.Column>
                  </Grid.Row>
                ))
              )
          }
        </Grid>
      </Card.Content>
    </Card>
  );
}
