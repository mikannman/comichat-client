import React, { useState, useEffect } from 'react';
import { Container, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import { Transition } from 'react-transition-group';
import { Center, Message } from '../style/globalStyle';
import emitter from '../utils/emitter';

const colorByType = {
  success: 'aqua',
  error: 'red',
};

const Back = styled.div`
  background-color: ${(props) => colorByType[props.type]};
  width: 100vw;
  color: white;
`;

const Close = styled.span`
  cursor: pointer;
  margin-right: 10px;
  align-self: center;
`;

const duration = 300;

const defaultSyle = {
  transition: `all ${duration}ms ease-in-out`,
  transform: 'translateY(-30px)',
  position: 'fixed',
  top: '0',
};

const transitionStyles = {
  entering: { transform: 'translateY(-30px)' },
  entered: { transform: 'translateY(0px)' },
  exiting: { transform: 'translateY(0px)' },
  exited: { transform: 'translateY(-30px)' },
};

export default function Flash() {
  const [visibility, setVisibility] = useState(false);
  const [message, setMessage] = useState('');
  const [type, setType] = useState('');

  useEffect(() => {
    emitter.addListener('flash', ({ messageArg, typeArg }) => {
      setVisibility(true);
      setMessage(messageArg);
      setType(typeArg);
      setTimeout(() => {
        setVisibility(false);
      }, 4000);
    });
  }, []);

  return (
    <Transition in={visibility} timeout={duration}>
      {(state) => (
        <div
          style={{
            ...defaultSyle,
            ...transitionStyles[state],
            zIndex: '99',
          }}
        >
          <Back type={type}>
            <Container>
              <Center height="30px">
                <Close
                  onClick={() => setVisibility(false)}
                >
                  <Icon name="close" />
                </Close>
                <Message alignSelf="center">{message}</Message>
              </Center>
            </Container>
          </Back>
        </div>
      )}
    </Transition>
  );
}
