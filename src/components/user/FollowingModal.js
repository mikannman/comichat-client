import React, {
  useState, useRef, useCallback, useEffect,
} from 'react';
import { useQuery } from '@apollo/client';
import {
  Icon, Modal, Card,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { GET_FOLLOWING } from '../../graphql/userQueries';
import DisplayPagination from '../shared/DisplayPagination';
import UserCard from './UserCard';

const perPage = 10;

export default function FollowingModal({
  userId, name,
}) {
  const [totalPage, setTotalPage] = useState(null);
  const [refetchLoad, setRefetchLoad] = useState(true);
  const dummy = useRef(null);

  const {
    data, loading, refetch,
  } = useQuery(GET_FOLLOWING, {
    variables: {
      id: userId,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getFollowing.totalCount / perPage));
    },
  });

  useEffect(async () => {
    await refetch();
    setRefetchLoad(false);
  }, []);

  const handleChangePage = useCallback(() => {
    dummy.current.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
    });
  }, [dummy]);

  return (
    <>
      <Modal.Header
        style={{
          textAlign: 'center',
          backgroundColor: '#ebebeb',
        }}
      >
        {`${name}さんのフォローしているユーザー一覧`}
      </Modal.Header>
      <Modal.Content scrolling>
        <div ref={dummy} />
        {
          loading || refetchLoad
            ? (
              <Icon loading name="spinner" />
            )
            : !data.getFollowing.edges.length
              ? (
                <div>まだ誰もフォローしていません</div>
              )
              : (
                <Card.Group centered>
                  {
                    data.getFollowing.edges.map((edge) => (
                      <UserCard
                        edge={edge}
                        refetch={refetch}
                        key={edge.node.id}
                      />
                    ))
                  }
                </Card.Group>
              )
        }
      </Modal.Content>
      {
        data && Boolean(data.getFollowing.edges.length)
          && (
            <>
              <hr
                style={{
                  width: '90%',
                  color: 'grey',
                }}
              />
              <Modal.Content
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                <DisplayPagination
                  perPage={perPage}
                  totalPage={totalPage}
                  refetch={refetch}
                  variables={{ id: userId }}
                  handleChangePage={handleChangePage}
                />
              </Modal.Content>
            </>
          )
      }
    </>
  );
}

FollowingModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
