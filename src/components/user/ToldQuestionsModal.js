import React, { useState, useRef, useCallback } from 'react';
import { useQuery } from '@apollo/client';
import {
  Icon, Modal, Card,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { GET_TOLD_QUESTIONS } from '../../graphql/userQueries';
import DisplayPagination from '../shared/DisplayPagination';
import QuestionCard from '../shared/QuestionCard';

const perPage = 10;

export default function ToldQuestionsModal({
  userId, name,
}) {
  const [totalPage, setTotalPage] = useState(null);
  const dummy = useRef(null);

  const {
    data, loading, refetch,
  } = useQuery(GET_TOLD_QUESTIONS, {
    variables: {
      id: userId,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getToldQuestions.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    dummy.current.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
    });
  }, [dummy]);

  return (
    <>
      <Modal.Header
        style={{
          textAlign: 'center',
          backgroundColor: '#ebebeb',
        }}
      >
        {`${name}さんの答えた質問一覧`}
      </Modal.Header>
      <Modal.Content scrolling>
        <div ref={dummy} />
        {
          loading
            ? (
              <Icon loading name="spinner" />
            )
            : !data.getToldQuestions.edges.length
              ? (
                <div>まだ教えたことがありません</div>
              )
              : (
                <Card.Group centered>
                  {
                    data.getToldQuestions.edges.map((edge) => (
                      <QuestionCard
                        question={edge.node}
                        refetch={refetch}
                        key={edge.node.id}
                      />
                    ))
                  }
                </Card.Group>
              )
        }
      </Modal.Content>
      {
        data && Boolean(data.getToldQuestions.edges.length)
          && (
            <>
              <hr
                style={{
                  width: '90%',
                  color: 'grey',
                }}
              />
              <Modal.Content
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                <DisplayPagination
                  perPage={perPage}
                  totalPage={totalPage}
                  refetch={refetch}
                  variables={{ id: userId }}
                  handleChangePage={handleChangePage}
                />
              </Modal.Content>
            </>
          )
      }
    </>
  );
}

ToldQuestionsModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
