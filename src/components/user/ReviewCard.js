import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Card } from 'semantic-ui-react';
import Review from '../shared/Review';
import MangaImage from '../shared/MangaImage';
import MangaTitle from '../shared/MangaTitle';

export default function ReviewCard({ edge, refetch }) {
  return (
    <Card fluid style={{ width: '500px' }}>
      <Card.Content>
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column width={11}>
              <Review
                edge={edge}
                refetch={refetch}
              />
            </Grid.Column>
            <Grid.Column width={5}>
              <Link
                to={`/manga/${encodeURIComponent(edge.node.manga.mid)}`}
                style={{ display: 'block' }}
              >
                <MangaTitle title={edge.node.manga.title} />
                <MangaImage
                  title={edge.node.manga.title}
                  length={60}
                  mid={edge.node.manga.mid}
                  imageUrl={edge.node.manga.imageUrl}
                  affiliateUrl={edge.node.manga.affiliateUrl}
                />
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Card.Content>
    </Card>
  );
}

ReviewCard.propTypes = {
  edge: PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired,
      comment: PropTypes.string.isRequired,
      star: PropTypes.number.isRequired,
      netabare: PropTypes.bool.isRequired,
      updatedAt: PropTypes.string.isRequired,
      likeReviews: PropTypes.shape({
        count: PropTypes.number.isRequired,
      }).isRequired,
      user: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        imageURL: PropTypes.string.isRequired,
      }).isRequired,
      manga: PropTypes.shape({
        mid: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        imageUrl: PropTypes.string.isRequired,
        affiliateUrl: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
  refetch: PropTypes.func.isRequired,
};
