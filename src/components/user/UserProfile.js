import React from 'react';
import { useParams } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import Profile from './Profile';
import MobileProfile from './responsive/MobileProfile';

export default function UserProfile() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const { id } = useParams();

  if (isMobile) {
    return <MobileProfile id={id} />;
  }

  return <Profile id={id} />;
}
