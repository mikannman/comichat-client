import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { setOnMyPage } from '../../redux/features/user/userSlice';
import Profile from './Profile';
import MobileProfile from './responsive/MobileProfile';

export default function MyPage() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const currentUser = useSelector((state) => state.user.value);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setOnMyPage(true));

    return () => dispatch(setOnMyPage(false));
  }, []);

  if (isMobile) {
    return <MobileProfile id={currentUser.id} />;
  }

  return <Profile id={currentUser.id} />;
}
