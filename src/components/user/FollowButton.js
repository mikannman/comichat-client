import React from 'react';
import { useMutation, gql } from '@apollo/client';
import { Button } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { FOLLOW, UNFOLLOW } from '../../graphql/userQueries';

const userFollowFragment = gql`
  fragment userFollowRef on User {
    id
    followers {
      count
    }
    following {
      count
    }
    currentUserFollowed {
      isExist
    }
  }
`;

const currentUserFollowFragment = gql`
  fragment currentUserFollowRef on User {
    id
    followers {
      count
    }
    following {
      count
    }
  }
`;

export default function FollowButton({ id, isFollowed, size }) {
  const currentUser = useSelector((state) => state.user.value);
  const onMyPage = useSelector((state) => state.user.onMyPage);

  const [follow, { loading: followLoding }] = useMutation(FOLLOW, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      const userRef = cache.readFragment({
        id: `User:${id}`,
        fragment: userFollowFragment,
      });

      cache.writeFragment({
        id: `User:${id}`,
        fragment: userFollowFragment,
        data: {
          followers: {
            count: userRef.followers.count + 1,
          },
          currentUserFollowed: {
            isExist: true,
          },
        },
      });

      if (onMyPage) {
        const currentUserRef = cache.readFragment({
          id: `User:${currentUser.id}`,
          fragment: currentUserFollowFragment,
        });

        cache.writeFragment({
          id: `User:${currentUser.id}`,
          fragment: currentUserFollowFragment,
          data: {
            following: {
              count: currentUserRef.following.count + 1,
            },
          },
        });
      }
    },
  });

  const [unfollow, { loading: unfollowLoading }] = useMutation(UNFOLLOW, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      const userRef = cache.readFragment({
        id: `User:${id}`,
        fragment: userFollowFragment,
      });

      cache.writeFragment({
        id: `User:${id}`,
        fragment: userFollowFragment,
        data: {
          followers: {
            count: userRef.followers.count - 1,
          },
          currentUserFollowed: {
            isExist: false,
          },
        },
      });

      if (onMyPage) {
        const currentUserRef = cache.readFragment({
          id: `User:${currentUser.id}`,
          fragment: currentUserFollowFragment,
        });

        cache.writeFragment({
          id: `User:${currentUser.id}`,
          fragment: currentUserFollowFragment,
          data: {
            following: {
              count: currentUserRef.following.count - 1,
            },
          },
        });
      }
    },
  });

  const handleClickFollow = () => {
    follow({ variables: { otherId: id } });
  };

  const handleClickUnfollow = () => {
    unfollow({ variables: { otherId: id } });
  };

  return (
    <div style={{ height: '40px' }}>
      {
        isFollowed
          ? (
            <Button
              color="grey"
              onClick={handleClickUnfollow}
              loading={unfollowLoading}
              size={size}
            >
              フォローを解除する
            </Button>
          )
          : (
            <Button
              color="blue"
              onClick={handleClickFollow}
              loading={followLoding}
              size={size}
            >
              フォローする
            </Button>
          )
      }
    </div>
  );
}

FollowButton.propTypes = {
  id: PropTypes.string.isRequired,
  isFollowed: PropTypes.bool.isRequired,
  size: PropTypes.string,
};

FollowButton.defaultProps = {
  size: '',
};
