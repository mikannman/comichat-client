import React, { useState } from 'react';
import { Grid, Button } from 'semantic-ui-react';
import { useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import firebase from '../../../firebase/config';
import InputArea from '../../shared/InputArea';
import { UPDATE_EMAIL } from '../../../graphql/userQueries';
import { VERIFY_EMAIL } from '../../../graphql/authQueries';
import { setUser } from '../../../redux/features/user/userSlice';
import { setOpen, setCallback } from '../../../redux/features/reauthModal/reauthModalSlice';

export default function UpdateProfile() {
  const emailValidation = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.value);
  const [inputData, setInputData] = useState(currentUser.email);
  const [errors, setErrors] = useState([]);
  const [loading, setLoading] = useState(false);

  const [updateEmail] = useMutation(UPDATE_EMAIL, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: (result) => {
      setLoading(false);
      dispatch(setUser(result.updateEmail.user));
      window.flash('メールアドレスを変更しました');
    },
  });

  const [verifyEmail] = useMutation(VERIFY_EMAIL, {
    onCompleted: (result) => {
      if (result.verifyEmail.registered) {
        setLoading(false);
        setErrors(['このメールアドレスはすでに登録されています']);
      } else {
        firebase.auth().currentUser.updateEmail(inputData)
          .then(() => {
            setErrors([]);
            updateEmail({
              variables: {
                email: inputData,
              },
            });
          })
          .catch((error) => {
            if (error.code === 'auth/requires-recent-login') {
              setErrors([]);
              setLoading(false);
              dispatch(setOpen(true));
              dispatch(setCallback(() => {
                firebase.auth().currentUser.updateEmail(inputData)
                  .then(async () => {
                    await updateEmail({
                      variables: {
                        email: inputData,
                      },
                    });
                    dispatch(setOpen(false));
                    window.flash('メールアドレスを変更しました');
                  });
              }));
            }
          });
      }
    },
  });

  const handleChange = (e) => setInputData(e.target.value);

  const handleSubmit = (e) => {
    e.preventDefault();
    const emailError = [];
    setLoading(true);

    if (inputData.length > 254) {
      emailError.push('メールアドレスが長すぎます');
    } else if (!emailValidation.test(inputData)) {
      emailError.push('メールアドレスの形式が正しくありません');
    } else if (inputData === currentUser.email) {
      emailError.push('メールアドレスが変更されていません');
    }

    if (!emailError.length) {
      verifyEmail({
        variables: {
          email: inputData,
        },
      });
    } else {
      setLoading(false);
      setErrors(emailError);
    }
  };

  return (
    <div>
      <form
        noValidate
        onSubmit={handleSubmit}
      >
        <Grid
          centered
          style={{
            padding: '0 0 30px 0',
            marginTop: '50px',
            marginBottom: '30px',
          }}
        >
          <InputArea
            label="メールアドレス"
            objectKey="email"
            objectType="text"
            value={inputData}
            errors={errors}
            handleChange={handleChange}
          />
          <Grid.Row style={{ marginTop: '10px' }}>
            <Button
              color="teal"
              type="submit"
              disabled={
                !inputData
              }
              loading={loading}
            >
              変更する
            </Button>
          </Grid.Row>
        </Grid>
      </form>
    </div>
  );
}
