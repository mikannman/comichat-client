import React, { useState, useEffect } from 'react';
import { Grid, Menu } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import DisplayAccountSetting from './DisplayAccountSetting';

export default function AccountSetting() {
  const currentUser = useSelector((state) => state.user.value);
  const [activeItem, setActiveItem] = useState('プロフィールを編集');
  const [menus, setMenus] = useState([]);
  useEffect(() => {
    if (currentUser.provider !== 'password') {
      setMenus(['プロフィールを編集', 'アカウントを削除']);
    } else {
      setMenus(['プロフィールを編集', 'メールアドレスを変更', 'パスワードを変更', 'アカウントを削除']);
    }
  }, []);

  const handleClickMenuItem = (_, dataProps) => setActiveItem(dataProps.name);

  return (
    <Grid columns={2} style={{ backgroundColor: 'white' }}>
      <Grid.Column width={5}>
        <Menu fluid vertical tabular>
          {menus.map((menu) => (
            <Menu.Item
              name={menu}
              active={activeItem === menu}
              onClick={handleClickMenuItem}
              key={menu}
            />
          ))}
        </Menu>
      </Grid.Column>
      <Grid.Column width={11}>
        <DisplayAccountSetting
          mode={activeItem}
          id={currentUser.id}
        />
      </Grid.Column>
    </Grid>
  );
}
