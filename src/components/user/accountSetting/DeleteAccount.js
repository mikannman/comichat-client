import React from 'react';
import { Grid, Button } from 'semantic-ui-react';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import firebase from '../../../firebase/config';
import { DELETE_USER } from '../../../graphql/userQueries';
import { resetUser, setIsLogin } from '../../../redux/features/user/userSlice';
import { setToken } from '../../../redux/features/authToken/authTokenSlice';
import { setOpen, setCallback } from '../../../redux/features/reauthModal/reauthModalSlice';
import useConfirm from '../../../hooks/useConfirm';

export default function DeleteAccount() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const dispatch = useDispatch();
  const history = useHistory();
  const confirm = useConfirm();

  const [deleteUser] = useMutation(DELETE_USER, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: () => {
      localStorage.removeItem('user');
      dispatch(setToken(''));
      dispatch(resetUser());
      dispatch(setIsLogin(false));
      history.push('/');
    },
  });

  const handleClick = () => {
    confirm({
      onConfirm: () => {
        firebase.auth().currentUser.delete()
          .then(() => {
            deleteUser();
          })
          .catch((error) => {
            if (error.code === 'auth/requires-recent-login') {
              dispatch(setOpen(true));
              dispatch(setCallback(() => {
                firebase.auth().currentUser.delete()
                  .then(() => {
                    deleteUser();
                  });
              }));
            }
          });
      },
    });
  };

  return (
    <div>
      <Grid
        centered
        style={{
          padding: '0 0 30px 0',
          marginTop: '50px',
          marginBottom: '30px',
        }}
      >
        <Grid.Row>
          <h4
            style={{
              fontSize: isMobile ? '12px' : '',
            }}
          >
            アカウントを削除すると、すべてのデータが消えます
          </h4>
        </Grid.Row>
        <Grid.Row style={{ marginTop: '10px' }}>
          <Button
            color="red"
            onClick={handleClick}
          >
            アカウントを削除する
          </Button>
        </Grid.Row>
      </Grid>
    </div>
  );
}
