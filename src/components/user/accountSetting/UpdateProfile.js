import React, { useState } from 'react';
import {
  Grid, Button, Icon,
} from 'semantic-ui-react';
import { useMutation } from '@apollo/client';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Warning, ClickableText, CircleImage } from '../../../style/globalStyle';
import UserIdInputArea from '../../shared/UserIdInputArea';
import InputArea from '../../shared/InputArea';
import MessageInputArea from '../../shared/MessageInputArea';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import { fileSizeCheck, previewFile } from '../../../functions/function';
import { UPDATE_USER } from '../../../graphql/userQueries';
import { setUser } from '../../../redux/features/user/userSlice';

const ImageLabel = styled.div`
  font-size: 15px;
  color: blue;
  margin-top: 10px;
`;

const CloseIcon = styled.div`
  position: absolute;
  top: 9%;
  right: 37%;
  cursor: pointer;
`;

export default function UpdateProfile() {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.value);
  const [inputData, setInputData] = useState({
    imageURL: currentUser.imageURL,
    userId: currentUser.userId,
    name: currentUser.name,
    message: currentUser.message,
  });
  const [userIDType, setUserIDType] = useState('green');
  const [errors, setErrors] = useState({
    imageURL: [],
    name: [],
    message: [],
    communication: [],
  });

  const [updateUser, { loading }] = useMutation(UPDATE_USER, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: (result) => {
      dispatch(setUser(result.updateUser.user));
      window.flash('プロフィールを更新しました');
    },
  });

  const handleClickDelete = () => {
    setInputData((prev) => ({ ...prev, imageURL: '' }));
  };

  const handleChangeImage = async (e) => {
    try {
      const file = await fileSizeCheck(e.target);
      previewFile(file, 150, (result) => {
        setInputData((prev) => ({ ...prev, imageURL: result }));
      });
    } catch (error) {
      setErrors((prev) => ({ ...prev, imageURL: [error.message] }));
    }
  };

  const handleChangeUserID = (e) => setInputData((prev) => ({ ...prev, userId: e.target.value }));

  const onVerifiedUserId = (type) => setUserIDType(type);

  const handleChangeName = (e) => setInputData((prev) => ({ ...prev, name: e.target.value }));

  const handleChangeMessage = (e) => setInputData((prev) => ({ ...prev, message: e.target.value }));

  const handleSubmit = (e) => {
    e.preventDefault();
    const nameError = [];

    if (inputData.name.length > 200) {
      nameError.push('名前が長すぎます');
    }

    if (!nameError.length) {
      updateUser({
        variables: {
          ...inputData,
        },
      });
    } else {
      setErrors((prev) => ({ ...prev, name: nameError }));
    }
  };

  return (
    <div>
      <form
        noValidate
        onSubmit={handleSubmit}
      >
        <Grid style={{ padding: '0 0 30px 0' }}>
          <Grid.Row>
            <Link
              to="/user/mypage"
              style={{ left: '15%' }}
            >
              <ClickableText color="grey">
                キャンセル
              </ClickableText>
            </Link>
          </Grid.Row>
          <Grid.Row centered>
            {Boolean(inputData.imageURL)
              && (
              <CloseIcon
                onClick={handleClickDelete}
              >
                <Icon name="close" />
              </CloseIcon>
              )}
            <label style={{ cursor: 'pointer' }}>
              <input
                type="file"
                accept="image/*"
                style={{ display: 'none' }}
                onChange={handleChangeImage}
              />
              <CircleImage
                src={inputData.imageURL || DefaultAccontImage}
                alt="プロフィール画像"
                height="120"
                width="120"
              />
              <ImageLabel>プロフィール画像を変更</ImageLabel>
            </label>
            {errors.imageURL.map((errorMessage) => (
              <Warning key={errorMessage}>{errorMessage}</Warning>
            ))}
          </Grid.Row>
          <UserIdInputArea
            value={inputData.userId}
            handleChange={handleChangeUserID}
            onVerifiedUserId={onVerifiedUserId}
          />
          <InputArea
            label="名前"
            objectKey="name"
            objectType="text"
            value={inputData.name}
            errors={errors.name}
            handleChange={handleChangeName}
          />
          <MessageInputArea
            value={inputData.message}
            errors={errors.message}
            handleChange={handleChangeMessage}
          />
          <Grid.Row
            centered
            style={{ marginTop: '10px' }}
          >
            <Button
              color="teal"
              type="submit"
              disabled={
                !inputData.name
                || userIDType === 'red'
              }
              loading={loading}
            >
              保存する
            </Button>
          </Grid.Row>
        </Grid>
      </form>
    </div>
  );
}
