import React, { useState } from 'react';
import { Grid, Button } from 'semantic-ui-react';
import { useDispatch } from 'react-redux';
import firebase from '../../../firebase/config';
import InputArea from '../../shared/InputArea';
import { setOpen, setCallback } from '../../../redux/features/reauthModal/reauthModalSlice';

export default function UpdatePassword() {
  const passwordValidation = /^\w{6,20}$/;
  const dispatch = useDispatch();
  const [inputData, setInputData] = useState({
    newPassword: '',
    newPasswordConfirmation: '',
  });
  const [errors, setErrors] = useState({
    newPassword: [],
    newPasswordConfirmation: [],
  });
  const [loading, setLoading] = useState(false);

  const handleChangeNewPassword = (e) => {
    setInputData((prev) => ({ ...prev, newPassword: e.target.value }));
  };

  const handleChangeNewPasswordConfirmation = (e) => {
    setInputData((prev) => ({ ...prev, newPasswordConfirmation: e.target.value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    const newPasswordError = [];
    const newPasswordConfirmationError = [];

    if (inputData.newPassword !== inputData.newPasswordConfirmation) {
      newPasswordError.push('パスワードと確認用パスワードが一致しません');
    }

    if (!passwordValidation.test(inputData.newPassword)) {
      newPasswordError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
    }

    if (!passwordValidation.test(inputData.newPasswordConfirmation)) {
      newPasswordConfirmationError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
    }

    if (
      !newPasswordError.length
      && !newPasswordConfirmationError.length
    ) {
      firebase.auth().currentUser.updatePassword(inputData.newPassword)
        .then(() => {
          setLoading(false);
          window.flash('パスワードを変更しました');
        })
        .catch((error) => {
          setLoading(false);
          if (error.code === 'auth/requires-recent-login') {
            dispatch(setOpen(true));
            dispatch(setCallback(() => {
              firebase.auth().currentUser.updatePassword(inputData.newPassword)
                .then(() => {
                  dispatch(setOpen(false));
                  window.flash('パスワードを変更しました');
                });
            }));
          }
        });
    } else {
      setLoading(false);
      setErrors({
        newPassword: newPasswordError,
        newPasswordConfirmation: newPasswordConfirmationError,
      });
    }
  };

  return (
    <div>
      <form
        noValidate
        onSubmit={handleSubmit}
      >
        <Grid
          centered
          style={{
            padding: '0 0 30px 0',
            marginTop: '50px',
            marginBottom: '30px',
          }}
        >
          <InputArea
            label="新しいパスワード"
            objectKey="newPassword"
            objectType="password"
            value={inputData.newPassword}
            errors={errors.newPassword}
            handleChange={handleChangeNewPassword}
          />
          <InputArea
            label="新しいパスワードの確認"
            objectKey="newPasswordConfirmation"
            objectType="password"
            value={inputData.newPasswordConfirmation}
            errors={errors.newPasswordConfirmation}
            handleChange={handleChangeNewPasswordConfirmation}
          />
          <Grid.Row style={{ marginTop: '10px' }}>
            <Button
              color="teal"
              type="submit"
              disabled={
                !inputData.newPassword
                && !inputData.newPasswordConfirmation
              }
              loading={loading}
            >
              変更する
            </Button>
          </Grid.Row>
        </Grid>
      </form>
    </div>
  );
}
