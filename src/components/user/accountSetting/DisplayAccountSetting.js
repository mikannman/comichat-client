import React from 'react';
import PropTypes from 'prop-types';
import UpdateProfile from './UpdateProfile';
import UpdateEmail from './UpdateEmail';
import UpdatePassword from './UpdatePassword';
import DeleteAccount from './DeleteAccount';

export default function DisplayAccountSetting({ mode, id }) {
  switch (mode) {
    case 'プロフィールを編集':
      return (
        <UpdateProfile id={id} />
      );
    case 'メールアドレスを変更':
      return (
        <UpdateEmail id={id} />
      );
    case 'パスワードを変更':
      return (
        <UpdatePassword id={id} />
      );
    case 'アカウントを削除':
      return (
        <DeleteAccount id={id} />
      );
    default:
      return (<></>);
  }
}

DisplayAccountSetting.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};
