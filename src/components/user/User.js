import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import {
  Route, Switch, useRouteMatch, Redirect,
} from 'react-router-dom';
import MyPage from './MyPage';
import UserProfile from './UserProfile';
import RecommendedUsers from './RecommendedUsers';
import AccountSetting from './accountSetting/AccountSetting';

export default function User() {
  const match = useRouteMatch();
  const currentUser = useSelector((state) => state.user.value);

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={11}>
            <Switch>
              <Route
                key="myPage"
                path={`${match.path}/mypage`}
              >
                {
                  currentUser
                    ? <MyPage />
                    : <Redirect to="/notFound" />
                }
              </Route>
              <Route key="setting" path={`${match.path}/setting`}>
                {
                  currentUser && !currentUser.guest
                    ? <AccountSetting />
                    : <Redirect to="/notFound" />
                }
              </Route>
              <Route key="userProfile" path={`${match.path}/:id`}>
                <UserProfile />
              </Route>
            </Switch>
          </Grid.Column>
          <Grid.Column width={5}>
            <RecommendedUsers />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
