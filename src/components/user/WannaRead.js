import React from 'react';
import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';
import { ALL_WANNA_READ_MANGAS } from '../../graphql/userQueries';
import UserBottomMangaList from './UserBottomMangaList';
import MobileUserBottomMangaList from './responsive/MobileUserBottomMangaList';

export default function WannaRead({ id }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });

  if (isMobile) {
    return (
      <MobileUserBottomMangaList
        id={id}
        query={ALL_WANNA_READ_MANGAS}
        topField="allWannaReadMangas"
      />
    );
  }

  return (
    <UserBottomMangaList
      id={id}
      query={ALL_WANNA_READ_MANGAS}
      topField="allWannaReadMangas"
    />
  );
}

WannaRead.propTypes = {
  id: PropTypes.string.isRequired,
};
