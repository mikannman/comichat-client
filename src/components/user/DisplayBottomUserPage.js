import React from 'react';
import PropTypes from 'prop-types';
import Readed from './Readed';
import WannaRead from './WannaRead';
import Teach from './teach/Teach';
import Taught from './taught/Taught';

export default function DisplayBottomUserPage({ mode, id }) {
  switch (mode) {
    case '読んだ':
      return (
        <Readed id={id} />
      );
    case '読みたい':
      return (
        <WannaRead id={id} />
      );
    case '教える':
      return (
        <Teach id={id} />
      );
    case '教えてもらう':
      return (
        <Taught id={id} />
      );
    default:
      return (<></>);
  }
}

DisplayBottomUserPage.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};
