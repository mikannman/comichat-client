import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Grid, Image } from 'semantic-ui-react';
import { useMutation, gql } from '@apollo/client';
import { useSelector } from 'react-redux';
import {
  StyledTextarea,
} from '../../../style/globalStyle';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import { CREATE_REPLY } from '../../../graphql/replyQueries';

const query = gql`
  query GetAnswerReplies($id: String, $after: String, $CREATE: Boolean) {
    getAnswerReplies(id: $id, after: $after, CREATE: $CREATE) {
      edges {
        node {
          id
          content
        }
      }
    }
  }
`;

const replyTextareafragment = gql`
  fragment answerReplyTextareaRef on Answer {
    id
    replies {
      count
    }
  }
`;

export default function ReplyTextarea({ id, setOpenReplyTextarea, answerUserId }) {
  const currentUser = useSelector((state) => state.user.value);
  const [textareaValue, setTextareaValue] = useState('');

  const [createReply] = useMutation(CREATE_REPLY, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache, { data: { createReply: cr } }) => {
      const data = cache.readQuery({
        query,
        variables: {
          id,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          id,
          CREATE: true,
        },
        data: {
          getAnswerReplies: {
            edges: [
              {
                node: cr.reply,
                __typename: 'ReplyEdge',
              },
              ...data.getAnswerReplies.edges,
            ],
          },
        },
      });

      const answerRef = cache.readFragment({
        id: `Answer:${id}`,
        fragment: replyTextareafragment,
      });

      cache.writeFragment({
        id: `Answer:${id}`,
        fragment: replyTextareafragment,
        data: {
          replies: {
            count: answerRef.replies.count + 1,
          },
        },
      });
    },
  });

  const handleChangeTextarea = (e) => {
    setTextareaValue(e.target.value);
  };

  const handleClickCancel = () => setOpenReplyTextarea(false);

  const handleClickSubmit = () => {
    createReply({
      variables: {
        answerId: id,
        answerUserId: String(answerUserId),
        content: textareaValue,
      },
    });
    setTextareaValue('');
  };

  return (
    <Grid>
      <Grid.Row>
        <Image
          src={currentUser.imageURL || DefaultAccontImage}
          alt="ユーザー画像"
          avatar
        />
        <StyledTextarea
          value={textareaValue}
          onChange={handleChangeTextarea}
          border="solid 1px black"
          width="280px"
          height="40px"
          padding="5px"
        />
      </Grid.Row>
      <Grid.Row
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          paddingTop: '0',
        }}
      >
        <Button
          onClick={handleClickCancel}
        >
          キャンセル
        </Button>
        <Button
          onClick={handleClickSubmit}
        >
          返信
        </Button>
      </Grid.Row>
    </Grid>
  );
}

ReplyTextarea.propTypes = {
  id: PropTypes.string.isRequired,
  answerUserId: PropTypes.string.isRequired,
  setOpenReplyTextarea: PropTypes.func.isRequired,
};
