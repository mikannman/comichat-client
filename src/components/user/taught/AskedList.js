import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Card,
} from 'semantic-ui-react';
import { useQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { GET_ASKED_LIST } from '../../../graphql/taughtQueries';
import QuestionCard from '../../shared/QuestionCard';
import MobileQuestionCard from '../../shared/responsive/MobileQuestionCard';
import DisplayPagination from '../../shared/DisplayPagination';

const perPage = 5;

export default function AskedList({ id }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [totalPage, setTotalPage] = useState(0);

  const {
    loading, data, refetch,
  } = useQuery(GET_ASKED_LIST, {
    variables: {
      id,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getAskedList.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (!data.getAskedList.edges.length) {
    return <div>まだ聞いてみたことがありません</div>;
  }

  return (
    <Grid centered>
      <Card.Group>
        {
          data.getAskedList.edges.map((edge) => (
            isMobile
              ? (
                <MobileQuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
              : (
                <QuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
          ))
        }
      </Card.Group>
      <Grid.Row>
        <DisplayPagination
          perPage={perPage}
          totalPage={totalPage}
          refetch={refetch}
          handleChangePage={handleChangePage}
        />
      </Grid.Row>
    </Grid>
  );
}

AskedList.propTypes = {
  id: PropTypes.string.isRequired,
};
