import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Label,
} from 'semantic-ui-react';
import AnswerFeed from './AnswerFeed';

const perPage = 5;

export default function AnswerArea({
  getQuestionAnswers, setShowAnswerOpen, loading, fetchMore,
  questionId,
}) {
  const [count, setCount] = useState(1);
  const [isMoreData, setIsMoreData] = useState(false);

  const handleClickShowMore = async () => {
    setIsMoreData(true);
    await fetchMore({
      variables: {
        after: btoa(String(count * perPage)),
      },
    });
    setIsMoreData(false);
    setCount((prev) => prev + 1);
  };

  const handleClickClose = () => {
    setShowAnswerOpen(false);
  };

  return (
    <Grid>
      {
        getQuestionAnswers.edges.map((answer) => (
          <Grid.Row key={answer.node.id}>
            <Grid.Column>
              <AnswerFeed
                answer={answer}
                questionId={questionId}
                key={answer.node.id}
              />
            </Grid.Column>
          </Grid.Row>
        ))
      }
      {
        loading || isMoreData
          ? <Icon loading name="spinner" />
          : (
            <Grid.Row centered>
              {
                getQuestionAnswers.edges.length === getQuestionAnswers.totalCount
                  ? (
                    <Label
                      as="a"
                      onClick={handleClickClose}
                    >
                      閉じる
                    </Label>
                  )
                  : (
                    <Label
                      as="a"
                      onClick={handleClickShowMore}
                    >
                      もっとみる
                    </Label>
                  )
              }
            </Grid.Row>
          )
      }
    </Grid>
  );
}

AnswerArea.propTypes = {
  loading: PropTypes.bool.isRequired,
  fetchMore: PropTypes.func.isRequired,
  setShowAnswerOpen: PropTypes.func.isRequired,
  questionId: PropTypes.string.isRequired,
  getQuestionAnswers: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          id: PropTypes.string.isRequired,
          content: PropTypes.string.isRequired,
          updatedAt: PropTypes.string.isRequired,
          replies: PropTypes.shape({
            count: PropTypes.number.isRequired,
          }).isRequired,
          likeAnswers: PropTypes.shape({
            count: PropTypes.number.isRequired,
          }).isRequired,
          user: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            imageURL: PropTypes.string.isRequired,
          }).isRequired,
          currentUserLikes: PropTypes.shape({
            isExist: PropTypes.bool.isRequired,
          }).isRequired,
        }).isRequired,
      }).isRequired,
    ).isRequired,
    totalCount: PropTypes.number.isRequired,
  }).isRequired,
};
