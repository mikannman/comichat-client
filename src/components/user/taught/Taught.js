import React, { useState } from 'react';
import { Grid, Menu } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import Ask from './Ask';
import AskedList from './AskedList';
import AnsweredList from './AnsweredList';
import UnansweredList from './UnansweredList';

function DisplayTaughtPage({ mode, id }) {
  switch (mode) {
    case '聞いてみる':
      return (
        <Ask id={id} />
      );
    case '聞いてみた一覧':
      return (
        <AskedList id={id} />
      );
    case '回答済み一覧':
      return (
        <AnsweredList id={id} />
      );
    case '未回答一覧':
      return (
        <UnansweredList id={id} />
      );
    default:
      return (<></>);
  }
}

DisplayTaughtPage.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default function Taught({ id }) {
  const [activeItem, setActiveItem] = useState('聞いてみる');
  const menus = ['聞いてみる', '聞いてみた一覧', '回答済み一覧', '未回答一覧'];

  const handleClickMenu = (_, { name }) => setActiveItem(name);

  return (
    <Grid.Row>
      <Grid.Column width={4}>
        <Menu fluid vertical tabular>
          {menus.map((menu) => (
            <Menu.Item
              name={menu}
              active={activeItem === menu}
              onClick={handleClickMenu}
              key={menu}
            />
          ))}
        </Menu>
      </Grid.Column>
      <Grid.Column width={12}>
        <DisplayTaughtPage
          id={id}
          mode={activeItem}
        />
      </Grid.Column>
    </Grid.Row>
  );
}

Taught.propTypes = {
  id: PropTypes.string.isRequired,
};
