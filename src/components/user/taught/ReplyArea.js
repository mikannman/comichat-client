import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Label,
} from 'semantic-ui-react';
import { useQuery } from '@apollo/client';
import { GET_ANSWER_REPLIES } from '../../../graphql/replyQueries';
import ReplyFeed from './ReplyFeed';

const perPage = 5;

export default function ReplyArea({ id, setOpenReply }) {
  const [count, setCount] = useState(1);
  const [isMoreData, setIsMoreData] = useState(false);

  const {
    loading, fetchMore, data,
  } = useQuery(GET_ANSWER_REPLIES, {
    variables: {
      id,
      first: perPage,
      after: btoa(String(0)),
    },
  });

  const handleClickShowMore = async () => {
    setIsMoreData(true);
    await fetchMore({
      variables: {
        after: btoa(String(count * perPage)),
      },
    });
    setIsMoreData(false);
    setCount((prev) => prev + 1);
  };

  const handleClickClose = () => {
    setOpenReply(false);
  };

  return (
    <Grid centered>
      {
        data && data.getAnswerReplies.edges.map((reply) => (
          <ReplyFeed
            answerId={id}
            reply={reply}
            key={reply.node.id}
          />
        ))
      }
      {
        loading || isMoreData
          ? <Icon loading name="spinner" />
          : (
            Boolean(data) && Boolean(data.getAnswerReplies.edges.length)
              && (
                <Grid.Row>
                  {
                    data.getAnswerReplies.edges.length === data.getAnswerReplies.totalCount
                      ? (
                        <Label
                          as="a"
                          onClick={handleClickClose}
                        >
                          閉じる
                        </Label>
                      )
                      : (
                        <Label
                          as="a"
                          onClick={handleClickShowMore}
                        >
                          もっとみる
                        </Label>
                      )
                  }
                </Grid.Row>
              )
          )
      }
    </Grid>
  );
}

ReplyArea.propTypes = {
  id: PropTypes.string.isRequired,
  setOpenReply: PropTypes.func.isRequired,
};
