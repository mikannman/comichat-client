import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Grid } from 'semantic-ui-react';
import { StyledTextarea } from '../../../style/globalStyle';

export default function FeedEdit({
  id, content, updateFunc, setEditMode,
}) {
  const [textareaValue, setTextAreaValue] = useState(content);

  const handleChangeTextarea = (e) => {
    setTextAreaValue(e.target.value);
  };

  const handleClickEditCancel = () => {
    setEditMode(false);
  };

  const handleClickEditSubmit = () => {
    setEditMode(false);
    updateFunc({
      variables: {
        id,
        content: textareaValue,
      },
    });
  };

  return (
    <Grid>
      <Grid.Row>
        <StyledTextarea
          value={textareaValue}
          onChange={handleChangeTextarea}
          border="solid 1px black"
          height="60px"
        />
      </Grid.Row>
      <Grid.Row
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <Button
          onClick={handleClickEditCancel}
        >
          キャンセル
        </Button>
        <Button
          onClick={handleClickEditSubmit}
        >
          更新
        </Button>
      </Grid.Row>
    </Grid>
  );
}

FeedEdit.propTypes = {
  id: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  updateFunc: PropTypes.func.isRequired,
  setEditMode: PropTypes.func.isRequired,
};
