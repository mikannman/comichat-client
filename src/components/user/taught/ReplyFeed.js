import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Feed, Dropdown,
} from 'semantic-ui-react';
import { useMutation, gql } from '@apollo/client';
import { useSelector } from 'react-redux';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import { howLongAgo } from '../../../functions/function';
import { DELETE_REPLY, UPDATE_REPLY } from '../../../graphql/replyQueries';
import Like from '../../shared/Like';
import FeedEdit from './FeedEdit';
import FeedContent from './FeedContent';
import useConfirm from '../../../hooks/useConfirm';

const query = gql`
  query GetAnswerReplies($id: String, $after: String, $DELETE: Boolean,) {
    getAnswerReplies(id: $id, after: $after, DELETE: $DELETE) {
      edges {
        node {
          id
          content
        }
      }
    }
  }
`;

const answerReplyfragment = gql`
  fragment answerReplyRef on Answer {
    id
    replies {
      count
    }
  }
`;

export default function ReplyFeed({ reply, answerId }) {
  const currentUser = useSelector((state) => state.user.value);
  const [editMode, setEditMode] = useState(false);
  const confirm = useConfirm();

  const [updateReply, { loading: updateLoading }] = useMutation(UPDATE_REPLY, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const [deleteReply] = useMutation(DELETE_REPLY, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      const data = cache.readQuery({
        query,
        variables: {
          id: answerId,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          id: answerId,
          DELETE: true,
        },
        data: {
          getAnswerReplies: {
            edges: data.getAnswerReplies.edges.filter((edge) => (
              edge.node.id !== reply.node.id
            )),
          },
        },
      });

      const answerRef = cache.readFragment({
        id: `Answer:${answerId}`,
        fragment: answerReplyfragment,
      });

      cache.writeFragment({
        id: `Answer:${answerId}`,
        fragment: answerReplyfragment,
        data: {
          replies: {
            count: answerRef.replies.count - 1,
          },
        },
      });
    },
  });

  const handleClickEdit = () => setEditMode(true);

  const handleClickDelete = () => {
    confirm({
      onConfirm: () => deleteReply({ variables: { id: reply.node.id } }),
    });
  };

  return (
    <Grid.Row centered>
      <Grid.Column width={12}>
        <Feed>
          <Feed.Event>
            <Feed.Label image={reply.node.user.imageURL || DefaultAccontImage} />
            <Feed.Content>
              <Feed.Summary>
                {reply.node.user.name}
                <Feed.Date>{howLongAgo(reply.node.updatedAt)}</Feed.Date>
                {
                  currentUser.id === reply.node.user.id
                    && (
                      <Dropdown
                        icon="ellipsis vertical"
                        style={{ alignSelf: 'center', color: 'grey', margin: '0 0 0 10px' }}
                      >
                        <Dropdown.Menu>
                          <Dropdown.Item
                            onClick={handleClickEdit}
                          >
                            編集
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={handleClickDelete}
                          >
                            削除
                          </Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    )
                }
              </Feed.Summary>
              {
                updateLoading
                  ? <Icon loading name="spinner" />
                  : editMode
                    ? (
                      <FeedEdit
                        id={reply.node.id}
                        content={reply.node.content}
                        updateFunc={updateReply}
                        setEditMode={setEditMode}
                      />
                    )
                    : (
                      <FeedContent
                        content={reply.node.content}
                      />
                    )
              }
              <Feed.Meta>
                <Feed.Like>
                  <Like
                    id={reply.node.id}
                    type="Reply"
                    isLike={reply.node.currentUserLikes.isExist}
                  />
                  {reply.node.likeReplies.count}
                </Feed.Like>
              </Feed.Meta>
            </Feed.Content>
          </Feed.Event>
        </Feed>
      </Grid.Column>
    </Grid.Row>
  );
}

ReplyFeed.propTypes = {
  answerId: PropTypes.string.isRequired,
  reply: PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
      likeReplies: PropTypes.shape({
        count: PropTypes.number.isRequired,
      }).isRequired,
      user: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        imageURL: PropTypes.string.isRequired,
      }).isRequired,
      currentUserLikes: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};
