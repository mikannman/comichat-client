import React from 'react';
import CreateQuestionForm from '../../shared/CreateQuestionForm';

export default function Ask() {
  return (
    <CreateQuestionForm
      labelWidth={6}
    />
  );
}
