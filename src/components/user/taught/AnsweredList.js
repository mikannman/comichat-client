import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Card,
} from 'semantic-ui-react';
import { useQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { GET_ANSWERED_LIST } from '../../../graphql/taughtQueries';
import QuestionCard from '../../shared/QuestionCard';
import MobileQuestionCard from '../../shared/responsive/MobileQuestionCard';
import DisplayPagination from '../../shared/DisplayPagination';

const perPage = 5;

export default function AnsweredList({ id }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [totalPage, setTotalPage] = useState(0);

  const {
    loading, data, refetch,
  } = useQuery(GET_ANSWERED_LIST, {
    variables: {
      id,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getAnsweredList.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (!data.getAnsweredList.edges.length) {
    return <div>回答済みの質問がありません</div>;
  }

  return (
    <Grid centered>
      <Card.Group>
        {
          data.getAnsweredList.edges.map((edge) => (
            isMobile
              ? (
                <MobileQuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
              : (
                <QuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
          ))
        }
      </Card.Group>
      <Grid.Row>
        <DisplayPagination
          perPage={perPage}
          totalPage={totalPage}
          refetch={refetch}
          handleChangePage={handleChangePage}
        />
      </Grid.Row>
    </Grid>
  );
}

AnsweredList.propTypes = {
  id: PropTypes.string.isRequired,
};
