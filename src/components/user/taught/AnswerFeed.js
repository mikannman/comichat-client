import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Label, Feed, Dropdown,
} from 'semantic-ui-react';
import { useMutation, gql } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import { howLongAgo } from '../../../functions/function';
import { DELETE_ANSWER, UPDATE_ANSWER } from '../../../graphql/answerQueries';
import { setConfirmConfig } from '../../../redux/features/confirm/confirmSlice';
import Like from '../../shared/Like';
import FeedEdit from './FeedEdit';
import FeedContent from './FeedContent';
import ReplyArea from './ReplyArea';
import ReplyTextarea from './ReplyTextarea';

const query = gql`
  query GetQuestionAnswers($id: String, $after: String, $DELETE: Boolean) {
    getQuestionAnswers(id: $id, after: $after, DELETE: $DELETE) {
      edges {
        node {
          id
          content
        }
      }
      totalCount
    }
  }
`;

const userAnswerFragment = gql`
  fragment userRef on User {
    id
    answers {
      count
    }
  }
`;

export default function AnswerFeed({ answer, questionId }) {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const onMyPage = useSelector((state) => state.user.onMyPage);
  const [editMode, setEditMode] = useState(false);
  const [openReply, setOpenReply] = useState(false);
  const [openReplyTextarea, setOpenReplyTextarea] = useState(false);

  const [updateAnswer, { loading: updateLoading }] = useMutation(UPDATE_ANSWER, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const [deleteAnswer] = useMutation(DELETE_ANSWER, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      const data = cache.readQuery({
        query,
        variables: {
          id: questionId,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          id: questionId,
          DELETE: true,
        },
        data: {
          getQuestionAnswers: {
            edges: data.getQuestionAnswers.edges.filter((edge) => (
              edge.node.id !== answer.node.id
            )),
            totalCount: data.getQuestionAnswers.totalCount - 1,
          },
        },
      });

      if (onMyPage) {
        const currentUserRef = cache.readFragment({
          id: `User:${currentUser.id}`,
          fragment: userAnswerFragment,
        });

        cache.writeFragment({
          id: `User:${currentUser.id}`,
          fragment: userAnswerFragment,
          data: {
            answers: {
              count: currentUserRef.answers.count - 1,
            },
          },
        });
      }
    },
  });

  const handleClickEdit = () => setEditMode(true);

  const handleClickDelete = () => {
    dispatch(setConfirmConfig({
      open: true,
      content: '本当に削除しますか',
      onConfirm: () => deleteAnswer({ variables: { id: answer.node.id } }),
    }));
  };

  const handleClickOpenReplyTextarea = () => {
    setOpenReplyTextarea(true);
    setOpenReply(true);
  };

  const handleClickOpenReply = () => setOpenReply(true);

  const handleClickCloseReply = () => setOpenReply(false);

  return (
    <Grid>
      <Grid.Row centered>
        <Grid.Column width={14}>
          <Feed>
            <Feed.Event>
              <Feed.Label image={answer.node.user.imageURL || DefaultAccontImage} />
              <Feed.Content>
                <Feed.Summary>
                  {answer.node.user.name}
                  <Feed.Date>{howLongAgo(answer.node.updatedAt)}</Feed.Date>
                  {
                    currentUserId === answer.node.user.id
                      && (
                        <Dropdown
                          icon="ellipsis vertical"
                          style={{ alignSelf: 'center', color: 'grey', margin: '0 0 0 10px' }}
                        >
                          <Dropdown.Menu>
                            <Dropdown.Item
                              onClick={handleClickEdit}
                            >
                              編集
                            </Dropdown.Item>
                            <Dropdown.Item
                              onClick={handleClickDelete}
                            >
                              削除
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      )
                  }
                </Feed.Summary>
                {
                  updateLoading
                    ? <Icon loading name="spinner" />
                    : editMode
                      ? (
                        <FeedEdit
                          id={answer.node.id}
                          content={answer.node.content}
                          updateFunc={updateAnswer}
                          setEditMode={setEditMode}
                        />
                      )
                      : (
                        <FeedContent
                          content={answer.node.content}
                        />
                      )
                }
                <Feed.Meta>
                  <Feed.Like>
                    <Like
                      id={answer.node.id}
                      type="Answer"
                      isLike={answer.node.currentUserLikes.isExist}
                    />
                    {answer.node.likeAnswers.count}
                  </Feed.Like>
                  <Icon
                    name="reply"
                    onClick={handleClickOpenReplyTextarea}
                  />
                  {
                    openReplyTextarea
                      && (
                        <ReplyTextarea
                          id={answer.node.id}
                          answerUserId={String(answer.node.user.id)}
                          setOpenReplyTextarea={setOpenReplyTextarea}
                        />
                      )
                  }
                  {
                    Boolean(answer.node.replies.count)
                      && (
                        openReply
                          ? (
                            <Label
                              as="a"
                              onClick={handleClickCloseReply}
                            >
                              返信を非表示
                            </Label>
                          )
                          : (
                            <Label
                              as="a"
                              onClick={handleClickOpenReply}
                            >
                              {`${answer.node.replies.count}件の返信を表示`}
                            </Label>
                          )
                      )
                  }
                </Feed.Meta>
              </Feed.Content>
            </Feed.Event>
          </Feed>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row centered columns={1}>
        <Grid.Column width={16}>
          {
            openReply
              && (
                <ReplyArea
                  id={answer.node.id}
                  setOpenReply={setOpenReply}
                />
              )
          }
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

AnswerFeed.propTypes = {
  questionId: PropTypes.string.isRequired,
  answer: PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
      replies: PropTypes.shape({
        count: PropTypes.number.isRequired,
      }).isRequired,
      likeAnswers: PropTypes.shape({
        count: PropTypes.number.isRequired,
      }).isRequired,
      user: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        imageURL: PropTypes.string.isRequired,
      }).isRequired,
      currentUserLikes: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};
