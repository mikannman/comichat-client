import React, { useCallback, useState } from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { Grid, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { sliceByNumber } from '../../functions/function';
import MangaImage from '../shared/MangaImage';
import DisplayPagination from '../shared/DisplayPagination';

const perPage = 9;

export default function UserBottomMangaList({ query, topField, id }) {
  const [totalPage, setTotalPage] = useState(0);
  const {
    loading, data, refetch,
  } = useQuery(query, {
    variables: {
      id,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result[topField].totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Grid.Row columns={1}>
      <Grid.Column width={16}>
        {
          data[topField].totalCount
            ? (
              <Grid>
                {
                  sliceByNumber(data[topField].edges, 3).map((row) => (
                    <Grid.Row columns={3} key={`row${row[0].node.mid}`}>
                      {
                        row.map((manga) => (
                          <Grid.Column
                            key={manga.node.mid}
                          >
                            <Grid>
                              <Grid.Row centered>
                                <Link to={`/manga/${encodeURIComponent(manga.node.mid)}`}>
                                  <MangaImage
                                    title={manga.node.title}
                                    length={100}
                                    mid={manga.node.mid}
                                    imageUrl={manga.node.imageUrl}
                                    affiliateUrl={manga.node.affiliateUrl}
                                  />
                                </Link>
                              </Grid.Row>
                              <Grid.Row centered>
                                <Link to={`/manga/${encodeURIComponent(manga.node.mid)}`}>
                                  <h5>
                                    {manga.node.title}
                                  </h5>
                                </Link>
                              </Grid.Row>
                            </Grid>
                          </Grid.Column>
                        ))
                      }
                    </Grid.Row>
                  ))
                }
                <Grid.Row centered>
                  <DisplayPagination
                    perPage={perPage}
                    totalPage={totalPage}
                    refetch={refetch}
                    handleChangePage={handleChangePage}
                  />
                </Grid.Row>
              </Grid>

            )
            : (
              <div>
                まだ登録されていません
              </div>
            )
        }
      </Grid.Column>
    </Grid.Row>
  );
}

UserBottomMangaList.propTypes = {
  id: PropTypes.string.isRequired,
  query: PropTypes.string.isRequired,
  topField: PropTypes.string.isRequired,
};
