import React, { useState, useRef, useCallback } from 'react';
import { useQuery } from '@apollo/client';
import {
  Icon, Modal, Card,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { GET_FOLLOWERS } from '../../graphql/userQueries';
import DisplayPagination from '../shared/DisplayPagination';
import UserCard from './UserCard';

const perPage = 10;

export default function FollowersModal({
  userId, name,
}) {
  const [totalPage, setTotalPage] = useState(null);
  const dummy = useRef(null);

  const {
    data, loading, refetch,
  } = useQuery(GET_FOLLOWERS, {
    variables: {
      id: userId,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getFollowers.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    dummy.current.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
    });
  }, [dummy]);

  return (
    <>
      <Modal.Header
        style={{
          textAlign: 'center',
          backgroundColor: '#ebebeb',
        }}
      >
        {`${name}さんのフォロワー一覧`}
      </Modal.Header>
      <Modal.Content scrolling>
        <div ref={dummy} />
        {
          loading
            ? (
              <Icon loading name="spinner" />
            )
            : !data.getFollowers.edges.length
              ? (
                <div>まだフォロワーがいません</div>
              )
              : (
                <Card.Group centered>
                  {
                    data.getFollowers.edges.map((edge) => (
                      <UserCard
                        edge={edge}
                        refetch={refetch}
                        key={edge.node.id}
                      />
                    ))
                  }
                </Card.Group>
              )
        }
      </Modal.Content>
      {
        data && Boolean(data.getFollowers.edges.length)
          && (
            <>
              <hr
                style={{
                  width: '90%',
                  color: 'grey',
                }}
              />
              <Modal.Content
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                <DisplayPagination
                  perPage={perPage}
                  totalPage={totalPage}
                  refetch={refetch}
                  variables={{ id: userId }}
                  handleChangePage={handleChangePage}
                />
              </Modal.Content>
            </>
          )
      }
    </>
  );
}

FollowersModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
