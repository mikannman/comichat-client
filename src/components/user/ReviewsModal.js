import React, { useState, useRef, useCallback } from 'react';
import { useQuery } from '@apollo/client';
import {
  Icon, Modal, Card,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { GET_USER_REVIEWS } from '../../graphql/userQueries';
import DisplayPagination from '../shared/DisplayPagination';
import ReviewCard from './ReviewCard';

const perPage = 10;

export default function ReviewsModal({
  userId, name,
}) {
  const [totalPage, setTotalPage] = useState(null);
  const dummy = useRef(null);

  const {
    data, loading, refetch,
  } = useQuery(GET_USER_REVIEWS, {
    variables: {
      id: userId,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getUserReviews.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    dummy.current.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
    });
  }, [dummy]);

  return (
    <>
      <Modal.Header
        style={{
          textAlign: 'center',
          backgroundColor: '#ebebeb',
        }}
      >
        {`${name}さんのレビュー一覧`}
      </Modal.Header>
      <Modal.Content scrolling>
        <div ref={dummy} />
        {
          loading
            ? (
              <Icon loading name="spinner" />
            )
            : !data.getUserReviews.edges.length
              ? (
                <div>まだレビューを書いたことがありません</div>
              )
              : (
                <Card.Group centered>
                  {
                    data.getUserReviews.edges.map((edge) => (
                      <ReviewCard
                        edge={edge}
                        refetch={refetch}
                        key={edge.node.id}
                      />
                    ))
                  }
                </Card.Group>
              )
        }
      </Modal.Content>
      {
        data && Boolean(data.getUserReviews.edges.length)
          && (
            <>
              <hr
                style={{
                  width: '90%',
                  color: 'grey',
                }}
              />
              <Modal.Content
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                <DisplayPagination
                  perPage={perPage}
                  totalPage={totalPage}
                  refetch={refetch}
                  variables={{ id: userId }}
                  handleChangePage={handleChangePage}
                />
              </Modal.Content>
            </>
          )
      }
    </>
  );
}

ReviewsModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
