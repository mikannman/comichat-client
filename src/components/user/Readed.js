import React from 'react';
import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';
import { ALL_READED_MANGAS } from '../../graphql/userQueries';
import UserBottomMangaList from './UserBottomMangaList';
import MobileUserBottomMangaList from './responsive/MobileUserBottomMangaList';

export default function Readed({ id }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });

  if (isMobile) {
    return (
      <MobileUserBottomMangaList
        query={ALL_READED_MANGAS}
        topField="allReadedMangas"
        id={id}
      />
    );
  }

  return (
    <UserBottomMangaList
      query={ALL_READED_MANGAS}
      topField="allReadedMangas"
      id={id}
    />
  );
}

Readed.propTypes = {
  id: PropTypes.string.isRequired,
};
