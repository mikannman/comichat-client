import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Card,
} from 'semantic-ui-react';
import { useQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { GET_ASKED_ME_QUESTIONS } from '../../../graphql/teachQueries';
import QuestionCard from '../../shared/QuestionCard';
import MobileQuestionCard from '../../shared/responsive/MobileQuestionCard';
import DisplayPagination from '../../shared/DisplayPagination';

const perPage = 5;

export default function WannaAskYou({ id }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [totalPage, setTotalPage] = useState(null);

  const {
    loading, data, refetch,
  } = useQuery(GET_ASKED_ME_QUESTIONS, {
    variables: {
      id,
      first: perPage,
    },
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getAskedMeQuestions.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (!data.getAskedMeQuestions.edges.length) {
    return <div>まだあなたへの質問はありません</div>;
  }

  return (
    <Grid centered>
      <Card.Group>
        {
          data.getAskedMeQuestions.edges.map((edge) => (
            isMobile
              ? (
                <MobileQuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
              : (
                <QuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
          ))
        }
      </Card.Group>
      <Grid.Row>
        <DisplayPagination
          perPage={perPage}
          totalPage={totalPage}
          refetch={refetch}
          handleChangePage={handleChangePage}
        />
      </Grid.Row>
    </Grid>
  );
}

WannaAskYou.propTypes = {
  id: PropTypes.string.isRequired,
};
