import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Card,
} from 'semantic-ui-react';
import { useQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { GET_ASKED_ALL_QUESTIONS } from '../../../graphql/teachQueries';
import QuestionCard from '../../shared/QuestionCard';
import MobileQuestionCard from '../../shared/responsive/MobileQuestionCard';
import DisplayPagination from '../../shared/DisplayPagination';

const perPage = 5;

export default function WannaAskEveryone({ id }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [totalPage, setTotalPage] = useState(null);

  const {
    loading, data, refetch,
  } = useQuery(GET_ASKED_ALL_QUESTIONS, {
    variables: {
      id,
      first: perPage,
    },
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.getAskedAllQuestions.totalCount / perPage));
    },
  });

  const handleChangePage = useCallback(() => {
    window.scroll(0, 0);
  }, []);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (!data.getAskedAllQuestions.edges.length) {
    return <div>まだ質問がありません</div>;
  }

  return (
    <Grid centered>
      <Card.Group>
        {
          data.getAskedAllQuestions.edges.map((edge) => (
            isMobile
              ? (
                <MobileQuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
              : (
                <QuestionCard
                  question={edge.node}
                  refetch={refetch}
                  key={edge.node.id}
                />
              )
          ))
        }
      </Card.Group>
      <Grid.Row>
        <DisplayPagination
          perPage={perPage}
          totalPage={totalPage}
          refetch={refetch}
          handleChangePage={handleChangePage}
        />
      </Grid.Row>
    </Grid>
  );
}

WannaAskEveryone.propTypes = {
  id: PropTypes.string.isRequired,
};
