import React, { useState } from 'react';
import { Grid, Menu } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import WannaAskYou from './WannaAskYou';
import WannaAskEveryone from './WannaAskEveryone';
import ToldList from './ToldList';

function DisplayTeachPage({ mode, id }) {
  switch (mode) {
    case 'あなたに聞きたい':
      return (
        <WannaAskYou id={id} />
      );
    case 'みんなに聞きたい':
      return (
        <WannaAskEveryone id={id} />
      );
    case 'おしえたもの一覧':
      return (
        <ToldList id={id} />
      );
    default:
      return (<></>);
  }
}

DisplayTeachPage.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default function Teach({ id }) {
  const onMyPage = useSelector((state) => state.user.onMyPage);
  const [activeItem, setActiveItem] = useState('あなたに聞きたい');
  const menus = onMyPage
    ? ['あなたに聞きたい', 'みんなに聞きたい', 'おしえたもの一覧']
    : ['みんなに聞きたい', 'おしえたもの一覧'];

  const handleClickMenu = (_, { name }) => setActiveItem(name);

  return (
    <Grid.Row>
      <Grid.Column width={4}>
        <Menu fluid vertical tabular>
          {menus.map((menu) => (
            <Menu.Item
              name={menu}
              active={activeItem === menu}
              onClick={handleClickMenu}
              key={menu}
            />
          ))}
        </Menu>
      </Grid.Column>
      <Grid.Column width={12}>
        <DisplayTeachPage
          id={id}
          mode={activeItem}
        />
      </Grid.Column>
    </Grid.Row>
  );
}

Teach.propTypes = {
  id: PropTypes.string.isRequired,
};
