import React from 'react';
import { useQuery } from '@apollo/client';
import { Icon } from 'semantic-ui-react';
import { GET_RECOMMENDED_USERS } from '../../graphql/userQueries';
import UsersCard from '../shared/UsersCard';

export default function RecommendedUsers() {
  const { loading, error, data } = useQuery(GET_RECOMMENDED_USERS);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (error) {
    return <div>{error.message}</div>;
  }

  return (
    <UsersCard
      headerText="おすすめのユーザー"
      users={data.getRecommendedUsers}
    />
  );
}
