import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import FollowingModal from './FollowingModal';
import FollowersModal from './FollowersModal';
import ReviewsModal from './ReviewsModal';
import ToldQuestionsModal from './ToldQuestionsModal';

export default function DisplaySubInfoModal({
  userId, name, subInfoModalOpen, setSubInfoModalOpen,
}) {
  const handleOnClose = () => {
    setSubInfoModalOpen((prev) => ({ ...prev, open: false }));
  };

  return (
    <Modal
      open={subInfoModalOpen.open}
      onClose={handleOnClose}
      size="small"
    >
      {
        subInfoModalOpen.open
          && (
            <SubInfoModal
              userId={userId}
              name={name}
              mode={subInfoModalOpen.name}
            />
          )
      }
    </Modal>
  );
}

DisplaySubInfoModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  subInfoModalOpen: PropTypes.shape({
    name: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
  }).isRequired,
  setSubInfoModalOpen: PropTypes.func.isRequired,
};

function SubInfoModal({
  userId, name, mode,
}) {
  switch (mode) {
    case 'フォロー中':
      return (
        <FollowingModal
          userId={userId}
          name={name}
        />
      );
    case 'フォロワー':
      return (
        <FollowersModal
          userId={userId}
          name={name}
        />
      );
    case 'レビュー数':
      return (
        <ReviewsModal
          userId={userId}
          name={name}
        />
      );
    case '回答数':
      return (
        <ToldQuestionsModal
          userId={userId}
          name={name}
        />
      );
    default:
      return <></>;
  }
}

SubInfoModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
};
