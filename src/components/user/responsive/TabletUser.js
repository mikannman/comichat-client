import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import {
  Route, Switch, useRouteMatch, Redirect,
} from 'react-router-dom';
import MyPage from '../MyPage';
import UserProfile from '../UserProfile';
import AccountSetting from '../accountSetting/AccountSetting';

export default function TabletUser() {
  const match = useRouteMatch();
  const currentUser = useSelector((state) => state.user.value);

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <Switch>
              <Route
                key="myPage"
                path={`${match.path}/mypage`}
              >
                {
                  currentUser
                    ? <MyPage />
                    : <Redirect to="/notFound" />
                }
              </Route>
              <Route key="setting" path={`${match.path}/setting`}>
                {
                  currentUser && !currentUser.guest
                    ? <AccountSetting />
                    : <Redirect to="/notFound" />
                }
              </Route>
              <Route key="userProfile" path={`${match.path}/:id`}>
                <UserProfile />
              </Route>
            </Switch>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
