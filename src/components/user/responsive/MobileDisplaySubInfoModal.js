import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import MobileFollowingModal from './MobileFollowingModal';
import MobileFollowersModal from './MobileFollowersModal';
import MobileReviewsModal from './MobileReviewsModal';
import MobileToldQuestionsModal from './MobileToldQuestionsModal';

export default function MobileDisplaySubInfoModal({
  userId, name, subInfoModalOpen, setSubInfoModalOpen,
}) {
  const handleOnClose = () => {
    setSubInfoModalOpen((prev) => ({ ...prev, open: false }));
  };

  return (
    <Modal
      open={subInfoModalOpen.open}
      onClose={handleOnClose}
      size="small"
    >
      {
        subInfoModalOpen.open
          && (
            <SubInfoModal
              userId={userId}
              name={name}
              mode={subInfoModalOpen.name}
              handleOnClose={handleOnClose}
            />
          )
      }
    </Modal>
  );
}

MobileDisplaySubInfoModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  subInfoModalOpen: PropTypes.shape({
    name: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
  }).isRequired,
  setSubInfoModalOpen: PropTypes.func.isRequired,
};

function SubInfoModal({
  userId, name, mode, handleOnClose,
}) {
  switch (mode) {
    case 'フォロー中':
      return (
        <MobileFollowingModal
          userId={userId}
          name={name}
          handleOnClose={handleOnClose}
        />
      );
    case 'フォロワー':
      return (
        <MobileFollowersModal
          userId={userId}
          name={name}
          handleOnClose={handleOnClose}
        />
      );
    case 'レビュー数':
      return (
        <MobileReviewsModal
          userId={userId}
          name={name}
          handleOnClose={handleOnClose}
        />
      );
    case '回答数':
      return (
        <MobileToldQuestionsModal
          userId={userId}
          name={name}
          handleOnClose={handleOnClose}
        />
      );
    default:
      return <></>;
  }
}

SubInfoModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
  handleOnClose: PropTypes.func.isRequired,
};
