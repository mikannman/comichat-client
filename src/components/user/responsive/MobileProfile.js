import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import {
  Button, Grid, Menu, Icon,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { CircleImage } from '../../../style/globalStyle';
import { GET_USER } from '../../../graphql/userQueries';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import MobileDisplayBottomUserPage from './MobileDisplayBottomUserPage';
import FollowButton from '../FollowButton';
import QuestionModal from '../../shared/QuestionModal';
import MobileDisplaySubInfoModal from './MobileDisplaySubInfoModal';

const menus = ['読んだ', '読みたい', '教える', '教えて'];

const Name = styled.div`
  font-size: 20px;
  font-weight: bold;
`;

export default function Profile({ id }) {
  const currentUser = useSelector((state) => state.user.value);
  const isLogin = useSelector((state) => state.user.isLogin);
  const [activeItem, setActiveItem] = useState('読んだ');
  const [modalOpen, setModalOpen] = useState(false);
  const [subInfoModalOpen, setSubInfoModalOpen] = useState({
    name: '',
    open: false,
  });
  const history = useHistory();

  const { loading, data } = useQuery(GET_USER, {
    variables: { id },
    fetchPolicy: 'network-only',
  });

  const handleClickQuestionButton = () => setModalOpen(true);

  const handleClickEditProfileButton = () => {
    history.push('/user/setting');
  };

  const handleClickSubInfo = (e) => {
    setSubInfoModalOpen({
      name: e.target.dataset.name,
      open: true,
    });
  };

  const handleClickMenuItem = (_, dataProps) => setActiveItem(dataProps.name);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Grid style={{ backgroundColor: '#fff' }}>
      <Grid.Row centered>
        <CircleImage
          src={data.getUser.imageURL || DefaultAccontImage}
          alt="プロフィール画像"
          height="90"
          width="90"
        />
      </Grid.Row>
      <Grid.Row
        centered
        style={{
          padding: '10px 0 0 0',
        }}
      >
        <Name>
          {data.getUser.name}
        </Name>
      </Grid.Row>
      {
        !currentUser.guest
          && (
            <Grid.Row
              centered
              style={{
                padding: '0 0 0 0',
              }}
            >
              <small>{data.getUser.userId}</small>
            </Grid.Row>
          )
      }
      {
        currentUser
          && (
            currentUser.id !== id
              ? (
                <Grid.Row centered>
                  {
                    isLogin && (
                      <FollowButton
                        id={id}
                        isFollowed={data.getUser.currentUserFollowed.isExist}
                      />
                    )
                  }
                  <Button
                    onClick={handleClickQuestionButton}
                    color="teal"
                    style={{
                      marginLeft: '5px',
                    }}
                  >
                    おすすめを聞いてみる
                  </Button>
                </Grid.Row>
              )
              : !currentUser.guest
                && (
                  <Grid.Row centered>
                    <Button
                      onClick={handleClickEditProfileButton}
                    >
                      プロフィールを編集
                    </Button>
                  </Grid.Row>
                )
          )
      }
      <Grid.Row columns={2} centered>
        {
          [
            { name: 'フォロー中', data: data.getUser.following.count },
            { name: 'フォロワー', data: data.getUser.followers.count },
          ].map((info) => (
            <Grid.Column
              width={6}
              key={info.name}
              data-name={info.name}
              onClick={handleClickSubInfo}
              style={{
                padding: '0',
                fontSize: '14px',
                cursor: 'pointer',
              }}
            >
              {`${info.name}: ${info.data}`}
            </Grid.Column>
          ))
        }
      </Grid.Row>
      <Grid.Row columns={2} centered>
        {
          [
            { name: 'レビュー数', data: data.getUser.reviews.count },
            { name: '回答数', data: data.getUser.answers.count },
          ].map((info) => (
            <Grid.Column
              width={6}
              key={info.name}
              data-name={info.name}
              onClick={handleClickSubInfo}
              style={{
                padding: '0',
                fontSize: '14px',
                cursor: 'pointer',
              }}
            >
              {`${info.name}: ${info.data}`}
            </Grid.Column>
          ))
        }
      </Grid.Row>
      <hr
        style={{
          width: '100%',
          margin: '15px',
          color: 'grey',
        }}
      />
      <Grid.Row style={{ padding: '20px 100px' }}>
        {data.getUser.message}
      </Grid.Row>
      <Menu tabular widths={4}>
        {menus.map((menu) => (
          <Menu.Item
            name={menu}
            active={activeItem === menu}
            onClick={handleClickMenuItem}
            key={menu}
          />
        ))}
      </Menu>
      <MobileDisplayBottomUserPage mode={activeItem} id={id} />
      <QuestionModal
        userId={data.getUser.id}
        name={data.getUser.name}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
      />
      <MobileDisplaySubInfoModal
        userId={data.getUser.id}
        name={data.getUser.name}
        subInfoModalOpen={subInfoModalOpen}
        setSubInfoModalOpen={setSubInfoModalOpen}
      />
    </Grid>
  );
}

Profile.propTypes = {
  id: PropTypes.string.isRequired,
};
