import React, { useEffect } from 'react';
import { Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import WannaAskYou from '../teach/WannaAskYou';
import WannaAskEveryone from '../teach/WannaAskEveryone';
import ToldList from '../teach/ToldList';
import {
  setMobileUserDropdownConfig, setMobileUserDropdownActiveItem,
  setMobileUserDropdownVisible,
} from '../../../redux/features/mobileUserDropdown/mobileUserDropdownSlice';

function DisplayTeachPage({ mode, id }) {
  switch (mode) {
    case 'あなたに聞きたい':
      return (
        <WannaAskYou id={id} />
      );
    case 'みんなに聞きたい':
      return (
        <WannaAskEveryone id={id} />
      );
    case 'おしえたもの一覧':
      return (
        <ToldList id={id} />
      );
    default:
      return (<></>);
  }
}

DisplayTeachPage.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default function MobileTeach({ id }) {
  const dispatch = useDispatch();
  const onMyPage = useSelector((state) => state.user.onMyPage);
  const activeItem = useSelector((state) => state.mobileUserDropdown.activeItem);
  const menus = onMyPage
    ? ['あなたに聞きたい', 'みんなに聞きたい', 'おしえたもの一覧']
    : ['みんなに聞きたい', 'おしえたもの一覧'];

  const handleClickMenu = (_, { name }) => {
    dispatch(setMobileUserDropdownActiveItem(name));
  };

  useEffect(() => {
    dispatch(setMobileUserDropdownConfig({
      visible: true,
      menus,
      activeItem: onMyPage ? 'あなたに聞きたい' : 'みんなに聞きたい',
      handleClickMenu,
    }));

    return () => {
      dispatch(setMobileUserDropdownVisible(false));
    };
  }, []);

  return (
    <Grid.Row>
      <Grid.Column width={16}>
        <DisplayTeachPage
          id={id}
          mode={activeItem}
        />
      </Grid.Column>
    </Grid.Row>
  );
}

MobileTeach.propTypes = {
  id: PropTypes.string.isRequired,
};
