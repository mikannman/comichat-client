import React, { useEffect } from 'react';
import { Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import Ask from '../taught/Ask';
import AskedList from '../taught/AskedList';
import AnsweredList from '../taught/AnsweredList';
import UnansweredList from '../taught/UnansweredList';
import {
  setMobileUserDropdownConfig, setMobileUserDropdownActiveItem,
  setMobileUserDropdownVisible,
} from '../../../redux/features/mobileUserDropdown/mobileUserDropdownSlice';

function DisplayTaughtPage({ mode, id }) {
  switch (mode) {
    case '聞いてみる':
      return (
        <Ask id={id} />
      );
    case '聞いてみた一覧':
      return (
        <AskedList id={id} />
      );
    case '回答済み一覧':
      return (
        <AnsweredList id={id} />
      );
    case '未回答一覧':
      return (
        <UnansweredList id={id} />
      );
    default:
      return (<></>);
  }
}

DisplayTaughtPage.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default function MobileTaught({ id }) {
  const dispatch = useDispatch();
  const activeItem = useSelector((state) => state.mobileUserDropdown.activeItem);
  const menus = ['聞いてみる', '聞いてみた一覧', '回答済み一覧', '未回答一覧'];

  const handleClickMenu = (_, { name }) => {
    dispatch(setMobileUserDropdownActiveItem(name));
  };

  useEffect(() => {
    dispatch(setMobileUserDropdownConfig({
      visible: true,
      menus,
      activeItem: '聞いてみる',
      handleClickMenu,
    }));

    return () => {
      dispatch(setMobileUserDropdownVisible(false));
    };
  }, []);

  return (
    <Grid.Row>
      <Grid.Column width={16}>
        <DisplayTaughtPage
          id={id}
          mode={activeItem}
        />
      </Grid.Column>
    </Grid.Row>
  );
}

MobileTaught.propTypes = {
  id: PropTypes.string.isRequired,
};
