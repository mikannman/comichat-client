import React, { useState, useEffect } from 'react';
import { Grid, Dropdown } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import DisplayAccountSetting from '../accountSetting/DisplayAccountSetting';

export default function MobileAccountSetting() {
  const currentUser = useSelector((state) => state.user.value);
  const [activeItem, setActiveItem] = useState('プロフィールを編集');
  const [menus, setMenus] = useState([]);
  useEffect(() => {
    if (currentUser.provider !== 'password') {
      setMenus(['プロフィールを編集', 'アカウントを削除']);
    } else {
      setMenus(['プロフィールを編集', 'メールアドレスを変更', 'パスワードを変更', 'アカウントを削除']);
    }
  }, []);

  const handleClickMenuItem = (_, dataProps) => setActiveItem(dataProps.name);

  return (
    <Grid columns={2} style={{ backgroundColor: 'white' }}>
      <Dropdown
        icon="bars"
        button
        className="icon"
        pointing="top left"
        style={{
          position: 'fixed',
          bottom: '17%',
          left: '5%',
          zIndex: '50',
          backgroundColor: '#76c639',
          color: 'white',
        }}
      >
        <Dropdown.Menu>
          {
            menus.map((menu) => (
              <Dropdown.Item
                name={menu}
                onClick={handleClickMenuItem}
                key={menu}
                style={{
                  backgroundColor: menu === activeItem ? '#76c639' : '',
                  color: menu === activeItem ? 'white' : '',
                }}
              >
                {menu}
              </Dropdown.Item>
            ))
          }
        </Dropdown.Menu>
      </Dropdown>
      <Grid.Column width={16}>
        <DisplayAccountSetting
          mode={activeItem}
          id={currentUser.id}
        />
      </Grid.Column>
    </Grid>
  );
}
