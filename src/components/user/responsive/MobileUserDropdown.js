import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import PropTypes from 'prop-types';

export default function MobileUserDropdown({
  menus, handleClickMenu, activeItem,
}) {
  return (
    <Dropdown
      icon="bars"
      button
      className="icon"
      pointing="top left"
      style={{
        position: 'sticky',
        bottom: '5%',
        left: '5%',
        zIndex: '50',
        backgroundColor: '#76c639',
        color: 'white',
      }}
    >
      <Dropdown.Menu>
        {
          menus.map((menu) => (
            <Dropdown.Item
              name={menu}
              onClick={handleClickMenu}
              key={menu}
              style={{
                backgroundColor: menu === activeItem ? '#76c639' : '',
                color: menu === activeItem ? 'white' : '',
              }}
            >
              {menu}
            </Dropdown.Item>
          ))
        }
      </Dropdown.Menu>
    </Dropdown>
  );
}

MobileUserDropdown.propTypes = {
  menus: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleClickMenu: PropTypes.func.isRequired,
  activeItem: PropTypes.string.isRequired,
};
