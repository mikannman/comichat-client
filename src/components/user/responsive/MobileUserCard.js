import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {
  Grid, Card, Image,
} from 'semantic-ui-react';
import { Row, RowItem } from '../../../style/globalStyle';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import FollowButton from '../FollowButton';
import StrechContent from '../../shared/StrechContent';

export default function MobileUserCard({ edge }) {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;

  return (
    <Card fluid style={{ width: '500px' }}>
      <Card.Content>
        <Grid style={{ padding: '15px' }}>
          <Grid.Row
            columns={2}
            verticalAlign="middle"
          >
            <Grid.Column
              width={4}
              style={{
                padding: '0',
              }}
            >
              <Link to={`/user/${edge.node.id}`}>
                <Image
                  src={edge.node.imageURL || DefaultAccontImage}
                  alt="ユーザー画像"
                  size="massive"
                  avatar
                />
              </Link>
            </Grid.Column>
            <Grid.Column width={12}>
              <Row
                justifyContent="space-between"
              >
                <RowItem
                  fontWeight="bold"
                  margin="0 5px 0 0"
                >
                  <Link to={`/user/${edge.node.id}`}>
                    {edge.node.name}
                  </Link>
                </RowItem>
                {
                  edge.node.id !== currentUserId
                    && (
                      <FollowButton
                        id={edge.node.id}
                        isFollowed={edge.node.currentUserFollowed.isExist}
                        size="mini"
                      />
                    )
                }
              </Row>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={2}>
            <Grid.Column width={8}>
              {`フォロー中: ${edge.node.following.count}`}
            </Grid.Column>
            <Grid.Column width={8}>
              {`フォロワー: ${edge.node.followers.count}`}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={2}>
            <Grid.Column width={8}>
              {`レビュー数: ${edge.node.reviews.count}`}
            </Grid.Column>
            <Grid.Column width={8}>
              {`回答数: ${edge.node.answers.count}`}
            </Grid.Column>
          </Grid.Row>
          {
            edge.node.message
              && (
                <Grid.Row>
                  <Grid.Column width={16}>
                    <StrechContent
                      content={edge.node.message}
                    />
                  </Grid.Column>
                </Grid.Row>
              )
          }
        </Grid>
      </Card.Content>
    </Card>
  );
}

MobileUserCard.propTypes = {
  edge: PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      following: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      followers: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      reviews: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      answers: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      currentUserFollowed: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};
