import React from 'react';
import PropTypes from 'prop-types';
import Readed from '../Readed';
import WannaRead from '../WannaRead';
import MobileTeach from './MobileTeach';
import MobileTaught from './MobileTaught';

export default function MobileDisplayBottomUserPage({ mode, id }) {
  switch (mode) {
    case '読んだ':
      return (
        <Readed id={id} />
      );
    case '読みたい':
      return (
        <WannaRead id={id} />
      );
    case '教える':
      return (
        <MobileTeach id={id} />
      );
    case '教えて':
      return (
        <MobileTaught id={id} />
      );
    default:
      return (<></>);
  }
}

MobileDisplayBottomUserPage.propTypes = {
  mode: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};
