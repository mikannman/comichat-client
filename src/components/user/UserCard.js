import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {
  Grid, Label, Card, Image,
} from 'semantic-ui-react';
import { Row, RowItem } from '../../style/globalStyle';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import FollowButton from './FollowButton';

const duration = 200;

const defaultSyle = {
  transition: `all ${duration}ms ease`,
  whiteSpace: 'pre-wrap',
  textAlign: 'left',
  maxHeight: '57px',
  overflow: 'hidden',
};

export default function UserCard({ edge }) {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const [readMore, setReadMore] = useState({ height: '', state: false });
  const [readMoreOpen, setReadMoreOpen] = useState({ transition: false, label: false });
  const contentElem = useRef(null);

  useEffect(() => {
    if (contentElem.current.getBoundingClientRect().height > 57) {
      setReadMore({
        height: `${contentElem.current.getBoundingClientRect().height}px`,
        state: true,
      });
    }
  }, []);

  const transitionStyles = {
    entering: {
      maxHeight: '57px',
    },
    entered: {
      maxHeight: readMore.height,
    },
    exiting: {
      maxHeight: readMore.height,
    },
    exited: {
      maxHeight: '57px',
    },
  };

  const handleClickReadMoreLabel = () => {
    setReadMoreOpen((prev) => ({ ...prev, transition: !prev.transition }));
    setTimeout(() => {
      setReadMoreOpen((prev) => ({ ...prev, label: !prev.label }));
    }, 400);
  };

  return (
    <Card fluid style={{ width: '500px' }}>
      <Card.Content>
        <Grid style={{ padding: '15px' }}>
          <Grid.Row
            columns={2}
            verticalAlign="middle"
          >
            <Grid.Column width={4}>
              <Link to={`/user/${edge.node.id}`}>
                <Image
                  src={edge.node.imageURL || DefaultAccontImage}
                  alt="ユーザー画像"
                  size="massive"
                  avatar
                />
              </Link>
            </Grid.Column>
            <Grid.Column width={12}>
              <Row
                justifyContent="space-between"
              >
                <RowItem
                  fontWeight="bold"
                >
                  <Link to={`/user/${edge.node.id}`}>
                    {edge.node.name}
                  </Link>
                </RowItem>
                {
                  edge.node.id !== currentUserId
                    && (
                      <FollowButton
                        id={edge.node.id}
                        isFollowed={edge.node.currentUserFollowed.isExist}
                      />
                    )
                }
              </Row>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={4}>
            <Grid.Column width={4}>
              {`フォロー中: ${edge.node.following.count}`}
            </Grid.Column>
            <Grid.Column width={4}>
              {`フォロワー: ${edge.node.followers.count}`}
            </Grid.Column>
            <Grid.Column width={4}>
              {`レビュー数: ${edge.node.reviews.count}`}
            </Grid.Column>
            <Grid.Column width={4}>
              {`回答数: ${edge.node.answers.count}`}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row
            style={{
              margin: '15px',
            }}
          >
            <Transition in={readMoreOpen.transition} timeout={duration}>
              {(state) => (
                <div
                  style={{
                    ...defaultSyle,
                    ...transitionStyles[state],
                  }}
                >
                  <div
                    ref={contentElem}
                  >
                    {edge.node.message}
                  </div>
                </div>
              )}
            </Transition>
          </Grid.Row>
          {
            readMore.state
              && (
                <Grid.Row centered>
                  <Label
                    onClick={handleClickReadMoreLabel}
                    as="a"
                  >
                    {readMoreOpen.label ? '戻す' : '続きを読む'}
                  </Label>
                </Grid.Row>
              )
          }
        </Grid>
      </Card.Content>
    </Card>
  );
}

UserCard.propTypes = {
  edge: PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      following: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      followers: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      reviews: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      answers: PropTypes.objectOf(
        PropTypes.number.isRequired,
      ).isRequired,
      currentUserFollowed: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};
