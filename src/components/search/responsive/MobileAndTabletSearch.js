import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import SearchMangaListArea from '../../shared/SearchMangaListArea';

export default function Search({ searchWord, searchCondition }) {
  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <SearchMangaListArea word={searchWord} condition={searchCondition} isSearch />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}

Search.propTypes = {
  searchWord: PropTypes.string.isRequired,
  searchCondition: PropTypes.string.isRequired,
};
