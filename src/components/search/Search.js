import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import SearchMangaListArea from '../shared/SearchMangaListArea';
import PopularKeywords from '../shared/PopularKeywords';

export default function Search({ searchWord, searchCondition }) {
  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={11}>
            <SearchMangaListArea word={searchWord} condition={searchCondition} isSearch />
          </Grid.Column>
          <Grid.Column width={5}>
            <PopularKeywords />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}

Search.propTypes = {
  searchWord: PropTypes.string.isRequired,
  searchCondition: PropTypes.string.isRequired,
};
