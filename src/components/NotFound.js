import React from 'react';
import { Container, Grid } from 'semantic-ui-react';

export default function NotFound() {
  return (
    <Container>
      <Grid centered>
        <Grid.Row>
          <h1>
            404 NotFound
          </h1>
        </Grid.Row>
        <Grid.Row>
          <p>
            ページが見つかりません
          </p>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
