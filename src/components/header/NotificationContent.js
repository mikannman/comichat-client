import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import StrechContent from '../shared/StrechContent';

const upperStyle = {
  padding: '3px 0 0 0',
};

const bottomStyle = {
  padding: '3px 0 0 0',
};

const gridStyle = {
  margin: '0',
};

export default function NotificationContent({ notification }) {
  switch (notification.action) {
    case 'answerLike':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたの
            <Link to={`/questions/${notification.answer.questionId}`}>
              回答
            </Link>
            にいいねしました
          </Grid.Row>
          <Grid.Row style={bottomStyle} columns={1}>
            <Grid.Column width={16}>
              あなたの回答：
              <StrechContent
                content={notification.answer.content}
                rowMargin="0"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    case 'replyLike':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたの
            <Link to={`/questions/${notification.reply.answer.questionId}`}>
              返信
            </Link>
            にいいねしました
          </Grid.Row>
          <Grid.Row style={bottomStyle} columns={1}>
            <Grid.Column width={16}>
              あなたの返信：
              <StrechContent
                content={notification.reply.content}
                rowMargin="0"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    case 'reviewLike':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたの
            <Link to={`/manga/${encodeURIComponent(notification.review.mid)}`}>
              レビュー
            </Link>
            にいいねしました
          </Grid.Row>
          <Grid.Row style={bottomStyle} columns={1}>
            <Grid.Column width={16}>
              あなたのレビュー：
              <StrechContent
                content={notification.review.comment}
                rowMargin="0"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    case 'question':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたに
            <Link to={`/questions/${notification.question.id}`}>
              質問
            </Link>
            をしています
          </Grid.Row>
          <Grid.Row style={bottomStyle} columns={1}>
            <Grid.Column width={16}>
              質問：
              <StrechContent
                content={notification.question.content}
                rowMargin="0"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    case 'answer':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたの
            <Link to={`/questions/${notification.answer.questionId}`}>
              質問
            </Link>
            に回答しました
          </Grid.Row>
          <Grid.Row style={bottomStyle} columns={1}>
            <Grid.Column width={16}>
              回答：
              <StrechContent
                content={notification.answer.content}
                rowMargin="0"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    case 'reply':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたの
            <Link to={`/questions/${notification.reply.answer.questionId}`}>
              回答
            </Link>
            に返信しました
          </Grid.Row>
          <Grid.Row style={bottomStyle} columns={1}>
            <Grid.Column width={16}>
              返信：
              <StrechContent
                content={notification.reply.content}
                rowMargin="0"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    case 'follow':
      return (
        <Grid style={gridStyle}>
          <Grid.Row style={upperStyle}>
            あなたをフォローしました
          </Grid.Row>
        </Grid>
      );
    default:
      return <></>;
  }
}

NotificationContent.propTypes = {
  notification: PropTypes.shape({
    id: PropTypes.string.isRequired,
    visitor: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
    }).isRequired,
    question: PropTypes.shape({
      id: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
    answer: PropTypes.shape({
      content: PropTypes.string.isRequired,
      questionId: PropTypes.string.isRequired,
    }).isRequired,
    reply: PropTypes.shape({
      content: PropTypes.string.isRequired,
      answer: PropTypes.shape({
        questionId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    review: PropTypes.shape({
      mid: PropTypes.string.isRequired,
      comment: PropTypes.string.isRequired,
    }).isRequired,
    action: PropTypes.string.isRequired,
  }).isRequired,
};
