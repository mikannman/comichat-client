import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, Grid } from 'semantic-ui-react';
import Notification from './Notification';

export default function Notifications({
  notifications, totalCount, fetchMore, perPage,
}) {
  const [count, setCount] = useState(1);
  const [isGetMoreData, setIsGetMoreData] = useState(false);
  const [gridHeight, setGridHeight] = useState(0);
  const gridElem = useRef();

  useEffect(() => {
    setGridHeight(gridElem.current.getBoundingClientRect().height);
  }, []);

  const handleClickShowMore = async () => {
    setIsGetMoreData(true);
    await fetchMore({
      variables: {
        after: btoa(String(count * perPage)),
      },
    });
    setIsGetMoreData(false);
    setCount((prev) => prev + 1);
  };

  return (
    <div
      style={{
        height: `${gridHeight - 10}px`,
        overflow: 'hidden',
        width: '300px',
      }}
    >
      <div
        ref={gridElem}
        style={{
          overflow: 'scroll',
          maxHeight: '500px',
        }}
      >
        <Grid
          style={{
            margin: '0',
          }}
        >
          {
            notifications.map((notification) => (
              <Notification
                notification={notification.node}
                key={notification.node.id}
              />
            ))
          }
          {
            isGetMoreData
              ? <Icon loading name="spinner" />
              : (
                notifications.length !== totalCount
                  && (
                    <Grid.Row centered>
                      <Label
                        as="a"
                        onClick={handleClickShowMore}
                      >
                        もっとみる
                      </Label>
                    </Grid.Row>
                  )
              )
          }
        </Grid>
      </div>
    </div>
  );
}

Notifications.propTypes = {
  totalCount: PropTypes.number.isRequired,
  fetchMore: PropTypes.func.isRequired,
  perPage: PropTypes.number.isRequired,
  notifications: PropTypes.arrayOf(
    PropTypes.shape({
      node: PropTypes.shape({
        id: PropTypes.string.isRequired,
        visitor: PropTypes.shape({
          id: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
          imageURL: PropTypes.string.isRequired,
        }).isRequired,
        question: PropTypes.shape({
          id: PropTypes.string.isRequired,
          content: PropTypes.string.isRequired,
        }).isRequired,
        answer: PropTypes.shape({
          content: PropTypes.string.isRequired,
          questionId: PropTypes.string.isRequired,
        }).isRequired,
        reply: PropTypes.shape({
          content: PropTypes.string.isRequired,
          answer: PropTypes.shape({
            questionId: PropTypes.string.isRequired,
          }).isRequired,
        }).isRequired,
        review: PropTypes.shape({
          comment: PropTypes.string.isRequired,
          mid: PropTypes.string.isRequired,
        }).isRequired,
        action: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  ).isRequired,
};
