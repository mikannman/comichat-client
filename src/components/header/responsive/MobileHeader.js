import React, { useState, useEffect } from 'react';
import {
  Container, Grid, Dropdown,
} from 'semantic-ui-react';
import { useMutation } from '@apollo/client';
import styled from 'styled-components';
import queryString from 'query-string';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { setState } from '../../../redux/features/sessionModal/sessionModalSlice';
import UserArea from '../UserArea';
import { ADD_POPULAR_KEYWORD } from '../../../graphql/searchQueries';
import HeaderSearchInput from '../HeaderSearchInput';
import { useAuth } from '../../../hooks/useAuth';

const StyledHeader = styled.header`
  background-color: black;
  min-height: 60px;
  width: 100vw;
`;

const Logo = styled.div`
  font-family: Cornerstone;
  font-size: 30px;
  color: #89da59;
  cursor: pointer;
  margin-top: 25px;
`;

export default function MobileHeader() {
  const [searchInput, setSearchInput] = useState('');
  const [dropDownValue, setDropDownValue] = useState('作品名');
  const [visibleRightSide, setVisibleRightSide] = useState(true);
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.value);
  const history = useHistory();
  const location = useLocation();

  const [addPopularKeyword] = useMutation(ADD_POPULAR_KEYWORD);
  const { loading, guestSignIn } = useAuth({ type: 'guest' });

  useEffect(() => {
    const qs = queryString.parse(location.search);
    if (qs.continueUrl) {
      const redirectTo = qs.continueUrl.split(process.env.REACT_APP_CLIENT_URL)[1];
      if (redirectTo === '/settingsPassword') {
        setVisibleRightSide(false);
      } else {
        setVisibleRightSide(true);
      }
    } else {
      setVisibleRightSide(true);
    }
  }, [location]);

  const handleClickSignUp = () => {
    dispatch(setState({ open: true, mode: 'アカウントを作成' }));
  };

  const handleClickSignIn = () => {
    dispatch(setState({ open: true, mode: 'ログイン' }));
  };

  const handleClickGuestSignIn = () => {
    guestSignIn();
  };

  const handleSubmitSearch = (e) => {
    e.preventDefault();
    if (dropDownValue === '作品名') {
      history.push(`/search?title=${encodeURIComponent(searchInput)}`);
    } else if (dropDownValue === '作者名') {
      history.push(`/search?creator=${encodeURIComponent(searchInput)}`);
    }
    addPopularKeyword({
      variables: {
        word: searchInput,
      },
    });
  };

  return (
    <StyledHeader>
      <Container>
        <Grid>
          <Grid.Row
            columns={2}
            style={{
              paddingBottom: '0',
            }}
          >
            <Grid.Column
              textAlign="left"
              width={6}
            >
              <Link to="/">
                <Logo>
                  comiQ
                </Logo>
              </Link>
            </Grid.Column>
            <Grid.Column
              style={{
                marginTop: '12px',
              }}
              width={10}
              textAlign="right"
            >
              {
                visibleRightSide
                  && (
                    currentUser
                      ? <UserArea />
                      : (
                        <Dropdown
                          icon="bars"
                          button
                          className="icon"
                          pointing="top right"
                        >
                          <Dropdown.Menu>
                            <Dropdown.Item
                              onClick={handleClickSignUp}
                            >
                              新規登録
                            </Dropdown.Item>
                            <Dropdown.Item
                              onClick={handleClickSignIn}
                            >
                              ログイン
                            </Dropdown.Item>
                            <Dropdown.Item
                              onClick={handleClickGuestSignIn}
                              loading={loading}
                            >
                              ゲストログイン
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      )
                  )
              }
            </Grid.Column>
          </Grid.Row>
          <Grid.Row
            columns={1}
            style={{
              marginBottom: '10px',
            }}
          >
            <Grid.Column
              width={16}
              textAlign="center"
            >
              <form
                onSubmit={handleSubmitSearch}
              >
                <HeaderSearchInput
                  value={searchInput}
                  setValue={setSearchInput}
                  handleSubmitSearch={handleSubmitSearch}
                  dropDownValue={dropDownValue}
                  setDropDownValue={setDropDownValue}
                  size="mini"
                />
              </form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </StyledHeader>
  );
}
