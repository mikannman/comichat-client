import React, { useState } from 'react';
import { Popup, Icon } from 'semantic-ui-react';
import { useQuery, useMutation, gql } from '@apollo/client';
import { GET_NOTIFICATIONS, CHECKED_NOTIFICATIONS } from '../../graphql/notificationQueries';
import Notifications from './Notifications';

const perPage = 5;

const query = gql`
  query GetNotifications($DELETE: Boolean) {
    getNotifications(DELETE: $DELETE) {
      edges {
        node {
          id
        }
      }
      totalCount
    }
  }
`;

export default function Bell() {
  const [visibleNotifications, setVisibleNotifications] = useState(false);

  const {
    loading, data, fetchMore, refetch,
  } = useQuery(GET_NOTIFICATIONS, {
    variables: {
      first: perPage,
      after: btoa(String(0)),
    },
  });

  const [checkedNotifications] = useMutation(CHECKED_NOTIFICATIONS, {
    update: (cache, { data: { checkedNotifications: cn } }) => {
      const d = cache.readQuery({
        query,
      });

      cache.writeQuery({
        query,
        variables: {
          DELETE: true,
        },
        data: {
          getNotifications: {
            edges: d.getNotifications.edges.filter((edge) => (
              !cn.checkedIds.includes(edge.node.id)
            )),
            totalCount: d.getNotifications.totalCount - cn.checkedIds.length,
          },
        },
      });
    },
    onCompleted: () => refetch(),
  });

  const handleClickBell = () => setVisibleNotifications(true);

  const handleClosePopup = () => {
    setVisibleNotifications(false);
    checkedNotifications({
      variables: {
        ids: data.getNotifications.edges.map((edge) => edge.node.id),
      },
    });
  };

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <>
      {
        data && data.getNotifications.totalCount
          ? (
            <Icon
              color="yellow"
              name="bell"
              style={{ cursor: 'pointer' }}
              onClick={handleClickBell}
            />
          )
          : (
            <Icon
              color="grey"
              name="bell slash"
            />
          )
      }
      <Popup
        open={visibleNotifications}
        onClose={handleClosePopup}
        position="bottom center"
        trigger={
          <span
            style={{
              marginLeft: '-10px',
            }}
          />
        }
      >
        {
          visibleNotifications
            && (
              <Notifications
                notifications={data.getNotifications.edges}
                perPage={perPage}
                totalCount={data.getNotifications.totalCount}
                fetchMore={fetchMore}
              />
            )
        }
      </Popup>
    </>
  );
}
