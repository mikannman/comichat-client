import React from 'react';
import { Button, Dropdown } from 'semantic-ui-react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import {
  ParentWrapper, ChildWrapper, SpanSpacer, CircleImage,
} from '../../style/globalStyle';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import { resetUser, setIsLogin } from '../../redux/features/user/userSlice';
import { setToken } from '../../redux/features/authToken/authTokenSlice';
import firebase from '../../firebase/config';
import Bell from './Bell';

export default function UserArea() {
  const dispatch = useDispatch();
  const history = useHistory();
  const currentUser = useSelector((state) => state.user.value);

  const handleClickLogout = () => {
    firebase.auth().signOut()
      .then(() => {
        localStorage.removeItem('user');
        dispatch(resetUser());
        dispatch(setIsLogin(false));
        dispatch(setToken(''));
        history.push('/');
      });
  };

  return (
    <ParentWrapper
      justifyContent="flex-end"
    >
      <ChildWrapper alignSelf="center">
        <CircleImage
          src={currentUser.imageURL || DefaultAccontImage}
          alt="プロフィール画像"
          height="40"
          width="40"
        />
        <Bell />
      </ChildWrapper>
      <ChildWrapper alignSelf="center">
        <SpanSpacer color="white" marginLeft="10px" fontSize="15px">
          {currentUser.name}
        </SpanSpacer>
      </ChildWrapper>
      <ChildWrapper alignSelf="center">
        <SpanSpacer marginLeft="10px">
          <Button.Group color="grey">
            <Dropdown floating className="button icon">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link to="/user/mypage">
                    マイページ
                  </Link>
                </Dropdown.Item>
                {
                  !currentUser.guest
                    && (
                      <Dropdown.Item>
                        <Link to="/user/setting">
                          アカウント設定
                        </Link>
                      </Dropdown.Item>
                    )
                }
                <Dropdown.Divider />
                <Dropdown.Item
                  onClick={handleClickLogout}
                >
                  ログアウト
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Button.Group>
        </SpanSpacer>
      </ChildWrapper>
    </ParentWrapper>
  );
}
