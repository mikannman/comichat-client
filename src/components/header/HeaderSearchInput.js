import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button, Dropdown, Input, Menu, Icon,
  Form,
} from 'semantic-ui-react';
import { useLazyQuery } from '@apollo/client';
import { SEARCH_CREATORS, SEARCH_MANGA_TITLES } from '../../graphql/searchQueries';

const options = [
  { key: '作品名', value: '作品名', text: '作品名で検索' },
  { key: '作者名', value: '作者名', text: '作者名で検索' },
];

export default function HeaderSearchInput({
  value, setValue, dropDownValue, setDropDownValue,
  handleSubmitSearch, size, handleSearch,
}) {
  const [suggestions, setSuggestions] = useState([]);
  const [lastRequest, setLastRequest] = useState(null);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const [getSuggestMangas] = useLazyQuery(SEARCH_MANGA_TITLES, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setLoading(false);
      if (data.searchMangaTitles.length) {
        setSuggestions(data.searchMangaTitles.map((manga) => manga.title));
      } else {
        setSuggestions(['検索結果がありません']);
      }
    },
  });

  const [getSuggestCreators] = useLazyQuery(SEARCH_CREATORS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setLoading(false);
      if (data.searchCreators.length) {
        setSuggestions(data.searchCreators.map((manga) => manga.creator));
      } else {
        setSuggestions(['検索結果がありません']);
      }
    },
  });

  const suggestionsFetchRequested = (word) => {
    if (word) {
      if (lastRequest !== null) {
        clearTimeout(lastRequest);
      }

      setLoading(true);
      setLastRequest(
        setTimeout(() => {
          if (dropDownValue === '作品名') {
            getSuggestMangas({
              variables: {
                word,
              },
            });
          } else if (dropDownValue === '作者名') {
            getSuggestCreators({
              variables: {
                word,
              },
            });
          }
        }, 300),
      );
    } else {
      clearTimeout(lastRequest);
      setSuggestions([]);
      setLoading(false);
    }
  };

  const handleSuggestItemClick = (_, { name }) => {
    setValue(name);
    handleSearch(name);
  };

  const handleDropDownChange = (_, { value: dValue }) => {
    setDropDownValue(dValue);
  };

  const handleFocus = (e) => {
    setOpen(true);
    e.target.select();
  };

  const handleBlur = () => {
    setTimeout(() => {
      setOpen(false);
    }, 200);
  };

  const handleChange = (_, data) => {
    setValue(data.value);
    suggestionsFetchRequested(data.value);
  };

  return (
    <Form
      onSubmit={handleSubmitSearch}
    >
      <Form.Field>
        <Input
          label={
            <Dropdown
              selection
              options={options}
              value={dropDownValue}
              onChange={handleDropDownChange}
              compact
            />
          }
          action={
            <Button
              onClick={handleSubmitSearch}
              size={size}
              type="submit"
            >
              <Icon
                name={loading ? 'circle notch' : 'search'}
                loading={loading}
              />
            </Button>
          }
          value={value}
          placeholder="マンガを検索しよう!"
          onFocus={handleFocus}
          onBlur={handleBlur}
          onChange={handleChange}
          size={size}
        />
      </Form.Field>
      {
        (open && Boolean(suggestions.length))
          && (
            <Menu
              vertical
              style={{
                position: 'absolute',
                left: '35%',
                zIndex: '999',
              }}
            >
              {
                suggestions.map((suggest) => (
                  <Menu.Item
                    key={suggest}
                    name={suggest}
                    onClick={handleSuggestItemClick}
                  >
                    {suggest}
                  </Menu.Item>
                ))
              }
            </Menu>
          )
      }
    </Form>
  );
}

HeaderSearchInput.propTypes = {
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  dropDownValue: PropTypes.string.isRequired,
  setDropDownValue: PropTypes.func.isRequired,
  handleSubmitSearch: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
  size: PropTypes.string,
};

HeaderSearchInput.defaultProps = {
  size: '',
};
