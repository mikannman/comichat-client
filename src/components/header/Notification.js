import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import NotificationContent from './NotificationContent';

export default function Notification({ notification }) {
  return (
    <Grid style={{ margin: '10px 0 0 0' }}>
      <Grid.Row style={{ padding: '0' }}>
        <Link to={`/user/${notification.visitor.id}`}>
          <Image
            src={notification.visitor.imageURL || DefaultAccontImage}
            alt="プロフィール画像"
            circular
            size="mini"
          />
        </Link>
        <Link
          to={`/user/${notification.visitor.id}`}
          style={{
            margin: '0 0 0 5px',
            lineHeight: '35px',
          }}
        >
          {notification.visitor.name}
          さんが
        </Link>
      </Grid.Row>
      <Grid.Row columns={1} style={{ padding: '0' }}>
        <Grid.Column width={16}>
          <NotificationContent notification={notification} />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

Notification.propTypes = {
  notification: PropTypes.shape({
    id: PropTypes.string.isRequired,
    visitor: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
    }).isRequired,
    question: PropTypes.shape({
      id: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
    answer: PropTypes.shape({
      content: PropTypes.string.isRequired,
      questionId: PropTypes.string.isRequired,
    }).isRequired,
    reply: PropTypes.shape({
      content: PropTypes.string.isRequired,
      answer: PropTypes.shape({
        questionId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    review: PropTypes.shape({
      comment: PropTypes.string.isRequired,
      mid: PropTypes.string.isRequired,
    }).isRequired,
    action: PropTypes.string.isRequired,
  }).isRequired,
};
