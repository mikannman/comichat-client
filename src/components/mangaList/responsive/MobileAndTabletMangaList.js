import React from 'react';
import { Container, Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import SearchMangaListArea from '../../shared/SearchMangaListArea';

export default function MangaList({ word, condition }) {
  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <SearchMangaListArea word={word} condition={condition} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}

MangaList.propTypes = {
  word: PropTypes.string.isRequired,
  condition: PropTypes.string.isRequired,
};
