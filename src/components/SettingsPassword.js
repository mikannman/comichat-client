import React, { useState } from 'react';
import { Grid, Segment, Button } from 'semantic-ui-react';
import { useMutation } from '@apollo/client';
import styled from 'styled-components';
import queryString from 'query-string';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { StyledInput, Warning, ClickableText } from '../style/globalStyle';
import firebase from '../firebase/config';
import { SIGN_UP_OR_IN } from '../graphql/authQueries';
import { setToken } from '../redux/features/authToken/authTokenSlice';
import { setUser } from '../redux/features/user/userSlice';

const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  background-color: #ebebeb;
  height: 60px;
  position: relative;
`;

const HeaderText = styled.h2`
  padding-top: 16px;
`;

const InputWrapper = styled.div`
  width: 100%;
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function SettingsPassword() {
  const dispatch = useDispatch();
  const passwordValidation = /^\w{6,20}$/;
  const history = useHistory();
  const location = useLocation();
  const qs = queryString.parse(location.search);
  const actionCode = qs.oobCode;
  const [loading, setLoading] = useState(false);
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [error, setError] = useState({
    password: [],
    passwordConfirmation: [],
    communication: [],
  });

  const [signIn] = useMutation(SIGN_UP_OR_IN, {
    onCompleted: (data) => {
      setLoading(false);
      dispatch(setUser(data.signUpOrIn.user));
      history.push('/');
    },
    onError: () => {
      setLoading(false);
      setError({
        password: [],
        passwordConfirmation: [],
        communication: ['予期せぬエラーが発生しました。'],
      });
    },
  });

  const passwordChange = () => {
    firebase.auth().verifyPasswordResetCode(actionCode)
      .then((email) => {
        firebase.auth().confirmPasswordReset(actionCode, password)
          .then(() => {
            firebase.auth().signInWithEmailAndPassword(email, password)
              .then(async (result) => {
                await result.user.getIdToken(/* forceRefresh */ true)
                  .then((idToken) => {
                    dispatch(setToken(idToken));
                  });
                signIn({
                  variables: {
                    email,
                    provider: result.user.providerData[0].providerId,
                  },
                });
              });
          });
      })
      .catch((ve) => {
        if (ve.code === 'auth/expired-action-code') {
          setLoading(false);
          setError({
            password: [],
            passwordConfirmation: [],
            communication: ['有効期限が切れました。'],
          });
        } else if (ve.code === 'auth/invalid-action-code') {
          setLoading(false);
          setError({
            password: [],
            passwordConfirmation: [],
            communication: ['このurlは一度使われているか、正しい形式ではありません。'],
          });
        } else if (ve.code === 'auth/user-disabled' || ve.code === 'auth/user-not-found') {
          setLoading(false);
          setError({
            password: [],
            passwordConfirmation: [],
            communication: ['ユーザーが無効になっているか、削除されています。'],
          });
        }
      });
  };

  const handleChangePassword = (e) => setPassword(e.target.value);

  const handleChangePasswordConfirmation = (e) => setPasswordConfirmation(e.target.value);

  const handleClickTopLink = () => history.push('/');

  const handleSubmit = (e) => {
    e.preventDefault();
    const passwordError = [];
    const passwordConfirmationError = [];
    setLoading(true);

    if (!passwordValidation.test(password)) {
      passwordError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
    }

    if (password !== passwordConfirmation) {
      passwordError.push('パスワードと確認用パスワードが一致しません');
    }

    if (!passwordValidation.test(passwordConfirmation)) {
      passwordConfirmationError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
    }

    if (
      !passwordError.length
      && !passwordConfirmationError.length
    ) {
      passwordChange();
    } else {
      setLoading(false);
      setError({
        password: passwordError,
        passwordConfirmation: passwordConfirmationError,
        communication: [],
      });
    }
  };

  return (
    <Grid centered>
      <Segment
        style={{
          width: '55%',
          padding: '0 0 10px 0',
          marginTop: '120px',
        }}
      >
        <StyledHeader>
          <HeaderText>
            パスワードの再設定
          </HeaderText>
        </StyledHeader>
        {error.communication.map((errorMessage) => (
          <Warning
            key={errorMessage}
          >
            {errorMessage}
            <ClickableText onClick={handleClickTopLink}>トップ</ClickableText>
            に戻り、やり直してください
          </Warning>
        ))}
        <form
          noValidate
          onSubmit={handleSubmit}
        >
          <Grid>
            <Grid.Row
              style={{ marginTop: '40px' }}
              centered
              columns={1}
            >
              <Grid.Column width={10}>
                <Grid>
                  <Grid.Row style={{ padding: '0' }}>
                    <h4>
                      パスワード
                    </h4>
                  </Grid.Row>
                  {error.password.map((errorMessage) => (
                    <Warning
                      key={errorMessage}
                    >
                      {errorMessage}
                    </Warning>
                  ))}
                  <Grid.Row style={{ padding: '0' }}>
                    <InputWrapper>
                      <StyledInput
                        height="30px"
                        type="password"
                        value={password}
                        onChange={handleChangePassword}
                      />
                    </InputWrapper>
                  </Grid.Row>
                </Grid>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row
              style={{ marginTop: '20px' }}
              centered
              columns={1}
            >
              <Grid.Column width={10}>
                <Grid>
                  <Grid.Row style={{ padding: '0' }}>
                    <h4>
                      パスワードの確認
                    </h4>
                  </Grid.Row>
                  {error.passwordConfirmation.map((errorMessage) => (
                    <Warning
                      key={errorMessage}
                    >
                      {errorMessage}
                    </Warning>
                  ))}
                  <Grid.Row style={{ padding: '0' }}>
                    <InputWrapper>
                      <StyledInput
                        height="30px"
                        type="password"
                        value={passwordConfirmation}
                        onChange={handleChangePasswordConfirmation}
                      />
                    </InputWrapper>
                  </Grid.Row>
                </Grid>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row centered>
              <Button
                color="teal"
                type="submit"
                disabled={!password || !passwordConfirmation}
                loading={loading}
              >
                送信
              </Button>
            </Grid.Row>
          </Grid>
        </form>
      </Segment>
    </Grid>
  );
}
