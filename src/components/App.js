import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { Route, Switch, Redirect } from 'react-router-dom';
import queryString from 'query-string';
import { useMediaQuery } from 'react-responsive';
import { useSelector } from 'react-redux';
import {
  NotMobile, Mobile, Tablet, Desktop,
} from '../hooks/useResponsive';
import Header from './header/Header';
import TabletHeader from './header/responsive/TabletHeader';
import MobileHeader from './header/responsive/MobileHeader';
import Nav from './Nav';
import About from './about/About';
import Flash from './Flash';
import emitter from '../utils/emitter';
import Top from './top/Top';
import TabletTop from './top/responsive/TabletTop';
import MobileTop from './top/responsive/MobileTop';
import User from './user/User';
import TabletUser from './user/responsive/TabletUser';
import MobileUser from './user/responsive/MobileUser';
import Search from './search/Search';
import MobileAndTabletSearch from './search/responsive/MobileAndTabletSearch';
import MangaList from './mangaList/MangaList';
import MobileAndTabletMangaList from './mangaList/responsive/MobileAndTabletMangaList';
import Manga from './manga/Manga';
import TabletManga from './manga/responsive/TabletManga';
import MobileManga from './manga/responsive/MobileManga';
import Ranking from './ranking/Ranking';
import MobileAndTabletRanking from './ranking/responsive/MobileAndTabletRanking';
import Questions from './questions/Questions';
import MobileQuestions from './questions/responsive/MobileQuestions';
import NotFound from './NotFound';
import DisplayConfirm from './shared/DisplayConfirm';
import SessionModal from './sessionModal/SessionModal';
import SettingsPassword from './SettingsPassword';
import ReauthModal from './shared/ReauthModal';
import MobileUserDropdown from './user/responsive/MobileUserDropdown';
import Footer from './footer/Footer';
import MobileFooter from './footer/responsive/MobileFooter';

const GlobalStyle = createGlobalStyle`
  a {
    color: black;
    &:hover {
      color: black;
      text-decoration: none;
    }
  }
  body {
    box-sizing: border-box;
    background-color: #fafafa;
  }
`;

const Main = styled.div`
  flex: 1;
`;

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  min-height: 100vh;
`;

export default function App() {
  const isDesktop = useMediaQuery({ minWidth: 992 });
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 991 });
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const isMobileAndTablet = useMediaQuery({ maxWidth: 991 });
  const mobileUserDropdownVisible = useSelector((state) => state.mobileUserDropdown.visible);
  const mobileUserDropdownProps = useSelector((state) => (
    {
      menus: state.mobileUserDropdown.menus,
      handleClickMenu: state.mobileUserDropdown.handleClickMenu,
      activeItem: state.mobileUserDropdown.activeItem,
    }
  ));

  window.flash = (messageArg, typeArg = 'success') => emitter.emit('flash', ({ messageArg, typeArg }));

  return (
    <Wrapper data-testid="root">
      <GlobalStyle />
      <DisplayConfirm />
      <ReauthModal />
      <SessionModal />
      <Flash />
      <Mobile>
        <div
          style={{
            position: 'fixed',
            zIndex: '50',
          }}
        >
          <MobileHeader />
          <Nav />
        </div>
        <div
          style={{
            height: '133px',
          }}
        />
      </Mobile>
      <Tablet>
        <TabletHeader />
        <Nav />
      </Tablet>
      <Desktop>
        <Header />
        <Nav />
      </Desktop>
      <h3
        style={{
          display: 'flex',
          justifyContent: 'center',
          color: 'red',
        }}
      >
        注意
        <br />
        このサイトは開発中のため、予告なく全てのデータがなくなる可能性があります
      </h3>
      <Main>
        <Switch>
          <Route
            exact
            key="top"
            path="/"
            render={(props) => {
              const qs = queryString.parse(props.location.search);
              if (qs.continueUrl) {
                const redirectTo = qs.continueUrl.split(process.env.REACT_APP_CLIENT_URL)[1];
                if (redirectTo === '/') {
                  if (isDesktop) {
                    return <Top />;
                  } if (isTablet) {
                    return <TabletTop />;
                  } if (isMobile) {
                    return <MobileTop />;
                  }
                } if (redirectTo === '/settingsPassword') {
                  return <SettingsPassword />;
                }
              }
              if (isDesktop) {
                return <Top />;
              } if (isTablet) {
                return <TabletTop />;
              } if (isMobile) {
                return <MobileTop />;
              }
            }}
          />
          <Route key="questions" path="/questions">
            <NotMobile>
              <Questions />
            </NotMobile>
            <Mobile>
              <MobileQuestions />
            </Mobile>
          </Route>
          <Route key="about" path="/about">
            <About />
          </Route>
          <Route key="user" path="/user">
            <Desktop>
              <User />
            </Desktop>
            <Tablet>
              <TabletUser />
            </Tablet>
            <Mobile>
              <MobileUser />
            </Mobile>
          </Route>
          <Route
            key="search"
            path="/search"
            render={(props) => {
              const qs = queryString.parse(props.location.search);
              if (qs.title) {
                if (isMobileAndTablet) {
                  return (
                    <MobileAndTabletSearch
                      searchWord={decodeURIComponent(qs.title)}
                      searchCondition="作品名検索"
                    />
                  );
                }
                return <Search searchWord={decodeURIComponent(qs.title)} searchCondition="作品名検索" />;
              } if (qs.creator) {
                if (isMobileAndTablet) {
                  return (
                    <MobileAndTabletSearch
                      searchWord={decodeURIComponent(qs.creator)}
                      searchCondition="作者名検索"
                    />
                  );
                }
                return <Search searchWord={decodeURIComponent(qs.creator)} searchCondition="作者名検索" />;
              }
              return <Redirect to="/notFound" />;
            }}
          />
          <Route
            key="mangaList"
            path="/mangaList"
            render={(props) => {
              const qs = queryString.parse(props.location.search);
              if (qs.creator) {
                if (isMobileAndTablet) {
                  return (
                    <MobileAndTabletMangaList
                      word={decodeURIComponent(qs.creator)}
                      condition="作者名一覧"
                    />
                  );
                }
                return (
                  <MangaList word={decodeURIComponent(qs.creator)} condition="作者名一覧" />
                );
              } if (qs.genre) {
                if (isMobileAndTablet) {
                  return (
                    <MobileAndTabletMangaList
                      word={decodeURIComponent(qs.genre)}
                      condition="ジャンル一覧"
                    />
                  );
                }
                return <MangaList word={decodeURIComponent(qs.genre)} condition="ジャンル一覧" />;
              } if (qs.publisher) {
                if (isMobileAndTablet) {
                  return (
                    <MobileAndTabletMangaList
                      word={decodeURIComponent(qs.publisher)}
                      condition="出版社一覧"
                    />
                  );
                }
                return <MangaList word={decodeURIComponent(qs.publisher)} condition="出版社一覧" />;
              } if (qs.label) {
                if (isMobileAndTablet) {
                  return (
                    <MobileAndTabletMangaList
                      word={decodeURIComponent(qs.label)}
                      condition="レーベル一覧"
                    />
                  );
                }
                return <MangaList word={decodeURIComponent(qs.label)} condition="レーベル一覧" />;
              }
              return <Redirect to="/notFound" />;
            }}
          />
          <Route key="ranking" path="/ranking/:type">
            {isMobileAndTablet ? <MobileAndTabletRanking /> : <Ranking />}
          </Route>
          <Route key="manga" path="/manga/:mid">
            <Desktop>
              <Manga />
            </Desktop>
            <Tablet>
              <TabletManga />
            </Tablet>
            <Mobile>
              <MobileManga />
            </Mobile>
          </Route>
          <Route key="notFound" path="/notFound">
            <NotFound />
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
        <div
          style={{
            height: '50px',
          }}
        />
        {
          mobileUserDropdownVisible
            && (
              <MobileUserDropdown
                {...mobileUserDropdownProps}
              />
            )
        }
        <div
          style={{
            height: '10px',
          }}
        />
      </Main>
      <div
        style={{
          textAlign: 'right',
          margin: '10px',
          fontSize: isMobile ? '10px' : '',
        }}
      >
        株式会社アウスタからインフラ等の技術的支援を得て、制作中
      </div>
      <div
        style={{
          textAlign: 'right',
          margin: '10px',
          fontSize: isMobile ? '10px' : '',
        }}
      >
        文化庁
        <a
          href="https://mediaarts-db.bunka.go.jp/"
          target="_blank"
          rel="noopener noreferrer"
        >
          「メディア芸術データベース（ベータ版）」
        </a>
        を加工して作成
      </div>
      <Mobile>
        <div className="footer">
          <MobileFooter />
        </div>
      </Mobile>
      <NotMobile>
        <div className="footer">
          <Footer />
        </div>
      </NotMobile>
    </Wrapper>
  );
}
