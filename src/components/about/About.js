import React from 'react';
import { Container, Grid } from 'semantic-ui-react';

export default function About() {
  return (
    <Container
      style={{
        marginTop: '40px',
      }}
    >
      <Grid
        style={{
          margin: '0',
        }}
      >
        <Grid.Row textAlign="center">
          誰にでも必ずある、自分の人生に大きな影響を与える至極の一冊...
          <br />
          自分だけの中に閉じ込めておくにはもったいない！！  ...そう思いませんか？
        </Grid.Row>
        <Grid.Row>
          comiQ(コミッキュー)とは、そんな一冊を人々に伝え、そんな一冊を人々から知ることができる
        </Grid.Row>
        <Grid.Row>
          ユーザー間の繋がりをより一層密接にした新時代のまんがレビューサイトです!!
        </Grid.Row>
        <Grid.Row>
          例えば、自分の好きなまんがに同じように高評価なレビューをしているユーザーに
          おすすめのまんがを直接聞くことができたり、
          ユーザー全員にこんなジャンルの面白いまんががあったら教えてくださいなど、
          気軽にユーザー間での情報共有ができます。
        </Grid.Row>
        <Grid.Row>
          一人で検索していたのではなかなか見つからないような隠れた名作や、
          古いまんがにきっと出会えます！！
        </Grid.Row>
        <Grid.Row>
          いま流行りのまんがから、一世代、二世代前のまんが。
          ジャンルの違うまんが。
          読みたいとは思っているけれどもなかなか手が出せないまんが。
          読まないでおくには実にもったいないです！
        </Grid.Row>
        <Grid.Row>
          昔読んだまんが。
          最近はじまったばかりのまんが。
          内容は過激だけどぜひ読んで欲しいまんが。
          自分の中に閉じ込めておくだけで本当に満足ですか？
        </Grid.Row>
        <Grid.Row>
          読みたいユーザーと読ませたいユーザー...
          ユーザー間の知識を共有し、同じ価値観のもの同士、
          comiQを使って、一つの輪の中で、みんなで、さあ、楽しましょう!!!
        </Grid.Row>
      </Grid>
    </Container>
  );
}
