import React from 'react';
import { Container, Grid, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const footerFontColor = '#d1d1d1';

const StyledFooter = styled.footer`
  width: 100vw;
  background-color: black;
  height: 100px;
`;

const Logo = styled.div`
  font-family: Cornerstone;
  font-size: 30px;
  cursor: pointer;
  line-height: 60px;
  color: ${footerFontColor};
`;

const navs = [
  { name: 'TOP', path: '/' },
  { name: 'みんなの質問', path: '/questions' },
  { name: 'comicQとは', path: '/about' },
];

export default function Footer() {
  return (
    <StyledFooter>
      <Container
        style={{
          height: '100%',
        }}
      >
        <Grid
          style={{
            height: '100%',
            width: '90%',
            margin: '0 auto',
          }}
        >
          <Grid.Row columns={3}>
            <Grid.Column
              width={3}
              verticalAlign="middle"
              style={{
                color: 'white',
                textAlign: 'left',
              }}
            >
              <Link to="/">
                <Logo>
                  comicQ
                </Logo>
              </Link>
            </Grid.Column>
            <Grid.Column
              width={1}
              style={{
                borderLeft: 'solid white 1px',
                display: 'inline-block',
                height: '100%',
              }}
            />
            <Grid.Column
              width={8}
              verticalAlign="middle"
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-start',
                }}
              >
                {
                  navs.map((nav) => (
                    <Link
                      to={nav.path}
                      key={nav.name}
                      style={{
                        color: footerFontColor,
                        marginLeft: '10px',
                      }}
                    >
                      {nav.name}
                    </Link>
                  ))
                }
              </div>
            </Grid.Column>
            <Grid.Column
              width={4}
              verticalAlign="middle"
              style={{
                color: footerFontColor,
                textAlign: 'right',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                FOLLOW ME
                <Link
                  to="https://twitter.com/yuji_mikann"
                  style={{
                    marginLeft: '15px',
                    color: footerFontColor,
                  }}
                >
                  <Icon
                    name="twitter"
                    size="big"
                    link
                  />
                </Link>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </StyledFooter>
  );
}
