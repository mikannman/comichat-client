import React from 'react';
import { Container, Grid, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const footerFontColor = '#d1d1d1';

const StyledFooter = styled.footer`
  width: 100vw;
  background-color: black;
  height: 100px;
`;

const Logo = styled.div`
  font-family: Cornerstone;
  font-size: 30px;
  cursor: pointer;
  line-height: 60px;
  color: ${footerFontColor};
`;

export default function MobileFooter() {
  return (
    <StyledFooter>
      <Container
        style={{
          height: '100%',
        }}
      >
        <Grid
          style={{
            height: '100%',
            width: '100%',
            margin: '0 auto',
          }}
        >
          <Grid.Row columns={3}>
            <Grid.Column
              width={5}
              verticalAlign="middle"
              style={{
                color: 'white',
                textAlign: 'left',
              }}
            >
              <Link to="/">
                <Logo>
                  comicQ
                </Logo>
              </Link>
            </Grid.Column>
            <Grid.Column
              width={11}
              verticalAlign="middle"
              style={{
                color: footerFontColor,
                textAlign: 'right',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                FOLLOW ME
                <Link
                  to="https://twitter.com/yuji_mikann"
                  style={{
                    marginLeft: '15px',
                    color: footerFontColor,
                  }}
                >
                  <Icon
                    name="twitter"
                    size="big"
                    link
                  />
                </Link>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </StyledFooter>
  );
}
