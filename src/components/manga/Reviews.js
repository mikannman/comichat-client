import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/client';
import { Grid, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { GET_REVIEWS } from '../../graphql/reviewQueries';
import Review from '../shared/Review';
import DisplayPagination from '../shared/DisplayPagination';

const perPage = 5;

export default function Reviews({ mid }) {
  const [totalPages, setTotalPages] = useState(null);
  const {
    loading, data, refetch,
  } = useQuery(GET_REVIEWS, {
    variables: { mid, first: perPage },
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    if (data) {
      setTotalPages(Math.ceil(data.getReviews.totalCount / perPage));
    }
  }, [data]);

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (data.getReviews.totalCount) {
    return (
      <Grid.Row columns={1}>
        <Grid.Column>
          <Grid>
            <Grid.Row>
              <Grid.Column>
                {data.getReviews.edges.map((edge) => (
                  <Review
                    edge={edge}
                    refetch={refetch}
                    key={edge.node.id}
                  />
                ))}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row centered columns={3}>
              <DisplayPagination
                perPage={perPage}
                totalPage={totalPages}
                refetch={refetch}
              />
            </Grid.Row>
          </Grid>
        </Grid.Column>
      </Grid.Row>
    );
  }

  return <div>まだレビューがありません</div>;
}

Reviews.propTypes = {
  mid: PropTypes.string.isRequired,
};
