import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import {
  Container, Grid, Icon, Button, Label,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useParams, Link } from 'react-router-dom';
import { GET_MANGA } from '../../graphql/mangaQueries';
import WriteReview from './WriteReview';
import Reviews from './Reviews';
import RegisterButton from '../shared/RegisterButtons';
import MangaImage from '../shared/MangaImage';
import PopularKeywords from '../shared/PopularKeywords';
import usePromptSignIn from '../../hooks/usePromptSignIn';
import Rating from './Rating';
import DisplayMangaStars from '../shared/DisplayMangaStars';

const Title = styled.h1`
  margin-left: 30px;
  font-size: 40px;
  font-weight: bolder;
  height: 100%;
  padding: 0;
  margin: 0;
`;

const StyledH4 = styled.h4`
  margin: 0;
  line-height: 26px;
`;

export default function Manga() {
  const { mid } = useParams();
  const [visibleWriteReview, setVisibleWriteReview] = useState(false);
  const [itemURL, setItemURL] = useState(null);
  const promptSignIn = usePromptSignIn();

  const { loading, data } = useQuery(GET_MANGA, {
    variables: { mid: decodeURIComponent(mid) },
  });

  const handleClickWriteReviewButton = () => {
    promptSignIn({
      callback: () => setVisibleWriteReview((prev) => !prev),
    });
  };

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={11} style={{ backgroundcolor: 'white' }}>
            <Grid>
              <Grid.Row>
                <Title>
                  <Icon name="book" />
                  {data.getManga.title}
                </Title>
              </Grid.Row>
              <Grid.Row>
                <div
                  style={{
                    margin: '20px 0 20px 20px',
                  }}
                >
                  <Rating
                    rate={data.getManga.currentUserRated ? data.getManga.currentUserRated.rate : 'unrated'}
                    mid={data.getManga.mid}
                  />
                </div>
              </Grid.Row>
              <Grid.Row columns={2}>
                <Grid.Column width={7}>
                  <MangaImage
                    title={data.getManga.title}
                    length={120}
                    setItemURL={setItemURL}
                    mid={data.getManga.mid}
                    imageUrl={data.getManga.imageUrl}
                    affiliateUrl={data.getManga.affiliateUrl}
                  />
                </Grid.Column>
                <Grid.Column width={9}>
                  <Grid>
                    <Grid.Row verticalAlign="middle">
                      <DisplayMangaStars
                        reviewsAverage={data.getManga.reviews.average}
                        size="big"
                      />
                      <div
                        style={{
                          fontSize: '20px',
                          margin: '4px 0 0 15px',
                        }}
                      >
                        {data.getManga.reviews.average}
                      </div>
                    </Grid.Row>
                    <Grid.Row>
                      <StyledH4>作者:</StyledH4>
                      {data.getManga.creators.map((c) => (
                        <Link key={c.creator} to={`/mangaList?creator=${encodeURIComponent(c.creator)}`}>
                          <Label>{c.creator}</Label>
                        </Link>
                      ))}
                      <StyledH4>出版社:</StyledH4>
                      <Link to={`/mangaList?publisher=${encodeURIComponent(data.getManga.publisher)}`}>
                        <Label>{data.getManga.publisher.replace(/\s+.+/, '')}</Label>
                      </Link>
                    </Grid.Row>
                    <Grid.Row>
                      {
                        Boolean(data.getManga.labels.length)
                          && (
                            <>
                              <StyledH4>レーベル:</StyledH4>
                              {data.getManga.labels.map((l) => (
                                <Link key={l.label} to={`/mangaList?label=${encodeURIComponent(l.label)}`}>
                                  <Label>{l.label}</Label>
                                </Link>
                              ))}
                            </>
                          )
                      }
                      {
                        Boolean(data.getManga.genres.length)
                          && (
                            <>
                              <StyledH4>ジャンル:</StyledH4>
                              {data.getManga.genres.map((g) => (
                                <Link key={g.genre} to={`/mangaList?genre=${encodeURIComponent(g.genre)}`}>
                                  <Label key={g.genre}>{g.genre}</Label>
                                </Link>
                              ))}
                            </>
                          )
                      }
                    </Grid.Row>
                    <Grid.Row>
                      {
                        data.getManga.datePublished
                          && (
                            <div>
                              発刊:
                              {data.getManga.datePublished}
                            </div>
                          )
                      }
                    </Grid.Row>
                    <Grid.Row>
                      {data.getManga.synopsis}
                    </Grid.Row>
                    <Grid.Row>
                      <RegisterButton
                        mid={data.getManga.mid}
                        isReadedManga={data.getManga.currentUserReaded.isExist}
                        isWannaReadManga={data.getManga.currentUserWannaRead.isExist}
                      />
                      <Button
                        onClick={handleClickWriteReviewButton}
                        style={{
                          backgroundColor: '#76c639',
                          color: 'white',
                          width: '145px',
                        }}
                      >
                        レビューを書く
                      </Button>
                    </Grid.Row>
                    <Grid.Row>
                      {
                        itemURL
                          && (
                            <Button
                              fluid
                              style={{
                                backgroundColor: '#fbbd08',
                                border: 'solid 1px #333',
                                color: '#333',
                              }}
                            >
                              <a href={itemURL} target="_blank" rel="noopener noreferrer">
                                購入する
                              </a>
                            </Button>
                          )
                      }
                    </Grid.Row>
                  </Grid>
                </Grid.Column>
              </Grid.Row>
              <hr style={{ width: '100%' }} />
              <WriteReview
                visibleWriteReview={visibleWriteReview}
                setVisibleWriteReview={setVisibleWriteReview}
                mid={data.getManga.mid}
              />
              <Reviews
                mid={data.getManga.mid}
              />
            </Grid>
          </Grid.Column>
          <Grid.Column width={5}>
            <PopularKeywords />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
