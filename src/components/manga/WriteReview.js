import React from 'react';
import { useMutation, gql } from '@apollo/client';
import { Transition } from 'react-transition-group';
import { Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { CREATE_REVIEW } from '../../graphql/reviewQueries';
import WriteArea from './WriteArea';

const duration = 300;

const defaultSyle = {
  transition: `all ${duration}ms ease`,
  height: '0px',
  padding: '0',
  overflow: 'hidden',
};

const transitionStyles = {
  entering: {
    height: '0px',
  },
  entered: {
    height: '210px',
  },
  exiting: {
    height: '210px',
  },
  exited: {
    height: '0px',
  },
};

const query = gql`
  query GetReviews($mid: String, $after: String, $CREATE: Boolean) {
    getReviews(mid: $mid, after: $after, CREATE: $CREATE) {
      edges {
        node {
          id
        }
      }
    }
  }
`;

export default function WriteReview({
  mid, visibleWriteReview, setVisibleWriteReview,
}) {
  const [createReview] = useMutation(CREATE_REVIEW, {
    update: (cache, { data: { createReview: cr } }) => {
      const data = cache.readQuery({
        query,
        variables: {
          mid,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          mid,
          CREATE: true,
        },
        data: {
          getReviews: {
            edges: [
              {
                node: cr.review,
                __typename: 'ReviewEdge',
              },
              ...data.getReviews.edges,
            ],
          },
        },
      });
    },
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const handleClickCancel = () => {
    setVisibleWriteReview(false);
  };

  const handleSubmit = async (variables) => {
    createReview({
      variables: { ...variables },
    });
  };

  return (
    <Transition in={visibleWriteReview} timeout={duration}>
      {(state) => (
        <Grid.Row
          centered
          style={{
            ...defaultSyle,
            ...transitionStyles[state],
          }}
          columns={1}
        >
          <Grid.Column width={10}>
            <WriteArea
              star={1}
              comment=""
              netabare={false}
              submitButtonText="投稿"
              variables={{ mid }}
              handleClickCancel={handleClickCancel}
              handleSubmit={handleSubmit}
            />
          </Grid.Column>
        </Grid.Row>
      )}
    </Transition>
  );
}

WriteReview.propTypes = {
  mid: PropTypes.string.isRequired,
  visibleWriteReview: PropTypes.bool.isRequired,
  setVisibleWriteReview: PropTypes.func.isRequired,
};
