import React, { useState } from 'react';
import {
  Grid, Icon, Button, Radio,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { StyledTextarea } from '../../style/globalStyle';

export default function WriteArea({
  star, comment, netabare, submitButtonText, variables,
  handleClickCancel, handleSubmit,
}) {
  const [inputParams, setInputParams] = useState({
    star, comment, netabare,
  });

  const handleChangeRadio = () => {
    setInputParams((prev) => (
      { ...prev, netabare: !prev.netabare }
    ));
  };

  const handleClickStar = (e) => {
    setInputParams((prev) => (
      { ...prev, star: Number(e.target.dataset.star) }
    ));
  };

  const handleChangeTextarea = (e) => {
    setInputParams((prev) => (
      { ...prev, comment: e.target.value }
    ));
  };

  const handleReviewSubmit = async (e) => {
    e.preventDefault();
    handleSubmit({
      ...variables,
      ...inputParams,
    }).then(() => {
      setInputParams({
        star: 1,
        comment: '',
        netabare: false,
      });
    });
  };

  return (
    <Grid>
      <Grid.Row>
        {[0, 1, 2, 3, 4].map((i) => (
          <Icon
            name="star"
            key={i}
            data-star={i + 1}
            color={i < inputParams.star ? 'yellow' : 'grey'}
            style={{ cursor: 'pointer' }}
            onClick={handleClickStar}
          />
        ))}
        <Radio
          toggle
          label="ネタバレあり"
          checked={inputParams.netabare}
          onChange={handleChangeRadio}
        />
      </Grid.Row>
      <Grid.Row
        style={{
          padding: '0',
        }}
      >
        <StyledTextarea
          value={inputParams.comment}
          onChange={handleChangeTextarea}
          padding="10px"
          rows="6"
          cols="50"
          border="solid 1px black"
        />
      </Grid.Row>
      <Grid.Row
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <Button
          onClick={handleClickCancel}
        >
          キャンセル
        </Button>
        <Button
          color="teal"
          onClick={handleReviewSubmit}
        >
          {submitButtonText}
        </Button>
      </Grid.Row>
    </Grid>
  );
}

WriteArea.propTypes = {
  star: PropTypes.number.isRequired,
  comment: PropTypes.string.isRequired,
  netabare: PropTypes.bool.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  variables: PropTypes.shape({
    id: PropTypes.string,
    mid: PropTypes.string,
  }),
  handleSubmit: PropTypes.func.isRequired,
  handleClickCancel: PropTypes.func.isRequired,
};

WriteArea.defaultProps = {
  variables: { id: '', mid: '' },
};
