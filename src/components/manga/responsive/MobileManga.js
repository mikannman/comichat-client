import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import {
  Container, Grid, Icon, Button, Label,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { useParams, Link } from 'react-router-dom';
import { GET_MANGA } from '../../../graphql/mangaQueries';
import MobileWriteReview from './MobileWriteReview';
import Reviews from '../Reviews';
import RegisterButton from '../../shared/RegisterButtons';
import MangaImage from '../../shared/MangaImage';
import usePromptSignIn from '../../../hooks/usePromptSignIn';
import Rating from '../Rating';
import DisplayMangaStars from '../../shared/DisplayMangaStars';

const Title = styled.h1`
  margin-left: 30px;
  font-size: 20px;
  font-weight: bolder;
  height: 100%;
  padding: 0;
  margin: 0;
`;

const StyledH4 = styled.h4`
  margin: 0;
  line-height: 26px;
`;

export default function MobileManga() {
  const { mid } = useParams();
  const [visibleWriteReview, setVisibleWriteReview] = useState(false);
  const [itemURL, setItemURL] = useState(null);
  const promptSignIn = usePromptSignIn();

  const { loading, data } = useQuery(GET_MANGA, {
    variables: { mid: decodeURIComponent(mid) },
  });

  const handleClickWriteReviewButton = () => {
    promptSignIn({
      callback: () => setVisibleWriteReview((prev) => !prev),
    });
  };

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    <Container style={{ marginTop: '50px' }}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16} style={{ backgroundcolor: 'white' }}>
            <Grid>
              <Grid.Row>
                <Title>
                  <Icon name="book" />
                  {data.getManga.title}
                </Title>
              </Grid.Row>
              <Grid.Row>
                <div
                  style={{
                    margin: '0px 0 0px 20px',
                  }}
                >
                  <Rating
                    rate={data.getManga.currentUserRated ? data.getManga.currentUserRated.rate : 'unrated'}
                    mid={data.getManga.mid}
                  />
                </div>
              </Grid.Row>
              <Grid.Row centered>
                <MangaImage
                  title={data.getManga.title}
                  length={120}
                  setItemURL={setItemURL}
                  mid={data.getManga.mid}
                  imageUrl={data.getManga.imageUrl}
                  affiliateUrl={data.getManga.affiliateUrl}
                />
              </Grid.Row>
              <Grid.Row verticalAlign="middle" centered>
                <DisplayMangaStars
                  reviewsAverage={data.getManga.reviews.average}
                  size="big"
                />
                <h4
                  style={{
                    fontSize: '20px',
                    margin: '4px 0 0 15px',
                  }}
                >
                  {data.getManga.reviews.average}
                </h4>
              </Grid.Row>
              <Grid.Row centered>
                <StyledH4>作者:</StyledH4>
                {data.getManga.creators.map((c) => (
                  <Link key={c.creator} to={`/mangaList?creator=${encodeURIComponent(c.creator)}`}>
                    <Label>{c.creator}</Label>
                  </Link>
                ))}
                <StyledH4>出版社:</StyledH4>
                <Link to={`/mangaList?publisher=${encodeURIComponent(data.getManga.publisher)}`}>
                  <Label>{data.getManga.publisher.replace(/\s+.+/, '')}</Label>
                </Link>
              </Grid.Row>
              <Grid.Row centered>
                {
                  Boolean(data.getManga.labels.length)
                    && (
                      <>
                        <StyledH4>レーベル:</StyledH4>
                        {data.getManga.labels.map((l) => (
                          <Link key={l.label} to={`/mangaList?label=${encodeURIComponent(l.label)}`}>
                            <Label>{l.label}</Label>
                          </Link>
                        ))}
                      </>
                    )
                }
              </Grid.Row>
              <Grid.Row centered>
                {
                  Boolean(data.getManga.genres.length)
                    && (
                      <>
                        <StyledH4>ジャンル:</StyledH4>
                        {data.getManga.genres.map((g) => (
                          <Link key={g.genre} to={`/mangaList?genre=${encodeURIComponent(g.genre)}`}>
                            <Label key={g.genre}>{g.genre}</Label>
                          </Link>
                        ))}
                      </>
                    )
                }
              </Grid.Row>
              <Grid.Row>
                {
                  data.getManga.datePublished
                    && (
                      <div>
                        発刊:
                        {data.getManga.datePublished}
                      </div>
                    )
                }
              </Grid.Row>
              <Grid.Row>
                {data.getManga.synopsis}
              </Grid.Row>
              <Grid.Row
                centered
                style={{
                  padding: '5px 0',
                }}
              >
                <RegisterButton
                  mid={data.getManga.mid}
                  isReadedManga={data.getManga.currentUserReaded.isExist}
                  isWannaReadManga={data.getManga.currentUserWannaRead.isExist}
                />
              </Grid.Row>
              <Grid.Row
                style={{
                  padding: '5px 10px',
                }}
              >
                <Button
                  fluid
                  onClick={handleClickWriteReviewButton}
                  style={{
                    backgroundColor: '#76c639',
                    color: 'white',
                  }}
                >
                  レビューを書く
                </Button>
              </Grid.Row>
              <Grid.Row
                style={{
                  padding: '5px 10px',
                }}
              >
                {
                  itemURL
                    && (
                      <Button
                        fluid
                        style={{
                          backgroundColor: '#fbbd08',
                          border: 'solid 1px #333',
                          color: '#333',
                        }}
                      >
                        <a href={itemURL} target="_blank" rel="noopener noreferrer">
                          購入する
                        </a>
                      </Button>
                    )
                }
              </Grid.Row>
              <hr style={{ width: '100%' }} />
              <MobileWriteReview
                visibleWriteReview={visibleWriteReview}
                setVisibleWriteReview={setVisibleWriteReview}
                mid={data.getManga.mid}
              />
              <Reviews
                mid={data.getManga.mid}
              />
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
