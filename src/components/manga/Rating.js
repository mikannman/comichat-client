import React from 'react';
import { useMutation, gql } from '@apollo/client';
import { Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Row } from '../../style/globalStyle';
import usePromptSignIn from '../../hooks/usePromptSignIn';
import { RATING_MANGA } from '../../graphql/mangaQueries';

const mangaRatingFragment = gql`
  fragment mangaRatingRef on Manga {
    mid
    currentUserRated {
      rate
    }
  }
`;

export default function Rating({ rate, mid }) {
  const promptSignIn = usePromptSignIn();
  const ratings = [
    {
      text: '面白い！！',
      iconName: rate === 'good' ? 'smile' : 'smile outline',
      color: rate === 'good' ? 'yellow' : 'grey',
      rating: 'good',
    },
    {
      text: '普通',
      iconName: rate === 'normal' ? 'meh' : 'meh outline',
      color: rate === 'normal' ? 'green' : 'grey',
      rating: 'normal',
    },
    {
      text: '面白くない',
      iconName: rate === 'bad' ? 'frown' : 'frown outline',
      color: rate === 'bad' ? 'blue' : 'grey',
      rating: 'bad',
    },
  ];

  const [ratingManga] = useMutation(RATING_MANGA, {
    update: (cache, { data: { ratingManga: rm } }) => {
      cache.writeFragment({
        id: `Manga:{"mid":"${mid}"}`,
        fragment: mangaRatingFragment,
        data: {
          currentUserRated: {
            rate: rm.rating.rate,
          },
        },
      });
    },
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const handleClickRating = (e) => {
    promptSignIn({
      callback: () => {
        ratingManga({
          variables: {
            mid,
            rate: e.target.dataset.rating,
          },
        });
      },
    });
  };

  return (
    <Row>
      {
        ratings.map((rating) => (
          <div key={rating.text}>
            {rating.text}
            <Icon
              name={rating.iconName}
              color={rating.color}
              onClick={handleClickRating}
              data-rating={rating.rating}
            />
          </div>
        ))
      }
    </Row>
  );
}

Rating.propTypes = {
  rate: PropTypes.string.isRequired,
  mid: PropTypes.string.isRequired,
};
