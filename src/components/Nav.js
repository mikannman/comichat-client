import React, { useState } from 'react';
import { Menu, Container } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

const Back = styled.div`
  background-color: grey;
`;

const navs = [
  { name: 'TOP', path: '/' },
  { name: 'みんなの質問', path: '/questions' },
  { name: 'comicQとは', path: '/about' },
];

export default function Nav() {
  const [activeItem, setActiveItem] = useState('TOP');
  const history = useHistory();

  const handleClick = (_, { name }) => {
    setActiveItem(name);
    history.push(navs.filter((nav) => nav.name === name)[0].path);
  };

  return (
    <Back>
      <Menu inverted color="grey" fluid size="mini">
        <Container>
          {navs.map((nav) => (
            <Menu.Item
              name={nav.name}
              active={activeItem === nav.name}
              onClick={handleClick}
              key={nav.name}
              style={{
                maxWidth: '8rem',
                width: '30%',
                textAlign: 'center',
                display: 'block',
              }}
            />
          ))}
        </Container>
      </Menu>
    </Back>
  );
}
