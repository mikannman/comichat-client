import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Grid, Card, Image, Icon,
} from 'semantic-ui-react';
import DefaultAccountImage from '../image/defaultAccountImage.png';

export default function UsersCard({ headerText, users, loading }) {
  return (
    <Card>
      <Card.Content>
        <Card.Header>{headerText}</Card.Header>
      </Card.Content>
      <Card.Content>
        <Grid>
          {
            loading
              ? <Icon name="spinner" loading />
              : (
                users.map((user, idx) => (
                  <Grid.Row columns={3} key={user.id}>
                    <Grid.Column
                      width={4}
                      verticalAlign="middle"
                      style={{
                        padding: '0 5px 0 15px',
                        textAlign: 'right',
                      }}
                    >
                      <h3>
                        {idx + 1}
                        位
                      </h3>
                    </Grid.Column>
                    <Grid.Column
                      width={4}
                      verticalAlign="middle"
                    >
                      <Link
                        to={`/user/${user.id}`}
                      >
                        <Image
                          circular
                          src={user.imageURL || DefaultAccountImage}
                        />
                      </Link>
                    </Grid.Column>
                    <Grid.Column
                      width={8}
                      verticalAlign="middle"
                      style={{
                        padding: '0',
                      }}
                    >
                      <Link to={`/user/${user.id}`}>
                        <h4 style={{ margin: '0' }}>{user.name}</h4>
                        {
                          !user.guest
                            && (
                              <small
                                style={{
                                  wordBreak: 'break-all',
                                }}
                              >
                                {user.userId}
                              </small>
                            )
                        }
                      </Link>
                    </Grid.Column>
                  </Grid.Row>
                ))
              )
          }
        </Grid>
      </Card.Content>
    </Card>
  );
}

UsersCard.propTypes = {
  loading: PropTypes.bool.isRequired,
  headerText: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      imageURL: PropTypes.string,
      guest: PropTypes.bool.isRequired,
    }).isRequired,
  ),
};

UsersCard.defaultProps = {
  users: [],
};
