import React, { useState } from 'react';
import { Pagination } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';

export default function DisplayPagination({
  perPage, totalPage, variables, refetch,
  handleChangePage = () => '',
}) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [activePage, setActivePage] = useState(1);

  const handlePaginationChange = async (_, data) => {
    setActivePage(data.activePage);
    await refetch({
      ...variables,
      first: perPage,
      after: btoa(String((data.activePage - 1) * perPage)),
    });
    handleChangePage();
  };

  if (isMobile) {
    return (
      <Pagination
        boundaryRange={0}
        siblingRange={0}
        prevItem={null}
        nextItem={null}
        activePage={activePage}
        onPageChange={handlePaginationChange}
        totalPages={totalPage}
      />
    );
  }

  return (
    <Pagination
      boundaryRange={0}
      siblingRange={1}
      activePage={activePage}
      onPageChange={handlePaginationChange}
      totalPages={totalPage}
    />
  );
}

DisplayPagination.propTypes = {
  perPage: PropTypes.number.isRequired,
  totalPage: PropTypes.number.isRequired,
  variables: PropTypes.objectOf(PropTypes.string),
  refetch: PropTypes.func.isRequired,
  handleChangePage: PropTypes.func.isRequired,
};

DisplayPagination.defaultProps = {
  variables: {},
};
