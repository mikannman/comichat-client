import React, {
  useState, useEffect, useRef, useCallback,
} from 'react';
import PropTypes from 'prop-types';
import {
  Grid, Icon, Label, Search,
} from 'semantic-ui-react';
import { useLazyQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { useDispatch, useSelector } from 'react-redux';
import { SEARCH_MANGA_INPUT } from '../../graphql/searchQueries';
import { setInputData } from '../../redux/features/questionForm/questionFormSlice';

export default function SearchMangaInput({ type }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const inputData = useSelector((state) => state.questionForm.inputData);
  const [suggestions, setSuggestions] = useState([]);
  const [lastRequest, setLastRequest] = useState(null);
  const divElem = useRef();

  const dispatch = useDispatch();

  const [getSuggestMangas, { loading: getSuggestLoad }] = useLazyQuery(SEARCH_MANGA_INPUT, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (data.searchManga.edges.length) {
        setSuggestions(data.searchManga.edges.map((edge) => ({ title: edge.node.title })));
      } else {
        setSuggestions([]);
      }
    },
  });

  const suggestionsFetchRequested = (word, selectedList) => {
    if (word) {
      if (lastRequest !== null) {
        clearTimeout(lastRequest);
      }

      setLastRequest(
        setTimeout(() => {
          getSuggestMangas({
            variables: {
              word,
              first: 5,
              selectedList,
            },
          });
        }, 300),
      );
    } else {
      clearTimeout(lastRequest);
      setSuggestions([]);
    }
  };

  const handleResultSelect = (_, data) => {
    const newList = [...inputData[`${type}List`], data.result.title];
    dispatch(setInputData({ [`${type}List`]: newList }));
    suggestionsFetchRequested(data.value, inputData[`${type}List`]);
  };

  const handleSearchChange = (_, data) => {
    dispatch(setInputData({ [`${type}Value`]: data.value }));
    suggestionsFetchRequested(data.value, inputData[`${type}List`]);
  };

  const handleClickDelete = (e) => {
    const newList = inputData[`${type}List`].filter((manga) => manga !== e.target.dataset.manga);
    suggestionsFetchRequested(inputData[`${type}Value`], newList);
    dispatch(setInputData({ [`${type}List`]: newList }));
  };

  const handlePressEnter = useCallback((e) => {
    if (e.isComposing || e.keyCode === 229) {
      return;
    }

    if (suggestions.length && e.keyCode === 13) {
      const newList = [...inputData[`${type}List`], suggestions[0].title];
      dispatch(setInputData({ [`${type}List`]: newList }));
      suggestionsFetchRequested(suggestions[0].title, newList);
      setSuggestions((prev) => prev.filter((_, idx) => idx !== 0));
    }
    divElem.current.removeEventListener('keydown', handlePressEnter);
  }, [suggestions]);

  useEffect(() => {
    divElem.current.addEventListener('keydown', handlePressEnter);
  }, [suggestions]);

  return (
    <Grid
      style={{ margin: '0' }}
      centered={isMobile}
    >
      {
        inputData[`${type}List`].length
          ? (
            inputData[`${type}List`].map((manga) => (
              <Label key={manga} style={{ marginTop: '5px' }}>
                {manga}
                <Icon
                  name="delete"
                  link
                  data-manga={manga}
                  onClick={handleClickDelete}
                />
              </Label>
            ))
          )
          : (
            <Label key="noSelect" style={{ marginTop: '5px' }}>
              選択されていません
            </Label>
          )
      }
      <Grid.Row>
        <div
          ref={divElem}
        >
          <Search
            className="class"
            onResultSelect={handleResultSelect}
            onSearchChange={handleSearchChange}
            value={inputData[`${type}Value`]}
            loading={getSuggestLoad}
            results={suggestions}
            noResultsMessage="検索結果がありません"
          />
        </div>
      </Grid.Row>
    </Grid>
  );
}

SearchMangaInput.propTypes = {
  type: PropTypes.string.isRequired,
};
