import React from 'react';
import { Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { StyledInput, Warning } from '../../style/globalStyle';

const InputWrapper = styled.div`
  width: 100%;
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function InputArea({
  label, objectKey, objectType, errors, handleChange, value,
}) {
  return (
    <Grid.Row
      style={{ marginTop: '20px' }}
      centered
      columns={1}
    >
      <Grid.Column width={10}>
        <Grid>
          <Grid.Row style={{ padding: '0' }}>
            <h4>
              {label}
            </h4>
          </Grid.Row>
          {errors.map((errorMessage) => (
            <Warning key={errorMessage}>{errorMessage}</Warning>
          ))}
          <Grid.Row style={{ padding: '0' }}>
            <InputWrapper>
              <StyledInput
                height="30px"
                type={objectType}
                value={value}
                data-key={objectKey}
                onChange={handleChange}
              />
            </InputWrapper>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    </Grid.Row>
  );
}

InputArea.propTypes = {
  label: PropTypes.string.isRequired,
  objectKey: PropTypes.string.isRequired,
  objectType: PropTypes.string.isRequired,
  errors: PropTypes.arrayOf(PropTypes.string).isRequired,
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
};
