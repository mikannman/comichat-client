import React, { useState, useEffect, useRef } from 'react';
import { useMutation, gql } from '@apollo/client';
import {
  Grid, Button, Dropdown,
} from 'semantic-ui-react';
import { Transition } from 'react-transition-group';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { CircleImage, Supplement } from '../../style/globalStyle';
import { UPDATE_REVIEW, DELETE_REVIEW } from '../../graphql/reviewQueries';
import { setConfirmConfig } from '../../redux/features/confirm/confirmSlice';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import DisplayStars from './DisplayStars';
import { howLongAgo } from '../../functions/function';
import WriteArea from '../manga/WriteArea';
import Like from './Like';
import StrechContent from './StrechContent';

const duration = 300;

const defaultSyle = {
  transition: `all ${duration}ms ease`,
  height: '0px',
  width: '100%',
  overflow: 'hidden',
};

const currentUserReviewFragment = gql`
  fragment currentUserReviewRef on User {
    id
    reviews {
      count
    }
  }
`;

export default function Review({ edge, refetch }) {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const onMyPage = useSelector((state) => state.user.onMyPage);
  const [visibleNetabareReview, setVisibleNetabareReview] = useState({
    height: '',
    state: false,
  });
  const [editMode, setEditMode] = useState(false);
  const dispatch = useDispatch();
  const contentElem = useRef(null);

  useEffect(() => {
    if (contentElem.current) {
      if (edge.node.netabare) {
        setVisibleNetabareReview((prev) => ({
          ...prev,
          height: `${contentElem.current.getBoundingClientRect().height}px`,
        }));
      }
    }
  }, [contentElem]);

  const transitionStyles = {
    entering: {
      height: '0px',
    },
    entered: {
      height: visibleNetabareReview.height,
    },
    exiting: {
      height: visibleNetabareReview.height,
    },
    exited: {
      height: '0px',
    },
  };

  const [updateReview] = useMutation(UPDATE_REVIEW, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: () => setEditMode(false),
  });
  const [deleteReview] = useMutation(DELETE_REVIEW, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: () => {
      refetch();
    },
    update: (cache) => {
      if (onMyPage) {
        const currentUserRef = cache.readFragment({
          id: `User:${currentUser.id}`,
          fragment: currentUserReviewFragment,
        });

        cache.writeFragment({
          id: `User:${currentUser.id}`,
          fragment: currentUserReviewFragment,
          data: {
            reviews: {
              count: currentUserRef.reviews.count - 1,
            },
          },
        });
      }
    },
  });

  const handleClickDelete = () => {
    dispatch(setConfirmConfig({
      open: true,
      content: '本当に削除しますか',
      onConfirm: () => deleteReview({ variables: { id: edge.node.id } }),
    }));
  };

  const handleClickEdit = () => {
    setEditMode(true);
  };

  const handleClickNetabareReview = () => setVisibleNetabareReview((prev) => ({
    ...prev,
    state: !prev.state,
  }));

  const handleSubmit = async (variables) => {
    updateReview({
      variables: { ...variables },
    });
  };

  const handleClickCancel = () => {
    setEditMode(false);
  };

  return (
    <Grid style={{ margin: '15px 0 0 35px' }}>
      <Grid.Row style={{ display: 'flex', height: '60px' }}>
        <CircleImage
          src={edge.node.user.imageURL || DefaultAccontImage}
          alt="プロフィール画像"
          height="40"
          width="40"
        />
        <h4
          style={{ margin: '0 0 0 15px', alignSelf: 'center' }}
        >
          <Link
            to={
              currentUserId === edge.node.user.id
                ? '/user/mypage'
                : `/user/${edge.node.user.id}`
            }
          >
            {edge.node.user.name}
          </Link>
        </h4>
        <Supplement
          alignSelf="center"
          marginLeft="15px"
        >
          {howLongAgo(edge.node.updatedAt)}
          <span style={{ marginLeft: '10px' }}>
            <Like
              id={edge.node.id}
              type="Review"
              isLike={edge.node.currentUserLikes.isExist}
            />
          </span>
          {edge.node.likeReviews.count}
        </Supplement>
        {
          currentUserId === edge.node.user.id
            && (
              <Dropdown
                icon="ellipsis vertical"
                style={{ alignSelf: 'center', color: 'grey', margin: '0 0 0 10px' }}
              >
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={handleClickEdit}
                  >
                    編集
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={handleClickDelete}
                  >
                    削除
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            )
        }
      </Grid.Row>
      {
        editMode
          ? (
            <WriteArea
              star={edge.node.star}
              comment={edge.node.comment}
              netabare={edge.node.netabare}
              submitButtonText="投稿"
              variables={{ id: edge.node.id }}
              handleClickCancel={handleClickCancel}
              handleSubmit={handleSubmit}
            />
          )
          : (
            <div>
              <Grid.Row style={{ padding: '0', margin: '0 0 0 10px' }}>
                <DisplayStars star={edge.node.star} />
              </Grid.Row>
              <div style={{ margin: '15px 0 0 0' }}>
                {
                  edge.node.netabare && (edge.node.user.id !== currentUserId)
                    ? (
                      <div>
                        <Button onClick={handleClickNetabareReview}>
                          ネタバレ注意！！続きを読む場合はクリックしてください
                        </Button>
                        <Transition in={visibleNetabareReview.state} timeout={duration}>
                          {(state) => (
                            <div
                              style={{
                                ...defaultSyle,
                                ...transitionStyles[state],
                                whiteSpace: 'pre-wrap',
                              }}
                            >
                              <div ref={contentElem}>
                                {edge.node.comment}
                              </div>
                            </div>
                          )}
                        </Transition>
                      </div>
                    )
                    : (
                      <StrechContent
                        content={edge.node.comment}
                      />
                    )
                }
              </div>
            </div>
          )
      }
    </Grid>
  );
}

Review.propTypes = {
  edge: PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired,
      comment: PropTypes.string.isRequired,
      star: PropTypes.number.isRequired,
      netabare: PropTypes.bool.isRequired,
      updatedAt: PropTypes.string.isRequired,
      likeReviews: PropTypes.shape({
        count: PropTypes.number.isRequired,
      }).isRequired,
      user: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        imageURL: PropTypes.string.isRequired,
      }).isRequired,
      manga: PropTypes.shape({
        mid: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }).isRequired,
      currentUserLikes: PropTypes.shape({
        isExist: PropTypes.bool.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
  refetch: PropTypes.func.isRequired,
};
