import React from 'react';
import { Grid } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Warning, StyledTextarea } from '../../style/globalStyle';

export default function MessageInputArea({
  errors, handleChange, value,
}) {
  return (
    <Grid.Row
      style={{ marginTop: '20px' }}
      centered
      columns={1}
    >
      <Grid.Column width={10}>
        <Grid>
          <Grid.Row style={{ padding: '0' }}>
            <h4>
              メッセージ
            </h4>
          </Grid.Row>
          {errors.map((errorMessage) => (
            <Warning key={errorMessage}>{errorMessage}</Warning>
          ))}
          <Grid.Row style={{ padding: '0', marginTop: '15px' }}>
            <StyledTextarea
              value={value}
              onChange={handleChange}
              border="solid 1px red"
              height="100px"
              padding="10px"
            />
          </Grid.Row>
        </Grid>
      </Grid.Column>
    </Grid.Row>
  );
}

MessageInputArea.propTypes = {
  errors: PropTypes.arrayOf(PropTypes.string).isRequired,
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
};
