import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Modal } from 'semantic-ui-react';
import CreateQuestionForm from './CreateQuestionForm';

const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  background-color: #ebebeb;
  height: 60px;
  position: relative;
`;

const CloseButton = styled.div`
  position: absolute;
  right: 10%;
  top: 37%;
  color: #b0b0b0;
  cursor: pointer;
`;

const HeaderText = styled.h2`
  padding-top: 16px;
`;

export default function QuestionModal({
  userId, name, modalOpen, setModalOpen,
}) {
  const handleOnClose = () => setModalOpen(false);

  const handleClickCloseButton = () => setModalOpen(false);

  return (
    <Modal
      open={modalOpen}
      onClose={handleOnClose}
      size="small"
    >
      <StyledHeader>
        <HeaderText>
          {`${name}さんにおすすめを聞いてみる`}
        </HeaderText>
        <CloseButton
          onClick={handleClickCloseButton}
        >
          閉じる
        </CloseButton>
      </StyledHeader>
      <div style={{ padding: '10px' }}>
        <CreateQuestionForm
          labelWidth={5}
          askedUserId={userId}
          setQuestionModalOpen={setModalOpen}
        />
      </div>
    </Modal>
  );
}

QuestionModal.propTypes = {
  userId: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  modalOpen: PropTypes.bool.isRequired,
  setModalOpen: PropTypes.func.isRequired,
};
