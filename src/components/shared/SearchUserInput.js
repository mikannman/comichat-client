import React, {
  useState, useEffect, useRef,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Grid, Icon, Label, Search,
} from 'semantic-ui-react';
import { useLazyQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { SEARCH_USER_INPUT } from '../../graphql/searchQueries';
import DefaultAccontImage from '../image/defaultAccountImage.png';
import { setInputData } from '../../redux/features/questionForm/questionFormSlice';

const resultRenderer = ({ name }) => (
  <Label>
    {name}
  </Label>
);

export default function SearchUserInput() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const inputData = useSelector((state) => state.questionForm.inputData);
  const [suggestions, setSuggestions] = useState([]);
  const [lastRequest, setLastRequest] = useState(null);
  const divElem = useRef();

  const dispatch = useDispatch();

  const [getSuggestUsers, { loading: getSuggestLoad }] = useLazyQuery(SEARCH_USER_INPUT, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (data.searchUser.length) {
        setSuggestions(data.searchUser.map((u) => (
          {
            id: u.id,
            name: u.name,
            imageURL: u.imageURL,
            userId: u.userId,
          }
        )));
      } else {
        setSuggestions([]);
      }
    },
  });

  const suggestionsFetchRequested = (word, selectedUserId) => {
    if (word) {
      if (lastRequest !== null) {
        clearTimeout(lastRequest);
      }

      setLastRequest(
        setTimeout(() => {
          getSuggestUsers({
            variables: {
              word,
              selectedUserId,
            },
          });
        }, 300),
      );
    } else {
      clearTimeout(lastRequest);
      setSuggestions([]);
    }
  };

  const handleResultSelect = (_, data) => {
    setSuggestions([]);
    dispatch(setInputData({
      askedUser: {
        id: data.result.id,
        name: data.result.name,
        imageURL: data.result.imageURL,
      },
    }));
  };

  const handleSearchChange = (_, data) => {
    dispatch(setInputData({ askedUserValue: data.value }));
    suggestionsFetchRequested(data.value, inputData.askedUser.userId && inputData.askedUser.userId);
  };

  const handleClickDelete = () => {
    setSuggestions([]);
    dispatch(setInputData({
      askedUser: {
        id: '',
        name: '',
        imageURL: '',
        userId: '',
      },
    }));
  };

  const handlePressEnter = (e) => {
    if (e.isComposing || e.keyCode === 229) {
      return;
    }

    if (suggestions.length && e.keyCode === 13) {
      dispatch(setInputData({
        askedUser: {
          id: suggestions[0].id,
          name: suggestions[0].name,
          imageURL: suggestions[0].imageURL,
          userId: suggestions[0].userId,
        },
      }));
      setSuggestions([]);
    }
    divElem.current.removeEventListener('keydown', handlePressEnter);
  };

  useEffect(() => {
    divElem.current.addEventListener('keydown', handlePressEnter);
  }, [suggestions]);

  return (
    <Grid
      style={{ margin: '0' }}
      centered={isMobile}
    >
      {
        inputData.askedUser.userId
          ? (
            <Label image>
              <img src={inputData.askedUser.imageURL || DefaultAccontImage} alt="ユーザー画像" />
              {inputData.askedUser.name}
              <small>{inputData.askedUser.userId}</small>
              <Icon
                name="delete"
                link
                onClick={handleClickDelete}
              />
            </Label>
          )
          : (
            <Label>
              選択されていません
            </Label>
          )
      }
      <Grid.Row>
        <div
          ref={divElem}
        >
          <Search
            loading={getSuggestLoad}
            onResultSelect={handleResultSelect}
            onSearchChange={handleSearchChange}
            results={suggestions}
            resultRenderer={resultRenderer}
            value={inputData.askedUserValue}
            noResultsMessage="検索結果がありません"
          />
        </div>
      </Grid.Row>
    </Grid>
  );
}
