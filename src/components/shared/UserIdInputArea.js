import React, { useState } from 'react';
import { Grid, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import { useLazyQuery } from '@apollo/client';
import PropTypes from 'prop-types';
import { StyledInput } from '../../style/globalStyle';
import { VERIFY_USER_ID } from '../../graphql/authQueries';

const InputWrapper = styled.div`
  width: 100%;
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function UserIdInputArea({
  value, handleChange, onVerifiedUserId,
}) {
  const [lastRequest, setLastRequest] = useState(null);

  const [verifyUserId, { data, loading }] = useLazyQuery(VERIFY_USER_ID, {
    fetchPolicy: 'network-only',
    onCompleted: (result) => onVerifiedUserId(result.verifyUserId.type),
  });

  const fetchWord = (word) => {
    if (word) {
      if (lastRequest !== null) {
        clearTimeout(lastRequest);
      }

      setLastRequest(
        setTimeout(() => {
          verifyUserId({
            variables: {
              userId: word,
            },
          });
        }, 300),
      );
    } else {
      clearTimeout(lastRequest);
    }
  };

  const handleChangeInput = (e) => {
    handleChange(e);
    fetchWord(e.target.value);
  };

  return (
    <Grid.Row
      style={{ marginTop: '20px' }}
      centered
      columns={1}
    >
      <Grid.Column width={10}>
        <Grid>
          <Grid.Row
            style={{
              padding: '0',
              display: 'flex',
              justifyContent: 'flex-start',
              height: '30px',
            }}
          >
            <h4>
              ユーザーID
            </h4>
            {
              loading
                ? (
                  <Icon
                    loading
                    name="spinner"
                    style={{
                      marginLeft: '15px',
                    }}
                  />
                )
                : (
                  data
                    && (
                      <div
                        style={{
                          marginLeft: '10px',
                          color: data.verifyUserId.type,
                        }}
                      >
                        {data.verifyUserId.text}
                      </div>
                    )
                )
            }
          </Grid.Row>
          <Grid.Row style={{ padding: '0' }}>
            <InputWrapper>
              <StyledInput
                height="30px"
                type="text"
                value={value}
                onChange={handleChangeInput}
              />
            </InputWrapper>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    </Grid.Row>
  );
}

UserIdInputArea.propTypes = {
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  onVerifiedUserId: PropTypes.func.isRequired,
};
