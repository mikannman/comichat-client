import React from 'react';
import { useQuery } from '@apollo/client';
import { Icon, Label } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import { GET_POPULAR_KEYWORDS } from '../../graphql/sharedQueries';

export default function PopularKeywords() {
  const history = useHistory();
  const { loading, data } = useQuery(GET_POPULAR_KEYWORDS, {
    fetchPolicy: 'network-only',
  });

  const handleClickLabel = (e) => {
    history.push(`/search?title=${e.target.dataset.keyword}`);
  };

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  return (
    data.getPopularKeywords.map((keyword) => (
      <Label
        key={keyword.word}
        data-keyword={keyword.word}
        onClick={handleClickLabel}
      >
        {keyword.word}
      </Label>
    ))
  );
}
