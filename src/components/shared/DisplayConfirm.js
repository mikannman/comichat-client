import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Modal } from 'semantic-ui-react';
import { setConfirmOpen } from '../../redux/features/confirm/confirmSlice';

export default function DisplayConfirm() {
  const confirmState = useSelector((state) => state.confirm);
  const dispatch = useDispatch();

  const handleCancel = () => {
    confirmState.onCancel();
    dispatch(setConfirmOpen(false));
  };

  const handleConfirm = () => {
    confirmState.onConfirm();
    dispatch(setConfirmOpen(false));
  };

  const handleClose = () => {
    dispatch(setConfirmOpen(false));
  };

  return (
    <Modal
      open={confirmState.open}
      onClose={handleClose}
      size="tiny"
    >
      <Modal.Content
        style={{
          textAlign: 'center',
        }}
      >
        <h5>
          {confirmState.content}
        </h5>
      </Modal.Content>
      <Modal.Actions>
        <Button
          onClick={handleCancel}
        >
          キャンセル
        </Button>
        <Button
          onClick={handleConfirm}
          color="red"
        >
          削除
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
