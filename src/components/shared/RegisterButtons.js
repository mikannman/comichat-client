import React from 'react';
import { useMutation, gql } from '@apollo/client';
import { Grid, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import {
  REGISTER_READED_MANGA, REGISTER_WANNA_READ_MANGA,
  UNREGISTER_READED_MANGA, UNREGISTER_WANNA_READ_MANGA,
} from '../../graphql/mangaQueries';
import usePromptSignIn from '../../hooks/usePromptSignIn';

const mangaRegisterFragment = gql`
  fragment mangaRef on Manga {
    mid
    currentUserReaded {
      isExist
    }
    currentUserWannaRead {
      isExist
    }
  }
`;

export default function RegisterButtons({
  mid, isReadedManga, isWannaReadManga, size, width,
  fontSize, padding, margin,
}) {
  const promptSignIn = usePromptSignIn();

  const [
    registerWannaReadManga, { loading: registerWannaRead },
  ] = useMutation(REGISTER_WANNA_READ_MANGA, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      cache.writeFragment({
        id: `Manga:{"mid":"${mid}"}`,
        fragment: mangaRegisterFragment,
        data: {
          currentUserWannaRead: {
            isExist: true,
          },
        },
      });
    },
  });
  const [
    unregisterWannaReadManga, { loading: unregisterWannaRead },
  ] = useMutation(UNREGISTER_WANNA_READ_MANGA, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      cache.writeFragment({
        id: `Manga:{"mid":"${mid}"}`,
        fragment: mangaRegisterFragment,
        data: {
          currentUserWannaRead: {
            isExist: false,
          },
        },
      });
    },
  });
  const [
    registerReadedManga, { loading: registerReaded },
  ] = useMutation(REGISTER_READED_MANGA, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      cache.writeFragment({
        id: `Manga:{"mid":"${mid}"}`,
        fragment: mangaRegisterFragment,
        data: {
          currentUserReaded: {
            isExist: true,
          },
        },
      });
    },
  });
  const [
    unregisterReadedManga, { loading: unregisterReaded },
  ] = useMutation(UNREGISTER_READED_MANGA, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      cache.writeFragment({
        id: `Manga:{"mid":"${mid}"}`,
        fragment: mangaRegisterFragment,
        data: {
          currentUserReaded: {
            isExist: false,
          },
        },
      });
    },
  });

  const handleClickWannaReadManga = () => {
    promptSignIn({
      callback: () => {
        if (isWannaReadManga) {
          unregisterWannaReadManga({
            variables: {
              mid,
            },
          });
        } else {
          registerWannaReadManga({
            variables: {
              mid,
            },
          });
        }
      },
    });
  };

  const handleClickReadedManga = () => {
    promptSignIn({
      callback: () => {
        if (isReadedManga) {
          unregisterReadedManga({
            variables: {
              mid,
            },
          });
        } else {
          registerReadedManga({
            variables: {
              mid,
            },
          });
        }
      },
    });
  };

  return (
    <Grid.Row>
      <Button
        onClick={handleClickWannaReadManga}
        style={{
          backgroundColor: 'white',
          color: isWannaReadManga ? 'grey' : '#76c639',
          border: `solid 1px ${isWannaReadManga ? 'grey' : '#76c639'}`,
          width,
          fontSize,
          padding,
          margin,
        }}
        loading={registerWannaRead || unregisterWannaRead}
        size={size}
      >
        {isWannaReadManga ? '読みたいを解除' : '読みたい'}
      </Button>
      <Button
        onClick={handleClickReadedManga}
        style={{
          backgroundColor: isReadedManga ? 'grey' : '#76c639',
          color: 'white',
          width,
          fontSize,
          padding,
          margin,
        }}
        loading={registerReaded || unregisterReaded}
        size={size}
      >
        {isReadedManga ? '読んだを解除' : '読んだ'}
      </Button>
    </Grid.Row>
  );
}

RegisterButtons.propTypes = {
  mid: PropTypes.string.isRequired,
  isReadedManga: PropTypes.bool.isRequired,
  isWannaReadManga: PropTypes.bool.isRequired,
  size: PropTypes.string.isRequired,
  width: PropTypes.string,
  fontSize: PropTypes.string,
  padding: PropTypes.string,
  margin: PropTypes.string,
};

RegisterButtons.defaultProps = {
  width: '145px',
  fontSize: '',
  padding: '10px',
  margin: '',
};
