import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useMutation, gql } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import { useDispatch, useSelector } from 'react-redux';
import { CREATE_QUESTION } from '../../graphql/questionQueries';
import WriteQuestionArea from './WriteQuestionArea';
import MobileWriteQuestionArea from './responsive/MobileWriteQuestionArea';
import { allReset } from '../../redux/features/questionForm/questionFormSlice';

const query = gql`
  query GetSmallAllQuestionsByCondition($first: Int, $after: String, $condition: String! $CREATE: Boolean) {
    getAllQuestionsByCondition(first: $first, after: $after, condition: $condition, CREATE: $CREATE) {
      edges {
        node {
          id
          content
        }
      }
    }
  }
`;

export default function CreateQuestionForm({
  labelWidth, askedUserId, setQuestionModalOpen,
}) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const dispatch = useDispatch();
  const inputData = useSelector((state) => state.questionForm.inputData);
  const genres = useSelector((state) => state.genre.data);
  const [genreState, setGenreState] = useState(() => (
    genres.map((genre) => ({ genre, choiced: false }))
  ));

  const [createQuestion] = useMutation(CREATE_QUESTION, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: () => {
      dispatch(allReset());
      setQuestionModalOpen(false);
      setGenreState(() => (
        genres.map((genre) => ({ genre, choiced: false }))
      ));
      window.flash('質問を送信しました');
    },
    update: (cache, { data: { createQuestion: cq } }) => {
      const recentData = cache.readQuery({
        query,
        variables: {
          condition: 'recent',
        },
      });

      const unansweredData = cache.readQuery({
        query,
        variables: {
          condition: 'unanswered',
        },
      });

      cache.writeQuery({
        query,
        variables: {
          condition: 'recent',
          CREATE: true,
        },
        data: {
          getAllQuestionsByCondition: {
            edges: [
              {
                node: cq.question,
                __typename: 'QuestionEdge',
              },
              ...recentData.getAllQuestionsByCondition.edges,
            ],
          },
        },
      });

      cache.writeQuery({
        query,
        variables: {
          condition: 'unanswered',
          CREATE: true,
        },
        data: {
          getAllQuestionsByCondition: {
            edges: [
              {
                node: cq.question,
                __typename: 'QuestionEdge',
              },
              ...unansweredData.getAllQuestionsByCondition.edges,
            ],
          },
        },
      });
    },
  });

  useEffect(() => (
    () => {
      dispatch(allReset());
    }
  ), []);

  const handleClickSubmitButton = () => {
    createQuestion({
      variables: {
        askedUserId: Number(askedUserId) || Number(inputData.askedUser.id) || null,
        genres: genreState.filter((genre) => genre.choiced).map((genre) => genre.genre),
        readingMangaMid: inputData.readingMangaList.join(),
        readedMangaMid: inputData.readedMangaList.join(),
        content: inputData.textareaValue,
        all: inputData.checkedAll,
      },
    });
  };

  if (isMobile) {
    return (
      <MobileWriteQuestionArea
        askedUserId={askedUserId}
        handleClickSubmitButton={handleClickSubmitButton}
        submitButtonText="この内容で質問する"
        genreState={genreState}
        setGenreState={setGenreState}
      />
    );
  }

  return (
    <WriteQuestionArea
      labelWidth={labelWidth}
      askedUserId={askedUserId}
      handleClickSubmitButton={handleClickSubmitButton}
      submitButtonText="この内容で質問する"
      genreState={genreState}
      setGenreState={setGenreState}
    />
  );
}

CreateQuestionForm.propTypes = {
  labelWidth: PropTypes.number.isRequired,
  askedUserId: PropTypes.string,
  setQuestionModalOpen: PropTypes.func,
};

CreateQuestionForm.defaultProps = {
  askedUserId: '',
  setQuestionModalOpen: (b) => b,
};
