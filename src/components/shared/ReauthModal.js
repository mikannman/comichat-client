import React, { useState } from 'react';
import styled from 'styled-components';
import { Modal, Button, Icon } from 'semantic-ui-react';
import { useDispatch, useSelector } from 'react-redux';
import { setOpen } from '../../redux/features/reauthModal/reauthModalSlice';
import {
  Warning, Wrapper, Center, DivSpacer, StyledInput,
} from '../../style/globalStyle';
import { useAuth } from '../../hooks/useAuth';
import {
  providerFacebook, providerTwitter,
} from '../../firebase/config';

const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  background-color: #ebebeb;
  height: 60px;
  position: relative;
`;

const CloseButton = styled.div`
  position: absolute;
  right: 10%;
  top: 37%;
  color: #b0b0b0;
  cursor: pointer;
`;

const HeaderText = styled.h2`
  padding-top: 16px;
`;

const InputWrapper = styled.div`
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: #da3c41;
  }
`;

export default function ReauthModal() {
  const dispatch = useDispatch();
  const modalOpen = useSelector((state) => state.reauthModal.open);
  const modalCallback = useSelector((state) => state.reauthModal.callback);
  const currentUser = useSelector((state) => state.user.value);
  const [inputData, setInputData] = useState({
    email: '',
    password: '',
  });
  const [errors, setErrors] = useState({
    email: [],
    password: [],
    communication: [],
  });
  const { emailAndPasswordSignIn, SNSSignIn, loading } = useAuth({});

  const onClose = () => {
    dispatch(setOpen(false));
  };

  const facebookLogin = () => {
    SNSSignIn({
      provider: providerFacebook,
      providerName: 'Facebook',
      setErrors,
      callback: modalCallback,
    });
  };

  const twitterLogin = () => {
    SNSSignIn({
      provider: providerTwitter,
      providerName: 'Twitter',
      setErrors,
      callback: modalCallback,
    });
  };

  const handleChangeEmail = (e) => {
    setInputData((prev) => ({ ...prev, email: e.target.value }));
  };

  const handleChangePassword = (e) => {
    setInputData((prev) => ({ ...prev, password: e.target.value }));
  };

  const handleEmailAndPasswordSignIn = (e) => {
    e.preventDefault();
    emailAndPasswordSignIn({
      email: inputData.email,
      password: inputData.password,
      setErrors,
      callback: modalCallback,
    });
  };

  return (
    <Modal
      open={modalOpen}
      onClose={onClose}
      size="small"
    >
      <StyledHeader>
        <HeaderText>
          再認証する
        </HeaderText>
        <CloseButton
          onClick={onClose}
        >
          閉じる
        </CloseButton>
      </StyledHeader>
      <h4 style={{ textAlign: 'center' }}>
        この操作を続行するには、もう一度ログイン操作を行う必要があります。
      </h4>
      {errors.communication.map((errorMessage) => (
        <Warning
          key={errorMessage}
          margin="10px 0 0 20px"
        >
          {errorMessage}
        </Warning>
      ))}
      <Wrapper
        padding="30px"
      >
        {
          currentUser && currentUser.provider !== 'password'
            ? (
              <div>
                <h4>SNSアカウントで再認証</h4>
                <Center>
                  <Button
                    color="facebook"
                    onClick={facebookLogin}
                  >
                    <Icon name="facebook" />
                    Facebook
                  </Button>
                  <DivSpacer marginLeft="20px">
                    <Button
                      color="twitter"
                      onClick={twitterLogin}
                    >
                      <Icon name="twitter" />
                      Twitter
                    </Button>
                  </DivSpacer>
                </Center>
              </div>
            )
            : (
              <DivSpacer>
                <h4>メールアドレスで再認証</h4>
                <form
                  noValidate
                  onSubmit={handleEmailAndPasswordSignIn}
                >
                  {errors.email.map((errorMessage) => (
                    <Warning key={errorMessage}>{errorMessage}</Warning>
                  ))}
                  <InputWrapper style={{ marginBottom: '10px' }}>
                    <StyledInput
                      height="40px"
                      placeholder="メールアドレス"
                      type="email"
                      value={inputData.email}
                      onChange={handleChangeEmail}
                    />
                  </InputWrapper>
                  {errors.password.map((errorMessage) => (
                    <Warning key={errorMessage}>{errorMessage}</Warning>
                  ))}
                  <InputWrapper>
                    <StyledInput
                      height="40px"
                      placeholder="パスワード"
                      type="password"
                      value={inputData.password}
                      onChange={handleChangePassword}
                    />
                  </InputWrapper>
                  <Center marginTop="20px">
                    <Button
                      color="teal"
                      type="submit"
                      disabled={!inputData.email || !inputData.password}
                      loading={loading}
                    >
                      再認証
                    </Button>
                  </Center>
                </form>
              </DivSpacer>
            )
        }
      </Wrapper>
    </Modal>
  );
}
