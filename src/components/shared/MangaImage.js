import React, { useState, useEffect } from 'react';
import { Placeholder } from 'semantic-ui-react';
import { useMutation } from '@apollo/client';
import PropTypes from 'prop-types';
import NoImage from '../image/NO-IMAGE.jpg';
import { ADD_IMAGE_URL_AND_AFFILIATE_URL } from '../../graphql/mangaQueries';

export default function MangaImage({
  title, length, setItemURL,
  imageUrl, affiliateUrl, mid,
}) {
  const [imageURL, setImageURL] = useState('');
  const [loading, setLoading] = useState(true);

  const [addImageUrlAndAffiliateUrl] = useMutation(ADD_IMAGE_URL_AND_AFFILIATE_URL);

  useEffect(() => {
    if (imageUrl) {
      setImageURL(imageUrl);
    }

    fetch(
      `https://app.rakuten.co.jp/services/api/BooksBook/Search/
      ${process.env.REACT_APP_RAKUTEN_ID}?
      format=json&title=${encodeURIComponent(title.replace(/\s:\s(.+)/, ''))}&size=0&booksGenreId=001001&
      sort=sales&hits=10&applicationId=${process.env.REACT_APP_RAKUTEN_APPLICATION_ID}&
      affiliateId=${process.env.REACT_APP_RAKUTEN_AFFILIATE_ID}`.replace(/\s+/g, ''),
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.Items.length) {
          data.Items.some((item) => {
            if (item.Item.title.includes(title.replace(/\s:\s(.+)/, ''))) {
              if (item.Item.largeImageUrl !== imageUrl
                || !imageUrl
                || !affiliateUrl
              ) {
                setImageURL(item.Item.largeImageUrl);

                if (setItemURL) {
                  setItemURL(item.Item.affiliateUrl);
                }

                addImageUrlAndAffiliateUrl({
                  variables: {
                    mid,
                    imageUrl: item.Item.largeImageUrl,
                    affiliateUrl: item.Item.affiliateUrl,
                  },
                });
              } else {
                setImageURL(imageUrl);
                if (setItemURL) {
                  setItemURL(affiliateUrl);
                }
              }
              return true;
            }
            return false;
          });
        } else {
          setImageURL(imageUrl);
          if (setItemURL) {
            setItemURL(affiliateUrl);
          }
        }
        setLoading(false);
      })
      .catch(() => {
        setImageURL(imageUrl);
        if (setItemURL) {
          setItemURL(affiliateUrl);
        }
        setLoading(false);
      });
  }, []);

  return (
    loading
      ? (
        <Placeholder
          style={{
            height: `${length * 3}px`,
            width: `${length * 2}px`,
          }}
        >
          <Placeholder.Image square />
        </Placeholder>
      )
      : (
        <img
          src={imageURL || NoImage}
          alt="準備中"
          style={{
            height: `${length * 3}px`,
            width: `${length * 2}px`,
            objectFit: 'cover',
          }}
        />
      )
  );
}

MangaImage.propTypes = {
  title: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
  setItemURL: PropTypes.func,
  imageUrl: PropTypes.string,
  affiliateUrl: PropTypes.string,
  mid: PropTypes.string.isRequired,
};

MangaImage.defaultProps = {
  setItemURL: null,
  imageUrl: '',
  affiliateUrl: '',
};
