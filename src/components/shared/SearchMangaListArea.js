import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import { Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';
import { SEARCH_MANGA } from '../../graphql/sharedQueries';
import MangaListArea from './MangaListArea';
import MobileMangaListArea from './responsive/MobileMangaListArea';
import TabletMangaListArea from './responsive/TabletMangaListArea';

const perPage = 10;

export default function SearchMangaListArea({ word, condition, isSearch }) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const isMobileAndTablet = useMediaQuery({ maxWidth: 991 });
  const [totalPage, setTotalPage] = useState(null);
  const {
    loading, data, refetch,
  } = useQuery(SEARCH_MANGA, {
    variables: { word, condition, first: perPage },
    onCompleted: (result) => {
      setTotalPage(Math.ceil(result.searchManga.totalCount / perPage));
    },
  });

  if (loading) {
    return <Icon loading name="spinner" />;
  }

  if (isMobileAndTablet) {
    return (
      <div>
        {
          isSearch
            ? (
              <h1
                style={{
                  fontSize: isMobile ? '20px' : '',
                  margin: '0 0 40px 0',
                }}
              >
                {word}
                の検索結果
                {data.searchManga.totalCount}
                件
              </h1>
            )
            : (
              <h1
                style={{
                  fontSize: isMobile ? '20px' : '',
                  margin: '0 0 40px 0',
                }}
              >
                {word}
                の漫画一覧
                {data.searchManga.totalCount}
                件
              </h1>
            )
        }
        {
          isMobile
            ? (
              <MobileMangaListArea
                mangas={data.searchManga.edges}
                totalPage={totalPage}
                perPage={perPage}
                paginationVariables={{ word }}
                paginationRefetch={refetch}
              />
            )
            : (
              <TabletMangaListArea
                mangas={data.searchManga.edges}
                totalPage={totalPage}
                perPage={perPage}
                paginationVariables={{ word }}
                paginationRefetch={refetch}
              />
            )
        }
      </div>
    );
  }

  return (
    <div>
      {
        isSearch
          ? (
            <h1>
              {word}
              の検索結果
              {data.searchManga.totalCount}
              件
            </h1>
          )
          : (
            <h1>
              {word}
              の漫画一覧
              {data.searchManga.totalCount}
              件
            </h1>
          )
      }
      <MangaListArea
        mangas={data.searchManga.edges}
        totalPage={totalPage}
        perPage={perPage}
        paginationVariables={{ word }}
        paginationRefetch={refetch}
      />
    </div>
  );
}

SearchMangaListArea.propTypes = {
  word: PropTypes.string.isRequired,
  condition: PropTypes.string.isRequired,
  isSearch: PropTypes.bool,
};

SearchMangaListArea.defaultProps = {
  isSearch: false,
};
