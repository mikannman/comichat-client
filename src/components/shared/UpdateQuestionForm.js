import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { useMutation } from '@apollo/client';
import { UPDATE_QUESTION } from '../../graphql/questionQueries';
import { setInputData, allReset } from '../../redux/features/questionForm/questionFormSlice';
import WriteQuestionArea from './WriteQuestionArea';
import MobileWriteQuestionArea from './responsive/MobileWriteQuestionArea';

export default function UpdateQuestionForm({
  id, selectedGenre, readingMangaMid, readedMangaMid, content, all, askedUser, setEditMode,
  labelWidth,
}) {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const dispatch = useDispatch();
  const inputData = useSelector((state) => state.questionForm.inputData);
  const genres = useSelector((state) => state.genre.data);
  const [genreState, setGenreState] = useState(() => (
    genres.map((genre) => {
      if (selectedGenre.includes(genre)) {
        return { genre, choiced: true };
      }
      return { genre, choiced: false };
    })
  ));

  useEffect(() => {
    dispatch(setInputData({
      readedMangaList: readedMangaMid ? readedMangaMid.split(',') : [],
      readingMangaList: readingMangaMid ? readingMangaMid.split(',') : [],
      textareaValue: content,
      checkedAll: all,
      askedUser,
    }));

    return () => dispatch(allReset());
  }, []);

  const [updateQuestion] = useMutation(UPDATE_QUESTION, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    onCompleted: () => {
      setEditMode(false);
      window.flash('更新しました');
      dispatch(allReset());
    },
  });

  const handleClickSubmitButton = () => {
    updateQuestion({
      variables: {
        id,
        askedUserId: inputData.askedUser.id,
        genres: genreState.filter((genre) => genre.choiced).map((genre) => genre.genre),
        readingMangaMid: inputData.readingMangaList.join(),
        readedMangaMid: inputData.readedMangaList.join(),
        content: inputData.textareaValue,
        all: inputData.checkedAll,
      },
    });
  };

  const handleClickCancel = () => setEditMode(false);

  if (isMobile) {
    return (
      <MobileWriteQuestionArea
        handleClickSubmitButton={handleClickSubmitButton}
        submitButtonText="この内容で更新する"
        genreState={genreState}
        setGenreState={setGenreState}
        updateMode
        handleClickCancel={handleClickCancel}
      />
    );
  }

  return (
    <WriteQuestionArea
      labelWidth={labelWidth}
      handleClickSubmitButton={handleClickSubmitButton}
      submitButtonText="この内容で更新する"
      genreState={genreState}
      setGenreState={setGenreState}
      updateMode
      handleClickCancel={handleClickCancel}
    />
  );
}

UpdateQuestionForm.propTypes = {
  id: PropTypes.string.isRequired,
  labelWidth: PropTypes.number.isRequired,
  selectedGenre: PropTypes.string.isRequired,
  readingMangaMid: PropTypes.string.isRequired,
  readedMangaMid: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  all: PropTypes.bool.isRequired,
  askedUser: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    imageURL: PropTypes.string,
  }),
  setEditMode: PropTypes.func.isRequired,
};

UpdateQuestionForm.defaultProps = {
  askedUser: {
    id: '',
    name: '',
    imageURL: '',
  },
};
