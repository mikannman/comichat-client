import React from 'react';
import { Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Row } from '../../style/globalStyle';

const GreyStars = styled.div`
  color: grey;
  position: relative;
  z-index: 0;
  width: ${(props) => props.width};
`;

const YellowStars = styled.div`
  color: #fbbd08;
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: ${(props) => props.width};
  overflow: hidden;
`;

export default function DisplayMangaStars({ reviewsAverage, size, height }) {
  let width;

  switch (size) {
    case 'big':
      width = '171px';
      break;
    case 'large':
      width = '135px';
      break;
    case 'small':
      width = '72px';
      break;
    case 'mini':
      width = '52px';
      break;
    default:
      width = '';
  }

  return (
    <GreyStars
      width={width}
    >
      <Row>
        {[0, 1, 2, 3, 4].map((i) => (
          <Icon name="star" size={size} key={i} />
        ))}
      </Row>
      <YellowStars
        width={`${reviewsAverage * 20}%`}
      >
        <Row height={height}>
          {[0, 1, 2, 3, 4].map((i) => (
            <Icon name="star" size={size} key={i} />
          ))}
        </Row>
      </YellowStars>
    </GreyStars>
  );
}

DisplayMangaStars.propTypes = {
  reviewsAverage: PropTypes.number.isRequired,
  size: PropTypes.string.isRequired,
  height: PropTypes.string,
};

DisplayMangaStars.defaultProps = {
  height: '',
};
