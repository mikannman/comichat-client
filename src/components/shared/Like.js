import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { useMutation, gql } from '@apollo/client';
import { LIKES, DISLIKES } from '../../graphql/likeQueries';
import usePromptSignIn from '../../hooks/usePromptSignIn';

const answerLikeFragment = gql`
  fragment answerRef on Answer {
    id
    likeAnswers {
      count
    }
    currentUserLikes {
      isExist
    }
  }
`;

const replyLikeFragment = gql`
  fragment replyRef on Reply {
    id
    likeReplies {
      count
    }
    currentUserLikes {
      isExist
    }
  }
`;

const reviewLikeFragment = gql`
  fragment reviewRef on Review {
    id
    likeReviews {
      count
    }
    currentUserLikes {
      isExist
    }
  }
`;

export default function Like({ id, type, isLike }) {
  const promptSignIn = usePromptSignIn();

  const [likes, { loading: likeLoading }] = useMutation(LIKES, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      switch (type) {
        case 'Answer': {
          const answerRef = cache.readFragment({
            id: `${type}:${id}`,
            fragment: answerLikeFragment,
          });

          cache.writeFragment({
            id: `${type}:${id}`,
            fragment: answerLikeFragment,
            data: {
              likeAnswers: {
                count: answerRef.likeAnswers.count + 1,
              },
              currentUserLikes: {
                isExist: true,
              },
            },
          });

          break;
        }
        case 'Reply': {
          const replyRef = cache.readFragment({
            id: `${type}:${id}`,
            fragment: replyLikeFragment,
          });

          cache.writeFragment({
            id: `${type}:${id}`,
            fragment: replyLikeFragment,
            data: {
              likeReplies: {
                count: replyRef.likeReplies.count + 1,
              },
              currentUserLikes: {
                isExist: true,
              },
            },
          });

          break;
        }
        case 'Review': {
          const reviewRef = cache.readFragment({
            id: `${type}:${id}`,
            fragment: reviewLikeFragment,
          });

          cache.writeFragment({
            id: `${type}:${id}`,
            fragment: reviewLikeFragment,
            data: {
              likeReviews: {
                count: reviewRef.likeReviews.count + 1,
              },
              currentUserLikes: {
                isExist: true,
              },
            },
          });

          break;
        }
        default:
          break;
      }
    },
  });

  const [dislikes, { loading: dislikeLoading }] = useMutation(DISLIKES, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache) => {
      switch (type) {
        case 'Answer': {
          const answerRef = cache.readFragment({
            id: `${type}:${id}`,
            fragment: answerLikeFragment,
          });

          cache.writeFragment({
            id: `${type}:${id}`,
            fragment: answerLikeFragment,
            data: {
              likeAnswers: {
                count: answerRef.likeAnswers.count - 1,
              },
              currentUserLikes: {
                isExist: false,
              },
            },
          });

          break;
        }
        case 'Reply': {
          const replyRef = cache.readFragment({
            id: `${type}:${id}`,
            fragment: replyLikeFragment,
          });

          cache.writeFragment({
            id: `${type}:${id}`,
            fragment: replyLikeFragment,
            data: {
              likeReplies: {
                count: replyRef.likeReplies.count - 1,
              },
              currentUserLikes: {
                isExist: false,
              },
            },
          });

          break;
        }
        case 'Review': {
          const reviewRef = cache.readFragment({
            id: `${type}:${id}`,
            fragment: reviewLikeFragment,
          });

          cache.writeFragment({
            id: `${type}:${id}`,
            fragment: reviewLikeFragment,
            data: {
              likeReviews: {
                count: reviewRef.likeReviews.count - 1,
              },
              currentUserLikes: {
                isExist: false,
              },
            },
          });

          break;
        }
        default:
          break;
      }
    },
  });

  const handleClickLike = () => {
    promptSignIn({
      callback: () => {
        if (isLike) {
          dislikes({
            variables: {
              id,
              type,
            },
          });
        } else {
          likes({
            variables: {
              id,
              type,
            },
          });
        }
      },
    });
  };

  return (
    <Icon.Group size="small">
      {
        likeLoading || dislikeLoading
          ? (
            <Icon loading size="mini" name="spinner" />
          )
          : <></>
      }
      <Icon
        name="like"
        color={
          isLike ? 'pink' : 'grey'
        }
        data-testid={`like-${id}`}
        disabled={likeLoading || dislikeLoading}
        onClick={handleClickLike}
      />
    </Icon.Group>
  );
}

Like.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  isLike: PropTypes.bool.isRequired,
};
