import React from 'react';
import { Grid, Label } from 'semantic-ui-react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useHistory, Link } from 'react-router-dom';
import RegisterButtons from '../RegisterButtons';
import DisplayPagination from '../DisplayPagination';
import DisplayMangaStars from '../DisplayMangaStars';
import MangaImage from '../MangaImage';

export default function MangaListArea({
  mangas, totalPage, perPage, paginationVariables, paginationRefetch,
}) {
  const isLogin = useSelector((state) => state.user.isLogin);
  const history = useHistory();

  const handleTitleClick = (e) => {
    history.push(`/manga/${encodeURIComponent(e.currentTarget.dataset.mid)}`);
  };

  return (
    <div>
      {mangas.map((manga) => (
        <Grid key={manga.node.mid}>
          <Grid.Column width={3}>
            <MangaImage
              title={manga.node.title}
              length={60}
              mid={manga.node.mid}
              imageUrl={manga.node.imageUrl}
              affiliateUrl={manga.node.affiliateUrl}
            />
          </Grid.Column>
          <Grid.Column width={8} verticalAlign="middle">
            <Grid.Row
              data-mid={manga.node.mid}
              onClick={handleTitleClick}
              style={{ cursor: 'pointer' }}
            >
              <h3>
                {manga.node.title}
              </h3>
            </Grid.Row>
            <Grid.Row
              style={{
                marginTop: '15px',
                display: 'flex',
                justifyContent: 'flex-start',
              }}
            >
              <DisplayMangaStars
                reviewsAverage={manga.node.reviews.average}
                size="large"
              />
              <h4
                style={{
                  margin: '0',
                  fontSize: '20px',
                }}
              >
                {manga.node.reviews.average}
              </h4>
            </Grid.Row>
            <Grid.Row style={{ marginTop: '15px' }}>
              {manga.node.creators.map((c) => (
                <Link key={c.creator} to={`/mangaList?creator=${encodeURIComponent(c.creator)}`}>
                  <Label>{c.creator}</Label>
                </Link>
              ))}
            </Grid.Row>
            {
              manga.node.datePublished
                && (
                  <Grid.Row style={{ marginTop: '15px' }}>
                    発刊：
                    {manga.node.datePublished}
                  </Grid.Row>
                )
            }
          </Grid.Column>
          <Grid.Column width={5} verticalAlign="middle">
            {
              isLogin
                && (
                  <RegisterButtons
                    mid={manga.node.mid}
                    isReadedManga={manga.node.currentUserReaded.isExist}
                    isWannaReadManga={manga.node.currentUserWannaRead.isExist}
                  />
                )
            }
          </Grid.Column>
        </Grid>
      ))}
      <Grid centered columns={3}>
        <DisplayPagination
          perPage={perPage}
          totalPage={totalPage}
          variables={paginationVariables}
          refetch={paginationRefetch}
        />
      </Grid>
    </div>
  );
}

MangaListArea.propTypes = {
  mangas: PropTypes.arrayOf(
    PropTypes.shape({
      node: PropTypes.shape({
        mid: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        synopsis: PropTypes.string.isRequired,
        datePublished: PropTypes.string.isRequired,
        publisher: PropTypes.string.isRequired,
        imageUrl: PropTypes.string.isRequired,
        affiliateUrl: PropTypes.string.isRequired,
        creators: PropTypes.arrayOf(
          PropTypes.shape({
            creator: PropTypes.string.isRequired,
          }),
        ),
        genres: PropTypes.arrayOf(
          PropTypes.shape({
            genre: PropTypes.string.isRequired,
          }),
        ),
        labels: PropTypes.arrayOf(
          PropTypes.shape({
            label: PropTypes.string.isRequired,
          }),
        ),
        currentUserWannaRead: PropTypes.shape({
          isExist: PropTypes.bool.isRequired,
        }),
        currentUserReaded: PropTypes.shape({
          isExist: PropTypes.bool.isRequired,
        }),
        currentUserRated: PropTypes.shape({
          rate: PropTypes.string.isRequired,
        }),
        reviews: PropTypes.shape({
          avarage: PropTypes.number.isRequired,
        }),
      }),
    }).isRequired,
  ).isRequired,
  totalPage: PropTypes.number.isRequired,
  perPage: PropTypes.number.isRequired,
  paginationVariables: PropTypes.objectOf(PropTypes.string),
  paginationRefetch: PropTypes.func.isRequired,
};

MangaListArea.defaultProps = {
  paginationVariables: {},
};
