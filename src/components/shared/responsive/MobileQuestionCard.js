import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button, Grid, Label, Card, Dropdown,
} from 'semantic-ui-react';
import { useMutation, useQuery, gql } from '@apollo/client';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { StyledTextarea } from '../../../style/globalStyle';
import DefaultAccontImage from '../../image/defaultAccountImage.png';
import { howLongAgo } from '../../../functions/function';
import { CREATE_ANSWER, GET_QUESTION_ANSWERS } from '../../../graphql/answerQueries';
import { DELETE_QUESTION } from '../../../graphql/questionQueries';
import AnswerArea from '../../user/taught/AnswerArea';
import UpdateQuestionForm from '../UpdateQuestionForm';
import StrechContent from '../StrechContent';
import { setConfirmConfig } from '../../../redux/features/confirm/confirmSlice';
import usePromptSignIn from '../../../hooks/usePromptSignIn';

const query = gql`
  query GetQuestionAnswers($id: String, $after: String, $CREATE: Boolean) {
    getQuestionAnswers(id: $id, after: $after, CREATE: $CREATE) {
      edges {
        node {
          id
          content
        }
      }
      totalCount
    }
  }
`;

const userQuestionCardFragment = gql`
  fragment userRef on User {
    id
    answers {
      count
    }
  }
`;

const perPage = 5;

export default function MobileQuestionCard({ question, refetch }) {
  const currentUser = useSelector((state) => state.user.value);
  const currentUserId = currentUser ? currentUser.id : 0;
  const onMyPage = useSelector((state) => state.user.onMyPage);
  const [showAnswerOpen, setShowAnswerOpen] = useState(false);
  const [textareaValue, setTextareaValue] = useState('');
  const [editMode, setEditMode] = useState(false);
  const promptSignIn = usePromptSignIn();
  const dispatch = useDispatch();

  const {
    loading, data, fetchMore,
  } = useQuery(GET_QUESTION_ANSWERS, {
    variables: {
      id: question.id,
      first: perPage,
      after: btoa(String(0)),
    },
  });

  const [createAnswer, { loading: createAnswerLoading }] = useMutation(CREATE_ANSWER, {
    onError: (e) => {
      window.flash(e.message, 'error');
    },
    update: (cache, { data: { createAnswer: ca } }) => {
      setTextareaValue('');

      const answersData = cache.readQuery({
        query,
        variables: {
          id: question.id,
        },
      });

      cache.writeQuery({
        query,
        variables: {
          id: question.id,
          CREATE: true,
        },
        data: {
          getQuestionAnswers: {
            edges: [
              {
                node: ca.answer,
                __typename: 'AnswerEdge',
              },
              ...answersData.getQuestionAnswers.edges,
            ],
            totalCount: answersData.getQuestionAnswers.totalCount + 1,
          },
        },
      });

      if (onMyPage) {
        const currentUserRef = cache.readFragment({
          id: `User:${currentUser.id}`,
          fragment: userQuestionCardFragment,
        });

        cache.writeFragment({
          id: `User:${currentUser.id}`,
          fragment: userQuestionCardFragment,
          data: {
            answers: {
              count: currentUserRef.answers.count + 1,
            },
          },
        });
      }
    },
  });

  const [deleteQuestion] = useMutation(DELETE_QUESTION, {
    onCompleted: () => refetch(),
    onError: (e) => {
      window.flash(e.message, 'error');
    },
  });

  const handleClickEdit = () => {
    setEditMode(true);
  };

  const handleClickDelete = () => {
    dispatch(setConfirmConfig({
      open: true,
      content: '本当に削除しますか',
      onConfirm: () => {
        deleteQuestion({
          variables: {
            id: question.id,
          },
        });
      },
    }));
  };

  const handleClickShowAnswerLabel = () => setShowAnswerOpen((prev) => !prev);

  const handleChangeTextarea = (e) => {
    setTextareaValue(e.target.value);
  };

  const handleClickSubmitButton = () => {
    promptSignIn({
      callback: () => {
        createAnswer({
          variables: {
            questionId: question.id,
            questionUserId: question.askUser.id,
            content: textareaValue,
          },
        });
      },
    });
  };

  return (
    <Card
      fluid
      style={{
        width: '100%',
      }}
    >
      <Card.Content>
        {
          editMode
            ? (
              <UpdateQuestionForm
                id={question.id}
                labelWidth={5}
                selectedGenre={question.genres.map((g) => g.genre)}
                readedMangaMid={question.readedMangaMid}
                readingMangaMid={question.readingMangaMid}
                content={question.content}
                all={question.all}
                askedUser={question.askedUser || {}}
                setEditMode={setEditMode}
              />
            )
            : (
              <Grid style={{ padding: '15px' }}>
                <Grid.Row>
                  {
                    data && data.getQuestionAnswers.totalCount
                      ? (
                        <Label color="red">
                          回答済み
                        </Label>
                      )
                      : (
                        <Label color="grey">
                          未回答
                        </Label>
                      )
                  }
                  {howLongAgo(question.updatedAt)}
                  {
                    currentUserId === question.askUser.id
                      && (
                        <Dropdown
                          icon="ellipsis vertical"
                          style={{ alignSelf: 'center', color: 'grey', margin: '0 0 0 10px' }}
                        >
                          <Dropdown.Menu>
                            <Dropdown.Item
                              onClick={handleClickEdit}
                              data-id={question.id}
                            >
                              編集
                            </Dropdown.Item>
                            <Dropdown.Item
                              onClick={handleClickDelete}
                            >
                              削除
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      )
                  }
                </Grid.Row>
                <Grid.Row
                  centered
                  columns={3}
                  style={{
                    padding: '3px 0 10px 0',
                  }}
                >
                  <Grid.Column width={7}>
                    <Link to={`/user/${question.askUser.id}`}>
                      <Label image size="mini">
                        <img src={question.askUser.imageURL || DefaultAccontImage} alt="ユーザー画像" />
                        {question.askUser.name}
                      </Label>
                    </Link>
                  </Grid.Column>
                  <Grid.Column width={2}>
                    to:
                  </Grid.Column>
                  <Grid.Column width={7}>
                    {
                      question.askedUser
                        && (
                          <Link to={`/user/${question.askedUser.id}`}>
                            <Label image size="mini">
                              <img src={question.askedUser.imageURL || DefaultAccontImage} alt="ユーザー画像" />
                              {question.askedUser.name}
                            </Label>
                          </Link>
                        )
                    }
                    {
                      question.all
                        && (
                          <Label size="mini">
                            all
                          </Label>
                        )
                    }
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row
                  centered
                  style={{
                    padding: '10px 0 0 0',
                  }}
                >
                  探しているジャンル
                </Grid.Row>
                <Grid.Row
                  centered
                  style={{
                    padding: '3px',
                  }}
                >
                  {
                    question.genres.length
                      ? (
                        question.genres.map((g) => (
                          <Label key={g.genre}>{g.genre}</Label>
                        ))
                      )
                      : <Label>選択されていません</Label>
                  }
                </Grid.Row>
                <Grid.Row
                  centered
                  style={{
                    padding: '10px 0 0 0',
                  }}
                >
                  今読んでいる漫画
                </Grid.Row>
                <Grid.Row
                  centered
                  style={{
                    padding: '3px',
                  }}
                >
                  {
                    question.readingMangaMid
                      ? (
                        question.readingMangaMid.split(',').map((mid) => (
                          mid && <Label key={mid}>{mid}</Label>
                        ))
                      )
                      : <Label>選択されていません</Label>
                  }
                </Grid.Row>
                <Grid.Row
                  centered
                  style={{
                    padding: '10px 0 0 0',
                  }}
                >
                  今まで読んできた漫画
                </Grid.Row>
                <Grid.Row
                  centered
                  style={{
                    padding: '3px',
                  }}
                >
                  {
                    question.readedMangaMid
                      ? (
                        question.readedMangaMid.split(',').map((mid) => (
                          mid && <Label key={mid}>{mid}</Label>
                        ))
                      )
                      : <Label>選択されていません</Label>
                  }
                </Grid.Row>
                <Grid.Row
                  columns={1}
                  style={{
                    margin: '15px',
                  }}
                >
                  <Grid.Column width={16}>
                    <StrechContent
                      content={question.content}
                      rowMargin="0"
                    />
                  </Grid.Column>
                </Grid.Row>
                {
                  data && Boolean(data.getQuestionAnswers.totalCount)
                    && (
                      <Grid.Row
                        style={{
                          display: 'flex',
                          justifyContent: 'flex-end',
                        }}
                      >
                        <span>
                          回答者:
                          {
                            data.getQuestionAnswers.edges.slice(0, 1).map((edge) => (
                              <Label image key={edge.node.id}>
                                <img src={edge.node.user.imageURL || DefaultAccontImage} alt="ユーザー画像" />
                                {edge.node.user.name}
                              </Label>
                            ))
                          }
                          {
                            data.getQuestionAnswers.totalCount > 2
                              ? '他...'
                              : ''
                          }
                        </span>
                        <span>
                          回答数:
                          {data.getQuestionAnswers.totalCount}
                        </span>
                      </Grid.Row>
                    )
                }
                <hr
                  style={{
                    margin: '15px 0',
                    width: '100%',
                  }}
                />
                {
                  question.askUser.id !== currentUserId
                    && (
                      <>
                        <Grid.Row>
                          <StyledTextarea
                            placeholder="〇〇な漫画がおすすめですよ！！"
                            value={textareaValue}
                            onChange={handleChangeTextarea}
                            border="solid 1px black"
                            height="70px"
                            padding="10px"
                          />
                        </Grid.Row>
                        <Grid.Row
                          style={{
                            display: 'flex',
                            justifyContent: 'flex-end',
                          }}
                        >
                          <Button
                            onClick={handleClickSubmitButton}
                            color="teal"
                            loading={createAnswerLoading}
                          >
                            送信
                          </Button>
                        </Grid.Row>
                      </>
                    )
                }
                {
                  data && Boolean(data.getQuestionAnswers.totalCount)
                    && (
                      <Grid.Row centered>
                        <Label
                          as="a"
                          color="teal"
                          onClick={handleClickShowAnswerLabel}
                        >
                          {showAnswerOpen ? '閉じる' : '回答を見る'}
                        </Label>
                      </Grid.Row>
                    )
                }
                {
                  data && data.getQuestionAnswers.totalCount
                    ? (
                      showAnswerOpen
                        && (
                          <Grid.Row>
                            <Grid.Column>
                              <AnswerArea
                                getQuestionAnswers={data.getQuestionAnswers}
                                setShowAnswerOpen={setShowAnswerOpen}
                                loading={loading}
                                fetchMore={fetchMore}
                                questionId={question.id}
                              />
                            </Grid.Column>
                          </Grid.Row>
                        )
                    )
                    : (
                      <Grid.Row>まだ回答がありません</Grid.Row>
                    )
                }
              </Grid>
            )
        }
      </Card.Content>
    </Card>
  );
}

MobileQuestionCard.propTypes = {
  refetch: PropTypes.func.isRequired,
  question: PropTypes.shape({
    id: PropTypes.string.isRequired,
    genres: PropTypes.arrayOf(
      PropTypes.shape({
        genre: PropTypes.string.isRequired,
      }),
    ).isRequired,
    readingMangaMid: PropTypes.string,
    readedMangaMid: PropTypes.string,
    content: PropTypes.string.isRequired,
    all: PropTypes.bool,
    updatedAt: PropTypes.string.isRequired,
    askUser: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
    }).isRequired,
    askedUser: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired,
    }),
  }).isRequired,
};
