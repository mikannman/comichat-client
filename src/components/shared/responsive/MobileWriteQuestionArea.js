import React from 'react';
import PropTypes from 'prop-types';
import {
  Button, Grid, Label, Checkbox,
} from 'semantic-ui-react';
import { useSelector, useDispatch } from 'react-redux';
import { StyledTextarea } from '../../../style/globalStyle';
import SearchMangaInput from '../SearchMangaInput';
import SearchUserInput from '../SearchUserInput';
import { setInputData, setIsChanged } from '../../../redux/features/questionForm/questionFormSlice';

export default function MobileWriteQuestionArea({
  askedUserId, handleClickSubmitButton, submitButtonText,
  genreState, setGenreState, updateMode, handleClickCancel,
}) {
  const inputData = useSelector((state) => state.questionForm.inputData);
  const isEmptyRequiredParams = useSelector((state) => {
    const v = state.questionForm.inputData;
    return (!v.checkedAll && !v.askedUser.name && !askedUserId) || !v.textareaValue;
  });
  const dispatch = useDispatch();

  const handleClickGenre = (e) => {
    dispatch(setIsChanged(true));
    setGenreState((prev) => (
      prev.map((state) => {
        if (state.genre === e.target.dataset.genre) {
          return { ...state, choiced: !state.choiced };
        }
        return state;
      })
    ));
  };

  const handleChangeTextarea = (e) => dispatch(setInputData({ textareaValue: e.target.value }));

  const handleChangeCheckbox = () => dispatch(setInputData({ checkedAll: !inputData.checkedAll }));

  return (
    <Grid>
      <Grid.Row
        centered
        style={{
          padding: '15px 0 0 0',
        }}
      >
        <h4>聞きたいジャンル</h4>
      </Grid.Row>
      <Grid.Row centered>
        {
          genreState.map((state) => (
            <Label
              as="a"
              onClick={handleClickGenre}
              color={state.choiced && 'teal'}
              data-genre={state.genre}
              key={state.genre}
              style={{
                marginTop: '5px',
              }}
            >
              {state.genre}
            </Label>
          ))
        }
      </Grid.Row>
      <Grid.Row
        centered
        style={{
          padding: '10px 0 0 0',
        }}
      >
        <h4>今読んでるマンガ</h4>
      </Grid.Row>
      <Grid.Row
        centered
        columns={1}
        style={{
          padding: '0 0 10px 0',
        }}
      >
        <Grid.Column width={16} textAlign="center">
          <SearchMangaInput
            type="readingManga"
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row
        centered
        style={{
          padding: '10px 0 0 0',
        }}
      >
        <h4>今まで読んできたマンガ</h4>
      </Grid.Row>
      <Grid.Row
        centered
        style={{
          padding: '0 0 10px 0',
        }}
      >
        <Grid.Column width={16} textAlign="center">
          <SearchMangaInput
            type="readedManga"
          />
        </Grid.Column>
      </Grid.Row>
      {
        !askedUserId
          && (
            <>
              <Grid.Row
                centered
                style={{
                  padding: '10px 0 0 0',
                }}
              >
                <h4>誰かに聞く？</h4>
              </Grid.Row>
              <Grid.Row
                centered
                style={{
                  padding: '0 0 10px 0',
                }}
              >
                <Grid.Column width={16} textAlign="center">
                  <SearchUserInput />
                </Grid.Column>
              </Grid.Row>
            </>
          )
      }
      <Grid.Row centered>
        <StyledTextarea
          value={inputData.textareaValue}
          onChange={handleChangeTextarea}
          placeholder="○○なおすすめのマンガについて教えてください！！"
          border="solid 1px black"
          height="100px"
          width="90%"
          padding="10px"
        />
      </Grid.Row>
      <Grid.Row
        centered
        verticalAlign="middle"
      >
        <Grid.Column width={8}>
          <Checkbox
            label="みんなにも聞く"
            checked={inputData.checkedAll}
            onChange={handleChangeCheckbox}
          />
        </Grid.Column>
        <Grid.Column
          width={8}
          style={{
            paddingLeft: '0',
          }}
        >
          <Button
            color="teal"
            onClick={handleClickSubmitButton}
            disabled={isEmptyRequiredParams}
            size="mini"
          >
            {submitButtonText}
          </Button>
        </Grid.Column>
      </Grid.Row>
      {
        updateMode
          && (
            <Grid.Row
              centered
              style={{
                paddingTop: '0',
              }}
            >
              <Button
                onClick={handleClickCancel}
              >
                キャンセル
              </Button>
            </Grid.Row>
          )
      }
    </Grid>
  );
}

MobileWriteQuestionArea.propTypes = {
  askedUserId: PropTypes.string,
  handleClickSubmitButton: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  genreState: PropTypes.arrayOf(PropTypes.shape({
    genre: PropTypes.string.isRequired,
    choiced: PropTypes.bool.isRequired,
  })).isRequired,
  setGenreState: PropTypes.func.isRequired,
  updateMode: PropTypes.bool,
  handleClickCancel: PropTypes.func,
};

MobileWriteQuestionArea.defaultProps = {
  askedUserId: '',
  updateMode: false,
  handleClickCancel: () => null,
};
