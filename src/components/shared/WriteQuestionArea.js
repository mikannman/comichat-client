import React from 'react';
import PropTypes from 'prop-types';
import {
  Button, Grid, Label, Checkbox,
} from 'semantic-ui-react';
import { useSelector, useDispatch } from 'react-redux';
import { StyledTextarea } from '../../style/globalStyle';
import SearchMangaInput from './SearchMangaInput';
import SearchUserInput from './SearchUserInput';
import { setInputData, setIsChanged } from '../../redux/features/questionForm/questionFormSlice';

export default function WriteQuestionArea({
  labelWidth, askedUserId, handleClickSubmitButton, submitButtonText,
  genreState, setGenreState, updateMode, handleClickCancel,
}) {
  const inputData = useSelector((state) => state.questionForm.inputData);
  const isEmptyRequiredParams = useSelector((state) => {
    const v = state.questionForm.inputData;
    return (!v.checkedAll && !v.askedUser.name && !askedUserId) || !v.textareaValue;
  });
  const dispatch = useDispatch();

  const handleClickGenre = (e) => {
    dispatch(setIsChanged(true));
    setGenreState((prev) => (
      prev.map((state) => {
        if (state.genre === e.target.dataset.genre) {
          return { ...state, choiced: !state.choiced };
        }
        return state;
      })
    ));
  };

  const handleChangeTextarea = (e) => dispatch(setInputData({ textareaValue: e.target.value }));

  const handleChangeCheckbox = () => dispatch(setInputData({ checkedAll: !inputData.checkedAll }));

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column width={labelWidth} textAlign="right">
          <h4>聞きたいジャンル:</h4>
        </Grid.Column>
        <Grid.Column width={16 - labelWidth}>
          {
            genreState.map((state) => (
              <Label
                as="a"
                onClick={handleClickGenre}
                color={state.choiced && 'teal'}
                data-genre={state.genre}
                key={state.genre}
                style={{
                  marginTop: '5px',
                }}
              >
                {state.genre}
              </Label>
            ))
          }
        </Grid.Column>
      </Grid.Row>
      <Grid.Row verticalAlign="middle">
        <Grid.Column width={labelWidth} textAlign="right">
          <h4>今読んでるマンガ:</h4>
        </Grid.Column>
        <Grid.Column width={16 - labelWidth}>
          <SearchMangaInput
            type="readingManga"
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row verticalAlign="middle">
        <Grid.Column width={labelWidth} textAlign="right">
          <h4>今まで読んできたマンガ:</h4>
        </Grid.Column>
        <Grid.Column width={16 - labelWidth}>
          <SearchMangaInput
            type="readedManga"
          />
        </Grid.Column>
      </Grid.Row>
      {
        !askedUserId
          && (
            <Grid.Row verticalAlign="middle">
              <Grid.Column width={labelWidth} textAlign="right">
                <h4>誰かに聞く？:</h4>
              </Grid.Column>
              <Grid.Column width={16 - labelWidth}>
                <SearchUserInput />
              </Grid.Column>
            </Grid.Row>
          )
      }
      <Grid.Row centered>
        <StyledTextarea
          value={inputData.textareaValue}
          onChange={handleChangeTextarea}
          placeholder="○○なおすすめのマンガについて教えてください！！"
          border="solid 1px black"
          height="100px"
          width="60%"
          padding="10px"
        />
      </Grid.Row>
      <Grid.Row
        centered
        verticalAlign="middle"
      >
        <Grid.Column width={5}>
          <Checkbox
            label="みんなにも聞く"
            checked={inputData.checkedAll}
            onChange={handleChangeCheckbox}
          />
        </Grid.Column>
        <Grid.Column width={5}>
          <Button
            color="teal"
            onClick={handleClickSubmitButton}
            disabled={isEmptyRequiredParams}
            style={{
              padding: '8px',
            }}
          >
            {submitButtonText}
          </Button>
        </Grid.Column>
        {
          updateMode
            && (
              <Grid.Column
                width="5"
                style={{
                  padding: '0',
                }}
              >
                <Button
                  onClick={handleClickCancel}
                >
                  キャンセル
                </Button>
              </Grid.Column>
            )
        }
      </Grid.Row>
    </Grid>
  );
}

WriteQuestionArea.propTypes = {
  labelWidth: PropTypes.number.isRequired,
  askedUserId: PropTypes.string,
  handleClickSubmitButton: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  genreState: PropTypes.arrayOf(PropTypes.shape({
    genre: PropTypes.string.isRequired,
    choiced: PropTypes.bool.isRequired,
  })).isRequired,
  setGenreState: PropTypes.func.isRequired,
  updateMode: PropTypes.bool,
  handleClickCancel: PropTypes.func,
};

WriteQuestionArea.defaultProps = {
  askedUserId: '',
  updateMode: false,
  handleClickCancel: () => null,
};
