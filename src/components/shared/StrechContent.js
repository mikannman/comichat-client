import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import { Grid, Label } from 'semantic-ui-react';

const duration = 200;

const defaultSyle = {
  transition: `all ${duration}ms ease`,
  whiteSpace: 'pre-wrap',
  textAlign: 'left',
  maxHeight: '57px',
  overflow: 'hidden',
};

export default function StrechContent({ content, rowMargin }) {
  const [readMore, setReadMore] = useState({ height: '', state: false });
  const [readMoreOpen, setReadMoreOpen] = useState({ transition: false, label: false });
  const contentElem = useRef(null);

  useEffect(() => {
    if (contentElem.current.getBoundingClientRect().height > 57) {
      setReadMore({
        height: `${contentElem.current.getBoundingClientRect().height}px`,
        state: true,
      });
    }
  }, []);

  const transitionStyles = {
    entering: {
      maxHeight: '57px',
    },
    entered: {
      maxHeight: readMore.height,
    },
    exiting: {
      maxHeight: readMore.height,
    },
    exited: {
      maxHeight: '57px',
    },
  };

  const handleClickReadMoreLabel = () => {
    setReadMoreOpen((prev) => ({ ...prev, transition: !prev.transition }));
    setTimeout(() => {
      setReadMoreOpen((prev) => ({ ...prev, label: !prev.label }));
    }, 400);
  };

  return (
    <Grid>
      <Grid.Row
        style={{
          margin: rowMargin,
        }}
      >
        <Transition in={readMoreOpen.transition} timeout={duration}>
          {(state) => (
            <div
              style={{
                ...defaultSyle,
                ...transitionStyles[state],
              }}
            >
              <div
                ref={contentElem}
              >
                {content}
              </div>
            </div>
          )}
        </Transition>
      </Grid.Row>
      {
        readMore.state
          && (
            <Grid.Row
              centered
              style={{
                paddingTop: '0',
              }}
            >
              <Label
                onClick={handleClickReadMoreLabel}
                as="a"
              >
                {readMoreOpen.label ? '戻す' : '続きを読む'}
              </Label>
            </Grid.Row>
          )
      }
    </Grid>
  );
}

StrechContent.propTypes = {
  content: PropTypes.string.isRequired,
  rowMargin: PropTypes.string,
};

StrechContent.defaultProps = {
  rowMargin: '15px',
};
