import React, {
  useState, useEffect, useRef,
} from 'react';
import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';

export default function MangaTitle({ title, fontSize, height }) {
  const [isOver, setIsOver] = useState(false);
  const parentElem = useRef();
  const childElem = useRef();
  const isLargeDesktop = useMediaQuery({ minWidth: 1441 });

  useEffect(() => {
    const pw = parentElem.current.getBoundingClientRect().width;
    const cw = childElem.current.getBoundingClientRect().width;

    if (pw === cw) {
      setIsOver(true);
    }
  }, []);

  return (
    <div
      style={{
        padding: '0 5px',
      }}
    >
      <div
        ref={parentElem}
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
        }}
      >
        <h5
          ref={childElem}
          style={{
            margin: '3px 0',
            height: isLargeDesktop ? '16px' : height,
            overflow: 'hidden',
            fontSize,
          }}
        >
          {title}
        </h5>
        {
          isOver
            && (
              <h5
                style={{
                  margin: '3px 0 3px -1px',
                  fontSize,
                }}
              >
                …
              </h5>
            )
        }
      </div>
    </div>
  );
}

MangaTitle.propTypes = {
  title: PropTypes.string.isRequired,
  fontSize: PropTypes.string,
  height: PropTypes.string,
};

MangaTitle.defaultProps = {
  fontSize: '',
  height: '18px',
};
