import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { useMediaQuery } from 'react-responsive';
import PropTypes from 'prop-types';
import {
  Grid, Card, Icon, Image,
} from 'semantic-ui-react';
import { GET_SMALL_ALL_QUESTIONS_BY_CONDITION } from '../../graphql/questionQueries';
import DefaultAccountImage from '../image/defaultAccountImage.png';

export default function AllQuestionsByCondition({
  condition, headerText, path, fluid,
}) {
  const history = useHistory();
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 991 });

  const { loading, data, client } = useQuery(GET_SMALL_ALL_QUESTIONS_BY_CONDITION, {
    variables: {
      condition,
      first: 5,
    },
    onCompleted: () => console.log(client.cache.data),
  });

  const handleClickQuestion = (e) => history.push(`/questions/show/${e.currentTarget.dataset.id}`);

  return (
    <Card fluid={fluid}>
      <Card.Content>
        {
          isTablet
            ? (
              <Card.Header>
                <div
                  style={{
                    fontSize: '14px',
                  }}
                >
                  {headerText}
                </div>
                <Link to={path}>
                  <small
                    style={{
                      marginLeft: '3px',
                      fontSize: '8px',
                    }}
                  >
                    全部見る
                  </small>
                </Link>
              </Card.Header>
            )
            : (
              <Card.Header>
                {headerText}
                <Link to={path}>
                  <small
                    style={{
                      marginLeft: '3px',
                      fontSize: '8px',
                    }}
                  >
                    全部見る
                  </small>
                </Link>
              </Card.Header>
            )
        }
      </Card.Content>
      <Card.Content>
        <Grid>
          {
            loading
              ? <Icon name="spinner" loading />
              : (
                data && data.getAllQuestionsByCondition.edges.map((edge) => (
                  <Grid.Row
                    columns={3}
                    key={edge.node.id}
                    data-id={edge.node.id}
                    onClick={handleClickQuestion}
                    style={{ cursor: 'pointer' }}
                  >
                    <Grid.Column
                      width={3}
                      verticalAlign="middle"
                      style={{
                        padding: '0 3px',
                      }}
                    >
                      <Image
                        circular
                        size="tiny"
                        src={edge.node.askUser.imageURL || DefaultAccountImage}
                      />
                    </Grid.Column>
                    <Grid.Column
                      width={5}
                      verticalAlign="middle"
                      style={{
                        padding: '0',
                      }}
                    >
                      <h4 style={{ margin: '0' }}>{edge.node.askUser.name}</h4>
                      {
                        !edge.node.askUser.guest
                          && (
                            <small
                              style={{
                                wordBreak: 'break-all',
                              }}
                            >
                              {edge.node.askUser.userId}
                            </small>
                          )
                      }
                    </Grid.Column>
                    <Grid.Column
                      width={8}
                      style={{
                        padding: '0 3px',
                        height: '56px',
                        overflow: 'hidden',
                        textAlign: 'left',
                      }}
                    >
                      {edge.node.content}
                    </Grid.Column>
                  </Grid.Row>
                ))
              )
          }
        </Grid>
      </Card.Content>
    </Card>
  );
}

AllQuestionsByCondition.propTypes = {
  condition: PropTypes.string.isRequired,
  headerText: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  fluid: PropTypes.bool,
};

AllQuestionsByCondition.defaultProps = {
  fluid: false,
};
