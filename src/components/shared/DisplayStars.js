import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';

export default function DisplayStars({ star, size }) {
  return (
    [0, 1, 2, 3, 4].map((i) => (
      <Icon
        name="star"
        style={{
          color: i < star ? '#fbbd08' : 'grey',
        }}
        key={i}
        size={size}
      />
    ))
  );
}

DisplayStars.propTypes = {
  star: PropTypes.number.isRequired,
  size: PropTypes.string,
};

DisplayStars.defaultProps = {
  size: '',
};
