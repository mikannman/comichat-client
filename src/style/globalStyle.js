import styled from 'styled-components';

export const DivSpacer = styled.div`
  margin-top: ${(props) => props.marginTop};
  margin-left: ${(props) => props.marginLeft};
  margin-right: ${(props) => props.marginRight};
  margin-bottom: ${(props) => props.marginBottom};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
`;

export const Wrapper = styled.div`
  padding: ${(props) => props.padding};
`;

export const StyledInput = styled.input`
  outline: ${(props) => props.outline || 'none'};
  border: ${(props) => props.border || 'none'};
  width: ${(props) => props.width || '100%'};
  border-radius: ${(props) => props.borderRadius};
  height: ${(props) => props.height};
  z-index: ${(props) => props.zIndex};
`;

export const StyledTextarea = styled.textarea`
  outline: ${(props) => props.outline || 'none'};
  border: ${(props) => props.border || 'none'};
  border-radius: ${(props) => props.borderRadius || '5px'};
  width: ${(props) => props.width || '100%'};
  height: ${(props) => props.height};
  padding: ${(props) => props.padding};
`;

export const ClickableText = styled.span`
  color: ${(props) => props.color || 'blue'};
  cursor: pointer;
  margin-left: ${(props) => props.marginLeft || '5px'};
  position: ${(props) => props.position};
  top: ${(props) => props.top};
  right: ${(props) => props.right};
  bottom: ${(props) => props.bottom};
  left: ${(props) => props.left};
`;

export const Center = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: ${(props) => props.flexDirection};
  margin-top: ${(props) => props.marginTop || '0px'};
  margin-left: ${(props) => props.marginLeft || '0px'};
  margin-bottom: ${(props) => props.marginBottom};
  color: ${(props) => props.color};
  flex-direction: ${(props) => props.flexDirection};
  height: ${(props) => props.height};
  width: ${(props) => props.width};
  font-size: ${(props) => props.fontSize};
  font-weight: ${(props) => props.fontWeight};
  font-family: ${(props) => props.fontFamily};
  background-color: ${(props) => props.backgroundColor};
  padding: ${(props) => props.padding};
  text-align: ${(props) => props.textAlign};
`;

export const Row = styled.div`
  display: flex;
  justify-content: ${(props) => props.justifyContent};
  height: ${(props) => props.height};
`;

export const RowItem = styled.div`
  display: flex;
  align-self: ${(props) => props.alignSelf || 'center'};
  font-weight: ${(props) => props.fontWeight};
  margin: ${(props) => props.margin};
`;

export const ParentWrapper = styled.div`
  display: ${(props) => props.display || 'flex'};
  justify-content: ${(props) => props.justifyContent};
  height: ${(props) => props.height};
  margin-top: ${(props) => props.marginTop};
`;

export const ChildWrapper = styled.div`
  align-self: ${(props) => props.alignSelf};
  flex-basis: ${(props) => props.flexBasis};
`;

export const SpanSpacer = styled.span`
  margin-left: ${(props) => props.marginLeft};
  margin-right: ${(props) => props.marginRight};
  color: ${(props) => props.color};
  font-size: ${(props) => props.fontSize};
`;

export const CircleImage = styled.img`
  border-radius: 50%;
`;

export const Warning = styled.div`
  color: red;
  margin-top: ${(props) => props.marginTop || '10px'};
  margin-bottom: ${(props) => props.marginBottom || '0px'};
  margin: ${(props) => props.margin};
  font-size: ${(props) => props.fontSize}
`;

export const Message = styled.p`
  color: ${(props) => props.color || 'white'};
  align-self: ${(props) => props.alignSelf};
`;

export const LabelText = styled.div`
  margin-right: ${(props) => props.marginRight};
  font-size: ${(props) => props.fontSize};
  font-weight: ${(props) => props.fontWeight};
  width: ${(props) => props.width};
  text-align: ${(props) => props.textAlign || 'right'}
`;

export const Border = styled.div`
  border: ${(props) => props.border || '1px solid black'};
  width: ${(props) => props.width};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  border-radius: ${(props) => props.borderRadius || '5px'};
  box-shadow: ${(props) => props.boxShadow};
  background-color: ${(props) => props.backgroundColor};
`;

export const InputWrapper = styled.div`
  width: ${(props) => props.width};
  &:after {
    display: block;
    width: 100%;
    height: 4px;
    margin-top: -1px;
    content: '';
    border-width: 0 1px 1px 1px;
    border-style: solid;
    border-color: teal;
  }
`;

export const TriangleBorder = styled.div`
  padding: ${(props) => props.padding || '30px'};
  position: relative;
  background-color: ${(props) => props.backGroundColor || 'teal'};
  &:before {
    position: absolute;
    content: '';
    bottom: 0;
    left: 0;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: ${(props) => props.borderWidth || '0 0 130px 500px'};
    border-color: ${(props) => props.borderColor || 'transparent transparent white transparent'};
  }
`;

export const Supplement = styled.small`
  color: grey;
  align-self: ${(props) => props.alignSelf};
  margin-left: ${(props) => props.marginLeft};
`;

export const Paragraph = styled.div`
  white-space: pre-wrap;
  text-align: ${(props) => props.textAlign || 'left'};
  margin: ${(props) => props.margin || '0 30px'};
  overflow: ${(props) => props.overflow};
  height: ${(props) => props.height};
  width: ${(props) => props.width};
  font-size: ${(props) => props.fontSize};
`;

export const UserId = styled.div`
  font-size: 10px;
  color: grey;
`;
