import { useDispatch, useSelector } from 'react-redux';
import { setState, setWarningText } from '../redux/features/sessionModal/sessionModalSlice';

export default function usePromptSignIn() {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.value);

  const promptSignIn = ({
    warningText = 'この操作を続行するにはログインが必要です',
    callback,
  }) => {
    if (currentUser) {
      callback();
    } else {
      dispatch(setState({ open: true, mode: 'ログイン' }));
      dispatch(setWarningText(warningText));
    }
  };

  return promptSignIn;
}
