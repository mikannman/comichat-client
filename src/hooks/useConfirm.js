import { useDispatch } from 'react-redux';
import { setConfirmConfig } from '../redux/features/confirm/confirmSlice';

const useConfirm = () => {
  const dispatch = useDispatch();

  const confirm = ({
    content = '本当に削除しますか',
    onConfirm,
    onCancel = () => null,
  }) => {
    dispatch(setConfirmConfig({
      open: true,
      content,
      onConfirm,
      onCancel,
    }));
  };

  return confirm;
};

export default useConfirm;
