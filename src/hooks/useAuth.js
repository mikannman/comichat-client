import { useState } from 'react';
import { useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../redux/features/user/userSlice';
import { VERIFY_EMAIL, SIGN_UP_OR_IN } from '../graphql/authQueries';
import { allReset, setError, setCommunicationError } from '../redux/features/sessionModal/sessionModalSlice';
import { setToken } from '../redux/features/authToken/authTokenSlice';
import firebase from '../firebase/config';

export const useAuth = ({
  onComfirmedEmail = null,
  type = '',
  onCompletedCheck = null,
  onFailedCheck = null,
}) => {
  const dispatch = useDispatch();
  const emailValidation = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  const passwordValidation = /^\w{6,20}$/;
  const modalInput = useSelector((state) => state.sessionModal.input);
  const [loading, setLoading] = useState(false);

  const [signUpOrIn] = useMutation(SIGN_UP_OR_IN, {
    onCompleted: (data) => {
      setLoading(false);
      dispatch(setUser(data.signUpOrIn.user));
      dispatch(allReset());
    },
    onError: (error) => {
      setLoading(false);
      dispatch(setCommunicationError([error.message]));
      if (type === 'signUp' || type === 'guest') {
        firebase.auth().currentUser.delete();
      } else if (type === 'signIn') {
        firebase.auth().signOut();
      }
    },
  });

  const [verifyEmail] = useMutation(VERIFY_EMAIL, {
    onCompleted: (result) => {
      if (type === 'signUp') {
        if (result.verifyEmail.registered) {
          dispatch(setError({ email: ['このメールアドレスはすでに登録されています'] }));
          setLoading(false);
        } else {
          firebase.auth().createUserWithEmailAndPassword(modalInput.email, modalInput.password)
            .then(async (firebaseResult) => {
              await firebaseResult.user.getIdToken(/* forceRefresh */ true)
                .then((idToken) => {
                  dispatch(setToken(idToken));
                });
              signUpOrIn({
                variables: {
                  name: modalInput.name,
                  email: modalInput.email,
                  userId: modalInput.userID,
                  imageURL: modalInput.imageURL,
                  provider: firebaseResult.user.providerData[0].providerId,
                },
              });
            })
            .catch((error) => {
              setLoading(false);
              if (error.code === 'auth/email-already-in-use') {
                dispatch(setCommunicationError(['このメールアドレスはすでに登録されています']));
              }
            });
        }
      } else if (type === 'signIn') {
        if (result.verifyEmail.registered) {
          firebase.auth().signInWithEmailAndPassword(modalInput.email, modalInput.password)
            .then(async (firebaseResult) => {
              await firebaseResult.user.getIdToken(/* forceRefresh */ true)
                .then((idToken) => {
                  dispatch(setToken(idToken));
                });
              signUpOrIn({
                variables: {
                  email: modalInput.email,
                  provider: firebaseResult.user.providerData[0].providerId,
                },
              });
            })
            .catch((error) => {
              setLoading(false);
              if (error.code === 'auth/wrong-password') {
                dispatch(setCommunicationError(['入力されたメールアドレスかパスワードが間違っています']));
              } else if (error.code === 'auth/user-disabled') {
                dispatch(setCommunicationError(['アカウントが無効になっています']));
              } else if (error.code === 'auth/user-not-found') {
                dispatch(setCommunicationError(['アカウントが登録されていません']));
              }
            });
        } else {
          setLoading(false);
          dispatch(setError({ email: ['このメールアドレスは登録されていません'] }));
        }
      } else if (result.verifyEmail.registered) {
        setLoading(false);
        dispatch(setError({ email: ['このメールアドレスはすでに登録されています'] }));
      } else {
        setLoading(false);
        onComfirmedEmail(result);
      }
    },
  });

  const validationCheckAndAuth = () => {
    const nameError = [];
    const emailError = [];
    const passwordError = [];
    const passwordConfirmationError = [];

    if (modalInput.name) {
      if (modalInput.name.length > 200) {
        nameError.push('名前が長すぎます');
      }
    }

    if (modalInput.email) {
      if (modalInput.email.length > 254) {
        emailError.push('メールアドレスが長すぎます');
      } else if (!emailValidation.test(modalInput.email)) {
        emailError.push('メールアドレスの形式が正しくありません');
      }
    }

    if (modalInput.password) {
      if (!passwordValidation.test(modalInput.password)) {
        passwordError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
      }
    }

    if (modalInput.passwordConfirmation) {
      if (modalInput.password !== modalInput.passwordConfirmation) {
        passwordError.push('パスワードと確認用パスワードが一致しません');
      }

      if (!passwordValidation.test(modalInput.passwordConfirmation)) {
        passwordConfirmationError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
      }
    }

    if (
      !nameError.length
      && !emailError.length
      && !passwordError.length
      && !passwordConfirmationError.length
    ) {
      setLoading(true);
      verifyEmail({
        variables: {
          email: modalInput.email,
        },
      });
    } else {
      dispatch(setError({
        name: nameError,
        email: emailError,
        password: passwordError,
        passwordConfirmation: passwordConfirmationError,
      }));
    }
  };

  const validationCheck = () => {
    const nameError = [];
    const emailError = [];
    const passwordError = [];
    const passwordConfirmationError = [];

    if (modalInput.name) {
      if (modalInput.name.length > 200) {
        nameError.push('名前が長すぎます');
      }
    }

    if (modalInput.email) {
      if (modalInput.email.length > 254) {
        emailError.push('メールアドレスが長すぎます');
      } else if (!emailValidation.test(modalInput.email)) {
        emailError.push('メールアドレスの形式が正しくありません');
      }
    }

    if (modalInput.password) {
      if (!passwordValidation.test(modalInput.password)) {
        passwordError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
      }
    }

    if (modalInput.passwordConfirmation) {
      if (modalInput.password !== modalInput.passwordConfirmation) {
        passwordError.push('パスワードと確認用パスワードが一致しません');
      }

      if (!passwordValidation.test(modalInput.passwordConfirmation)) {
        passwordConfirmationError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
      }
    }

    if (
      !nameError.length
      && !emailError.length
      && !passwordError.length
      && !passwordConfirmationError.length
    ) {
      onCompletedCheck();
    } else {
      dispatch(setError({
        name: nameError,
        email: emailError,
        password: passwordError,
        passwordConfirmation: passwordConfirmationError,
      }));
      onFailedCheck();
    }
  };

  const emailAndPasswordSignIn = ({
    email = '',
    password = '',
    setErrors = null,
    callback = null,
  }) => {
    setLoading(true);
    const emailError = [];
    const passwordError = [];

    if (email.length > 254) {
      emailError.push('メールアドレスが長すぎます');
    } else if (!emailValidation.test(email)) {
      emailError.push('メールアドレスの形式が正しくありません');
    }

    if (!passwordValidation.test(password)) {
      passwordError.push('パスワードは6文字以上20文字以内の英数字で入力してください');
    }

    if (!emailError.length && !passwordError.length) {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(async (firebaseResult) => {
          await firebaseResult.user.getIdToken(/* forceRefresh */ true)
            .then((idToken) => {
              dispatch(setToken(idToken));
            });
          await callback();
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
          if (error.code === 'auth/wrong-password') {
            setErrors(({
              email: [],
              password: [],
              communication: ['入力されたメールアドレスかパスワードが間違っています'],
            }));
          } else if (error.code === 'auth/user-disabled') {
            setErrors(({
              email: [],
              password: [],
              communication: ['アカウントが無効になっています'],
            }));
          } else if (error.code === 'auth/user-not-found') {
            setErrors(({
              email: [],
              password: [],
              communication: ['アカウントが登録されていません'],
            }));
          }
        });
    } else {
      setLoading(false);
      setErrors(({
        email: emailError,
        password: passwordError,
        communication: [],
      }));
    }
  };

  const SNSSignIn = ({
    provider = null,
    providerName = '',
    setErrors = null,
    callback = null,
  }) => {
    firebase.auth().signInWithPopup(provider)
      .then(async (result) => {
        await result.user.getIdToken(/* forceRefresh */ true)
          .then((idToken) => {
            dispatch(setToken(idToken));
          });
        await callback();
      })
      .catch((error) => {
        if (error.code === 'auth/account-exists-with-different-credential') {
          firebase.auth().fetchSignInMethodsForEmail(error.email)
            .then((result) => {
              setErrors(({
                email: [],
                password: [],
                communication: [`${result[0]}ですでに登録されています。`],
              }));
            });
        } else if (error.code === 'auth/operation-not-supported-in-this-environment') {
          setErrors(({
            email: [],
            password: [],
            communication: [`この環境では${providerName}は利用できません。`],
          }));
        } else if (error.code === 'auth/cancelled-popup-request') {
          setErrors(({
            email: [],
            password: [],
            communication: ['連続したログイン要求はできません。'],
          }));
        } else if (error.code === 'auth/popup-blocked') {
          setErrors(({
            email: [],
            password: [],
            communication: ['ポップアップがブラウザにブロックされました。メールアドレスで登録してください。'],
          }));
        } else if (error.code === 'auth/unauthorized-domain') {
          setErrors(({
            email: [],
            password: [],
            communication: ['アプリのドメインが許可されていません。'],
          }));
        } else {
          setErrors(({
            email: [],
            password: [],
            communication: ['予期せぬエラーが発生しました。'],
          }));
        }
      });
  };

  const guestSignIn = () => {
    setLoading(true);
    firebase.auth().signInAnonymously()
      .then(async (firebaseResult) => {
        await firebaseResult.user.getIdToken(/* forceRefresh */ true)
          .then((idToken) => {
            dispatch(setToken(`guest ${idToken}`));
            signUpOrIn({
              variables: {
                name: 'ゲスト',
                userId: 'guest',
                email: 'guest@example.com',
                imageURL: '',
                provider: 'guest',
              },
            });
          });
      });
  };

  return {
    validationCheckAndAuth,
    loading,
    signUpOrIn,
    validationCheck,
    emailAndPasswordSignIn,
    SNSSignIn,
    guestSignIn,
  };
};
